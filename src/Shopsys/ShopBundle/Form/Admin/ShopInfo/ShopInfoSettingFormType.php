<?php

namespace Shopsys\ShopBundle\Form\Admin\ShopInfo;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ShopInfoSettingFormType extends AbstractType
{
    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('phoneNumber', TextType::class, [
                'required' => false,
            ])
            ->add('email', TextType::class, [
                'required' => false,
            ])
            ->add('phoneHours', TextType::class, [
                'required' => false,
            ])
            ->add('facebookLink', TextType::class, [
                'required' => false,
            ])
            ->add('googlePlusLink', TextType::class, [
                'required' => false,
            ])
            ->add('twitterLink', TextType::class, [
                'required' => false,
            ])
            ->add('youtubeLink', TextType::class, [
                'required' => false,
            ])
            ->add('customerServiceInfo', TextareaType::class, [
                'required' => false,
                'attr' => ['rows' => '5', 'style' => 'height: auto; width:auto;']
            ])
            ->add('save', SubmitType::class);
    }


    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'attr' => ['novalidate' => 'novalidate'],
        ]);
    }
}
