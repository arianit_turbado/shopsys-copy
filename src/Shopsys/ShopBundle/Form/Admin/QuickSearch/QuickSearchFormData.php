<?php

namespace Shopsys\ShopBundle\Form\Admin\QuickSearch;

class QuickSearchFormData
{
    /**
     * @var string|null
     */
    public $text;
}
