<?php

namespace Shopsys\ShopBundle\Form\Admin\ArticleCategory;

use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Shopsys\FormTypesBundle\MultidomainType;
use Shopsys\ShopBundle\Component\Domain\Config\DomainConfig;
use Shopsys\ShopBundle\Component\Domain\Domain;
use Shopsys\ShopBundle\Component\Form\InvertChoiceTypeExtension;
use Shopsys\ShopBundle\Component\Plugin\PluginCrudExtensionFacade;
use Shopsys\ShopBundle\Form\DomainsType;
use Shopsys\ShopBundle\Form\FileUploadType;
use Shopsys\ShopBundle\Form\Locale\LocalizedType;
use Shopsys\ShopBundle\Form\UrlListType;
use Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategory;
use Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryData;
use Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryFacade;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;

class ArticleCategoryFormType extends AbstractType
{
    /**
     * @var \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryFacade
     */
    private $articleCategoryFacade;

    /**
     * @var \Shopsys\ShopBundle\Component\Domain\Domain
     */
    private $domain;

    public function __construct(
        ArticleCategoryFacade $articleCategoryFacade,
        Domain $domain
    ) {
        $this->articleCategoryFacade = $articleCategoryFacade;
        $this->domain = $domain;
    }

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', LocalizedType::class, [
                'main_constraints' => [
                    new Constraints\NotBlank(['message' => 'Please enter name']),
                ],
                'entry_options' => [
                    'required' => false,
                    'constraints' => [
                        new Constraints\Length(['max' => 255, 'maxMessage' => 'Name cannot be longer than {{ limit }} characters']),
                    ],
                ],
            ])
            ->add('placement', ChoiceType::class, [
                'required' => false,
                'choices' => ['PLACEMENT_HEADER' => ArticleCategory::PLACEMENT_HEADER, 'PLACEMENT_FOOTER' => ArticleCategory::PLACEMENT_FOOTER, 'PLACEMENT_HEADER_FOOTER' => ArticleCategory::PLACEMENT_HEADER_FOOTER],
            ])
            ->add('iconName', TextType::class, [
                'required' => false,
            ])
            ->add('save', SubmitType::class);
    }

    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('articleCategory')
            ->setAllowedTypes('articleCategory', [ArticleCategory::class, 'null'])
            ->setDefaults([
                'data_class' => ArticleCategoryData::class,
                'attr' => ['novalidate' => 'novalidate'],
            ]);
    }

    /**
     * @param \Shopsys\ShopBundle\Component\Domain\Config\DomainConfig $domainConfig
     * @param \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategory|null $articleCategory
     * @return string
     */
    private function getArticleCategoryNameForPlaceholder(DomainConfig $domainConfig, ArticleCategory $articleCategory = null)
    {
        $domainLocale = $domainConfig->getLocale();
        return $articleCategory === null ? '' : $articleCategory->getName($domainLocale);
    }
}
