<?php

namespace Shopsys\ShopBundle\Form\Front\WarrantyClaim;

use Doctrine\ORM\PersistentCollection;
use Shopsys\ShopBundle\Component\Constraints\FieldsAreNotIdentical;
use Shopsys\ShopBundle\Component\Constraints\NotIdenticalToEmailLocalPart;
use Shopsys\ShopBundle\Form\DatePickerType;
use Shopsys\ShopBundle\Model\Country\CountryFacade;
use Shopsys\ShopBundle\Model\Customer\User;
use Shopsys\ShopBundle\Model\Order\Item\OrderItem;
use Shopsys\ShopBundle\Model\WarrantyClaim\WarrantyData;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Shopsys\ShopBundle\Model\Transport\TransportFacade;
use Shopsys\ShopBundle\Model\Payment\PaymentFacade;
use Shopsys\ShopBundle\Form\SingleCheckboxChoiceType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use GuzzleHttp\Client;

class WarrantyClaimForm extends AbstractType
{

    private $resolve_options_path = "http://orders.turbado.eu/api/rma_resolve_options/keyvalue";

    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $payments = $options['payment_facade']->getVisibleByDomainId($options['domain_id']);
        $transports = $options['transport_facade']->getVisibleByDomainId($options['domain_id'], $payments);
        $countries = $options['country_facade']->getAllByDomainId($options['domain_id']);
        $items = $options['items'];

        $builder->add('customer', TextType::class)
            ->add('transport', ChoiceType::class, [
                'choices' => $transports,
                'choice_label' => 'name',
                'choice_value' => 'id',
                'constraints' => [
                    new Constraints\NotNull(['message' => 'Please choose delivery type']),
                ],
                'invalid_message' => 'Please choose delivery type',
            ])
            ->add('item', ChoiceType::class, [
                'choices' => $items,
                'choice_label' => 'name',
                'choice_value' => 'id',
                'constraints' => [
                    new Constraints\NotNull(['message' => 'Please choose item']),
                ],
                'invalid_message' => 'Please choose item',
            ])
            ->add('resolve_option', ChoiceType::class, [
                'choices' => $this->getResolveOptions(),
                'constraints' => [
                    new Constraints\NotNull(['message' => 'Please choose resolve option']),
                ],
                'invalid_message' => 'Please choose resolve option',
            ])
            ->add('delivery_country', ChoiceType::class, [
                'choices' => $countries,
                'choice_label' => 'name',
                'choice_value' => 'id',
                'constraints' => [
                    new Constraints\NotBlank(['message' => 'Please choose country']),
                ],])
            ->add('order_date', DatePickerType::class)
            ->add('delivery_date', DatePickerType::class)
            ->add('serial_number', TextType::class)
            ->add('unlocking_code', TextType::class)
            ->add('problem_description', TextareaType::class)
            ->add('physical_condition', TextareaType::class)
            ->add('additional_comment', TextareaType::class)
            ->add('delivery_first_name', TextType::class)
            ->add('delivery_last_name', TextType::class)
            ->add('delivery_company_name', TextType::class)
            ->add('delivery_telephone', TextType::class)
            ->add('delivery_street', TextType::class)
            ->add('delivery_city', TextType::class)
            ->add('delivery_postcode', TextType::class)
            ->add('save', SubmitType::class);
    }

    /**
     * @param \Symfony\Component\OptionsResolver\OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver
            ->setRequired('domain_id')
            ->addAllowedTypes('domain_id', 'int')
            ->setRequired('payment_facade')
            ->setAllowedTypes('payment_facade', PaymentFacade::class)
            ->setRequired('transport_facade')
            ->setAllowedTypes('transport_facade', TransportFacade::class)
            ->setRequired('country_facade')
            ->setAllowedTypes('country_facade', CountryFacade::class)
            ->setRequired('items')
            ->setAllowedTypes('items', 'array')
            ->setDefaults([
                'data_class' => WarrantyData::class,
                'attr' => ['novalidate' => 'novalidate'],
            ]);
    }


    public function getResolveOptions()
    {
        $result = [];
        $client = new Client();
        try{
            $request = $client->request('GET', $this->resolve_options_path);
            $result = json_decode($request->getBody()->__toString(), true);
        } catch (\Exception $ex) {
            $result = ['1' => $ex->getMessage()];
        }
        return $result;
    }
}
