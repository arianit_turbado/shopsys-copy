// initial state
const state = {
  translations: {},
  currency: '€'
}

const getters = {
  translations: (state) => {
    return state.translations
  },
  currency: (state) => {
    return state.currency
  }
}

const actions = {
  saveTranslations: ({commit}, payload) => {
    const trans = JSON.parse(payload)
    commit('saveTranslations', trans)
  },
  saveCurrency: ({commit}, payload) => {
    commit('saveCurrency', payload)
  }
}

const mutations = {
  saveTranslations: (state, payload) => {
    state.translations = payload
  },
  saveCurrency: (state, payload) => {
    state.currency = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
