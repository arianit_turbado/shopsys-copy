// initial state
const state = {
  pages: 0,
  activePage: 1,
  productsPerPage: 10,
  activeCategoryId: null
}

const getters = {
  pagesCount: state => {
    return state.pages
  },
  activePage: state => {
    return state.activePage
  }
}

const actions = {
  changePage: ({commit, state, dispatch}, payload) => {
    console.log('changePage')
    commit('changePage', payload)
    dispatch('fetchProductsByCategory', state.activeCategoryId, {root: true})
    document.getElementById('root-listing').scrollIntoView()
  },
  changeCategoryId: ({commit}, payload) => {
    commit('changeCategoryId', payload)
  }
}

const mutations = {
  savePagesCountToStore: (state, payload) => {
    state.pages = payload
  },
  changePage: (state, payload) => {
    state.activePage = payload
  },
  changeCategoryId: (state, payload) => {
    state.activeCategoryId = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
