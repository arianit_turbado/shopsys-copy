// initial state
const state = {
  filters: {},
  activeFilters: {
    brands: {},
  },
  showFilter: false,
  showFilterText: 'Show Filters',
  orderBy: 'name_asc'
}

const getters = {
  productsFilters: state => {
    return state.filters
  },
  showFilter: state => {
    return state.showFilter
  },
  filterButtonText: state => {
    return state.showFilterText
  }
}

const actions = {
  changeSliderFilter: ({commit, dispatch, rootState, state}, payload) => {
    const minimalPrice = payload.value[0]
    const maximalPrice = payload.value[1]

    const isInit = function () {
      return state.activeFilters.Price === undefined
    }

    const isDifferentPrice = function () {
      if (!isInit()) {
        const currentMaxPrice = state.activeFilters.Price.maximalPrice
        const currentMinPrice = state.activeFilters.Price.minimalPrice

        return currentMaxPrice !== maximalPrice || currentMinPrice !== minimalPrice
      }
      return false
    }

    const itsInit = isInit()

    if (isDifferentPrice() || isInit())
      commit('changeFilterParameters', {id: 'Price', value: {minimalPrice, maximalPrice}})

    if (!itsInit || isDifferentPrice())
      dispatch('fetchProductsByCategory')

    commit('changePage', 1)
  },
  changeActiveBrands: ({commit, dispatch}, payload) => {
    console.log('changeCheckbox')
    commit('changeActiveBrands', payload)
    dispatch('fetchProductsByCategory')
    commit('changePage', 1)
  },
  changeActiveFilter: ({commit, dispatch}, payload) => {
    commit('changeActiveFilter', payload)
    commit('changePage', 1)
  },
  toggleFilters: ({commit}, payload) => {
    commit('toggleFilters')
  },
  changeOrder: ({commit, dispatch}, payload) => {
    commit('changeOrder', payload)
    dispatch('fetchProductsByCategory')
  }
}

const mutations = {
  changeActiveBrands: (state, payload) => {
    state.activeFilters.brands[payload.id] ? delete state.activeFilters.brands[payload.id] : state.activeFilters.brands[payload.id] = payload.value
  },
  changeOrder: (state, payload) => {
    state.orderBy = payload
  },
  changeFilterParameters: (state, payload) => {
    state.activeFilters[payload.id] = payload.value
  },
  toggleFilters: (state, payload) => {
    state.showFilter = !state.showFilter
    if (state.showFilter) {
      state.showFilterText = 'x'
    } else {
      state.showFilterText = 'Show Filter'
    }
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
