import Vue from 'vue'
import Vuex from 'vuex'

import Products from './modules/products'
import Manufacturers from './modules/manufacturers'
import Filters from './modules/filters'
import Pagination from './modules/pagination'
import Translations from './modules/translations'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    Products,
    Manufacturers,
    Filters,
    Pagination,
    Translations
  }
})

export default store
