import axios from 'axios'
import qs from 'querystring'

const API_URL = process.env.NODE_ENV === 'production' ? window.location.origin : 'http://127.0.0.1:8000'

export function fetchProducts(categoryId, page, orderBy, filters) {
  const filtersParam = filters ? '&' + filters : ''
  const orderByParams = orderBy ? '&orderBy=' + orderBy : '&orderBy=name_asc'
  return axios.get(API_URL + '/api/listProductsByCategory/' + categoryId + '?page=' + page + orderByParams + filtersParam)
  .then(({data}) => {
    return data
  })
}

export function fetchProductsByBrand(categoryId, page, orderBy, filters) {
  const filtersParam = filters ? '&' + filters : ''
  const orderByParams = orderBy ? '&orderBy=' + orderBy : '&orderBy=name_asc'
  return axios.get(API_URL + '/api/listProductsByBrand/' + categoryId + '?page=' + page + orderByParams + filtersParam)
  .then(({data}) => {
    return data
  })
}


export function addProductToCart(productId) {
  const data =
    {
      'add_product_form[quantity]': 1,
      'add_product_form[productId]': productId
    }

  return axios({
    method: 'POST',
    url: API_URL + '/cart/addProductAjax/',
    headers: {
      'X-Requested-With': 'XMLHttpRequest',
      'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
    },
    data: qs.stringify(data)
  })
  .then((data) => {
    if (data.status === 200) {
      Shopsys.addProduct.onSuccess(data.data, 'success');
      return data
    }
  })
}