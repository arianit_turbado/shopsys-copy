// initial state
const state = {
  manufacturers: []
}

const getters = {
  manufacturers: state => {
    return state.manufacturers
  }
}

const actions = {
}

const mutations = {
  saveManufacturersToStore: (state, payload) => {
    state.manufacturers = payload
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
