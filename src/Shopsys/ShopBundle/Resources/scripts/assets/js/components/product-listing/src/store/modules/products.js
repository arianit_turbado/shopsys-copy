import lodash from 'lodash'
import {fetchProducts, addProductToCart, fetchProductsByBrand} from '../../api'
import {fitFilters, getVisibleProducts} from '../../helpers/filters'

// initial state
const state = {
  categoryHasProducts: true,
  products: [],
  prices: {
    minPrice: null,
    maxPrice: null,
    defaultMinPrice: null,
    defaultMaxPrice: null
  },
  popup: {status: false, text: ''},
  isLoaded: false
}

const getters = {
  products: (state, getters, rootState) => {
    return state.products
  },
  prices: state => {
    return state.prices
  },
  popupStatus: state => {
    return state.popup.status
  },
  popupText: state => {
    return state.popup.text
  },
  isLoaded: state => {
    return state.isLoaded
  },
  categoryHasProducts: state => {
    return state.categoryHasProducts
  }
}

const actions = {
  fetchProductsByCategory: ({commit, rootState}) => {
    commit('isLoading');

    let activeFilters = {}

    if (Object.keys(rootState.Filters.activeFilters).length !== 0 && rootState.Filters.activeFilters.Price !== undefined) {
      const filters = rootState.Filters.activeFilters
      activeFilters.minimalPrice = filters.Price.minimalPrice
      activeFilters.maximalPrice = filters.Price.maximalPrice
    }

    if (Object.keys(rootState.Filters.activeFilters.brands).length !== 0) {
      activeFilters.brands = Object.keys(rootState.Filters.activeFilters.brands).map(function (k) {
        return k
      }).join(",");
    }

    const filtersParams = Object.entries(activeFilters).map(([key, val]) => `${key}=${val}`).join('&')

    fetchProducts(rootState.Pagination.activeCategoryId, rootState.Pagination.activePage, rootState.Filters.orderBy, filtersParams)
      .then(data => {
        if (data.products.length > 0) {
          commit('changeIsLoaded')
        } else {
          commit('noProducts')
        }
        commit('savePagesCountToStore', data.pageCount)
        commit('saveProductsToStore', data.products)
        commit('saveManufacturersToStore', data.brands, {root: true})
        commit('savePricesToStore', {maxPrice: data.highestPrice, minPrice: data.lowestPrice})
      })
  },
  fetchProductsByBrand: ({commit, rootState}) => {
    commit('isLoading');

    let activeFilters = {}

    if (Object.keys(rootState.Filters.activeFilters).length !== 0 && rootState.Filters.activeFilters.Price !== undefined) {
      const filters = rootState.Filters.activeFilters
      activeFilters.minimalPrice = filters.Price.minimalPrice
      activeFilters.maximalPrice = filters.Price.maximalPrice
    }

    if (Object.keys(rootState.Filters.activeFilters.brands).length !== 0) {
      activeFilters.brands = Object.keys(rootState.Filters.activeFilters.brands).map(function (k) {
        return k
      }).join(",");
    }

    const filtersParams = Object.entries(activeFilters).map(([key, val]) => `${key}=${val}`).join('&')

    fetchProductsByBrand(rootState.Pagination.activeCategoryId, rootState.Pagination.activePage, rootState.Filters.orderBy, filtersParams)
      .then(data => {
        if (data.products.length > 0) {
          commit('changeIsLoaded')
        } else {
          commit('noProducts')
        }
        commit('savePagesCountToStore', data.pageCount)
        commit('saveProductsToStore', data.products)
        commit('saveManufacturersToStore', data.brands, {root: true})
        commit('savePricesToStore', {maxPrice: data.highestPrice, minPrice: data.lowestPrice})
      })
  },
  changeProductsOrder: ({commit}, payload) => {
    commit('changeProductsOrder', payload)
  },
  addProductToCart: ({commit}, payload) => {
    addProductToCart(payload)
      .then((res) => {

          if (res.status === 200) {
          }
        }
      )
  },
  isLoading: ({commit}) => {
    commit('isLoading')
  }
}

const mutations = {
  saveProductsToStore: (state, payload) => {
    state.products = payload
    state.count = payload.length
  },
  savePricesToStore: (state, payload) => {
    state.prices.minPrice = payload.minPrice
    state.prices.defaultMinPrice = payload.minPrice
    state.prices.maxPrice = payload.maxPrice
    state.prices.defaultMaxPrice = payload.maxPrice
  },
  changeIsLoaded: (state, payload) => {
    state.isLoaded = true
  },
  isLoading: (state, payload) => {
    state.isLoaded = false
  },
  noProducts: (state, payload) => {
    state.categoryHasProducts = false
  },
  changeProductsOrder: (state, payload) => {
    switch (payload) {
      case 'alphabetically':
        let newArr = lodash.sortBy(state.products, ['name'])
        state.products = newArr
        break
      case 'priceLowest':
        newArr = lodash.sortBy(state.products, function (obj) {
          return parseFloat(obj.price)
        })
        state.products = newArr
        break
      case 'priceHighest':
        newArr = lodash.sortBy(state.products, function (obj) {
          return parseFloat(obj.price)
        })
        state.products = lodash.reverse(newArr)
        break
    }
  },
  showPopup: (state, payload) => {
    state.popup.text = payload
    state.popup.status = true
  },
  closePopup: (state, payload) => {
    state.popup.status = false
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
