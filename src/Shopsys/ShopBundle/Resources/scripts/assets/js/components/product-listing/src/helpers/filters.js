import lodash from 'lodash'

let parameters = {}

/**
 *
 * @param filters - filters array
 * @param products - initial products array
 * @returns {Array} - of visible products by filters
 */
export function getVisibleProducts (products, filters) {
  let res = []

  if (!lodash.isEmpty(filters.activeFilters)) {
    lodash.forEach(products, function (value, key) {
      passFilters(value, filters.activeFilters) ? res.push(value) : null
    })
  }
  else if (filters.Price) {
    return lodash.filter(products, function (o) {
      return o.price >= filters.Price[0] && o.price <= filters.Price[1]
    })
  }
  else {
    return products
  }

  return lodash.filter(res, function (o) {
    return o.price >= filters.Price[0] && o.price <= filters.Price[1]
  })
}

function passFilters (product, filters) {
  return lodash.every(filters, function (filterValue, filterName) {
    return lodash.some(product.parameters, (value, name) => name === filterName && lodash.includes(filterValue, value))
  })
}

export function fitFilters (products) {
  lodash.forEach(products, function (val, key) {
    getParametersFromProduct(val)
  })

  return parameters
}

function getParametersFromProduct (product) {
  lodash.forEach(product.parameters, function (val, key) {
    const parameterName = key
    const parameterValue = val
    const activeRow = lodash.find(parameters, ['title', parameterName])

    if (activeRow) {
      assignOption(activeRow.id, parameterValue)
    } else {
      let id = lodash.uniqueId()
      parameters[id] = {}
      parameters[id].id = id
      parameters[id].title = parameterName
      parameters[id].options = [parameterValue]
    }
  })
}

function assignOption (id, option) {
  if (!lodash.includes(parameters[id].options, option)) {
    parameters[id].options.push(option)
  }
}

export function editActiveFiltersObject (activeFilters, filter) {
  let activeRow = activeFilters[filter.title]
  if (activeRow) {
    if (lodash.includes(activeRow, filter.value)) {
      lodash.pull(activeFilters[filter.title], filter.value)
      if (activeFilters[filter.title].length === 0) delete activeFilters[filter.title]
    } else {
      activeFilters[filter.title].push(filter.value)
    }
  } else {
    activeFilters[filter.title] = [filter.value]
  }

  return activeFilters
}