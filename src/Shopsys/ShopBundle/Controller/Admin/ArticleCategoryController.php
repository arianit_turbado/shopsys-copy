<?php

namespace Shopsys\ShopBundle\Controller\Admin;

use Doctrine\ORM\QueryBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Shopsys\ShopBundle\Component\Controller\AdminBaseController;
use Shopsys\ShopBundle\Component\Domain\Domain;
use Shopsys\ShopBundle\Component\Grid\GridFactory;
use Shopsys\ShopBundle\Component\Router\Security\Annotation\CsrfProtection;
use Shopsys\ShopBundle\Form\Admin\ArticleCategory\ArticleCategoryFormType;
use Shopsys\ShopBundle\Form\Admin\Category\CategoryFormType;
use Shopsys\ShopBundle\Model\AdminNavigation\Breadcrumb;
use Shopsys\ShopBundle\Model\AdminNavigation\MenuItem;
use Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryFacade;
use Shopsys\ShopBundle\Model\Category\CategoryDataFactory;
use Shopsys\ShopBundle\Model\Category\CategoryFacade;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryDataFactory;
use Shopsys\ShopBundle\Component\Grid\QueryBuilderWithRowManipulatorDataSource;
use Shopsys\ShopBundle\Model\Product\Product;

class ArticleCategoryController extends AdminBaseController
{
    const ALL_DOMAINS = 0;

    /**
     * @var \Shopsys\ShopBundle\Model\AdminNavigation\Breadcrumb
     */
    private $breadcrumb;

    /**
     * @var \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryDataFactory
     */
    private $articleCategoryDataFactory;

    /**
     * @var \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryFacade
     */
    private $articleCategoryFacade;
    /**
     * @var \Shopsys\ShopBundle\Component\Domain\Domain
     */
    private $domain;

    /**
     * @var \Symfony\Component\HttpFoundation\Session\Session
     */
    private $session;

    /**
     * @var \Shopsys\ShopBundle\Component\Grid\GridFactory
     */
    private $gridFactory;

    public function __construct(
        Session $session,
        ArticleCategoryDataFactory $articleCategoryDataFactory,
        ArticleCategoryFacade $articleCategoryFacade,
        GridFactory $gridFactory,
        Domain $domain,
        Breadcrumb $breadcrumb
    )
    {
        $this->session = $session;
        $this->articleCategoryDataFactory = $articleCategoryDataFactory;
        $this->articleCategoryFacade = $articleCategoryFacade;
        $this->gridFactory = $gridFactory;
        $this->domain = $domain;
        $this->breadcrumb = $breadcrumb;
    }

    /**
     * @Route("/articleCategory/edit/{id}", requirements={"id" = "\d+"})
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function editAction(Request $request, $id)
    {
        $articleCategory = $this->articleCategoryFacade->getById($id);
        $articleCategoryData = $this->articleCategoryDataFactory->createFromArticleCategory($articleCategory);

        $form = $this->createForm(ArticleCategoryFormType::class, $articleCategoryData, [
            'articleCategory' => $articleCategory,
        ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $this->articleCategoryFacade->edit($id, $articleCategoryData);

            $this->getFlashMessageSender()->addSuccessFlashTwig(
                t('Article category<strong><a href="{{ url }}">{{ name }}</a></strong> was modified'),
                [
                    'name' => $articleCategory->getName(),
                    'url' => $this->generateUrl('admin_articlecategory_edit', ['id' => $articleCategory->getId()]),
                ]
            );
            return $this->redirectToRoute('admin_articlecategory_list');
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->getFlashMessageSender()->addErrorFlashTwig(t('Please check the correctness of all data filled.'));
        }

        $this->breadcrumb->overrideLastItem(new MenuItem(t('Editing article category - %name%', ['%name%' => $articleCategory->getName()])));

        return $this->render('@ShopsysShop/Admin/Content/ArticleCategory/edit.html.twig', [
            'form' => $form->createView(),
            'articleCategory' => $articleCategory,
        ]);
    }

    /**
     * @Route("/articleCategory/new/")
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function newAction(Request $request)
    {
        $articleCategoryData = $this->articleCategoryDataFactory->createDefault();

        $form = $this->createForm(ArticleCategoryFormType::class, $articleCategoryData, [
            'articleCategory' => null,
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $articleCategoryData = $form->getData();
            $articleCategory = $this->articleCategoryFacade->create($articleCategoryData);

            $this->getFlashMessageSender()->addSuccessFlashTwig(
                t('Article category <strong><a href="{{ url }}">{{ name }}</a></strong> created'),
                [
                    'name' => $articleCategory->getName(),
                    'url' => $this->generateUrl('admin_articlecategory_edit', ['id' => $articleCategory->getId()]),
                ]
            );

            return $this->redirectToRoute('admin_articlecategory_list');
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->getFlashMessageSender()->addErrorFlashTwig(t('Please check the correctness of all data filled.'));
        }

        return $this->render('@ShopsysShop/Admin/Content/ArticleCategory/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/articleCategory/list/")
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function listAction(Request $request)
    {
        $queryBuilder = $this->articleCategoryFacade->getArticleCategoryQueryBuilder();
        $grid = $this->getGrid($queryBuilder);

        return $this->render('@ShopsysShop/Admin/Content/ArticleCategory/list.html.twig', [
            'gridView' => $grid->createView()
        ]);
    }

    /**
     * @Route("/articleCategory/delete/{id}", requirements={"id" = "\d+"})
     * @CsrfProtection
     * @param int $id
     */
    public function deleteAction($id)
    {
        try {
            $fullName = $this->articleCategoryFacade->getById($id)->getName();

            $this->articleCategoryFacade->deleteById($id);

            $this->getFlashMessageSender()->addSuccessFlashTwig(
                t('Article category <strong>{{ name }}</strong> deleted'),
                [
                    'name' => $fullName,
                ]
            );
        } catch (\Exception $ex) {
            $this->getFlashMessageSender()->addErrorFlash(t('Selected article category doesn\'t exist.'));
        }

        return $this->redirectToRoute('admin_articlecategory_list');
    }

    /**
     * @param \Doctrine\ORM\QueryBuilder $queryBuilder
     * @return \Shopsys\ShopBundle\Component\Grid\Grid
     */
    private function getGrid(QueryBuilder $queryBuilder)
    {
        $dataSource = new QueryBuilderWithRowManipulatorDataSource(
            $queryBuilder,
            'ac.id',
            function ($row) {
                $articleCategory = $this->articleCategoryFacade->getById($row['ac']['id']);
                $row['articleCategory'] = $articleCategory;
                return $row;
            }
        );

        $grid = $this->gridFactory->create('articlecategorylis', $dataSource);
        $grid->enablePaging();
        $grid->enableSelecting();
        $grid->setDefaultOrder('name');

        $grid->addColumn('name', 'act.name', t('Name'), true);

        $grid->setActionColumnClassAttribute('table-col table-col-10');
        $grid->addEditActionColumn('admin_articlecategory_edit', ['id' => 'ac.id']);
        $grid->addDeleteActionColumn('admin_articlecategory_delete', ['id' => 'ac.id'])
            ->setConfirmMessage(t('Do you really want to remove this article category?'));

        $grid->setTheme('@ShopsysShop/Admin/Content/ArticleCategory/listGrid.html.twig');

        return $grid;
    }
}
