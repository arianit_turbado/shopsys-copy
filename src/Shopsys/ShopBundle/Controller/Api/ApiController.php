<?php

namespace Shopsys\ShopBundle\Controller\Api;

use Shopsys\ShopBundle\Model\Localization\Localization;
use Shopsys\ShopBundle\Model\Order\Status\OrderStatusData;
use Shopsys\ShopBundle\Model\Product\Parameter\ParameterData;
use Shopsys\ShopBundle\Model\Product\Parameter\ParameterValueData;
use Shopsys\ShopBundle\Model\Product\Parameter\ProductParameterValueData;
use Shopsys\ShopBundle\Model\WarrantyClaim\WarrantyData;
use Symfony\Component\HttpFoundation\JsonResponse;
use Shopsys\ShopBundle\Model\Category\CategoryFacade;
use Shopsys\ShopBundle\Model\Category\CategoryDataFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Shopsys\ShopBundle\Model\Product\ProductEditDataFactory;
use Shopsys\ShopBundle\Model\Product\ProductEditData;
use Shopsys\ShopBundle\Model\Product\ProductFacade;
use Shopsys\ShopBundle\Model\Product\Brand\BrandFacade;
use Shopsys\ShopBundle\Model\Product\Brand\BrandEditData;
use Shopsys\ShopBundle\Model\Product\Brand\BrandEditDataFactory;
use Shopsys\ShopBundle\Model\Product\Parameter\ParameterFacade;
use Shopsys\ShopBundle\Model\Product\Availability\AvailabilityFacade;
use Shopsys\ShopBundle\Form\Admin\Product\ProductEditFormType;
use Shopsys\ShopBundle\Model\Order\OrderFacade;
use Shopsys\ShopBundle\Model\Order\OrderRepository;
use Shopsys\ShopBundle\Component\Image\ImageFacade;
use Shopsys\ShopBundle\Model\Order\Status\OrderStatusFacade;
use Shopsys\ShopBundle\Model\Payment\PaymentFacade;
use Shopsys\ShopBundle\Model\Transport\TransportFacade;
use Shopsys\ShopBundle\Model\Order\OrderData;
use Shopsys\ShopBundle\Model\Order\Item\OrderItemData;
use Shopsys\ShopBundle\Model\Order\Item\OrderItemFacade;
use Shopsys\ShopBundle\Component\Domain\Domain;
use Shopsys\ShopBundle\Model\WarrantyClaim\WarrantyClaimFacade;
use Shopsys\ShopBundle\Model\Country\CountryFacade;

use Shopsys\ShopBundle\Model\Pricing\Group\PricingGroupRepository;
use Shopsys\ShopBundle\Model\Product\Pricing\ProductPriceCalculation;

class ApiController extends Controller
{

    const DOMAIN_ID = 1;
    const LOCALE = 'en';

    /**
     * @var \Shopsys\ShopBundle\Model\Product\ProductEditDataFactory
     */
    private $productEditDataFactory;


    /**
     * @var \Shopsys\ShopBundle\Model\Product\ProductFacade
     */
    private $productFacade;


    public function __construct(
        CategoryFacade $categoryFacade,
        ProductEditDataFactory $productEditDataFactory,
        ProductFacade $productFacade,
        BrandFacade $brandFacade,
        BrandEditData $brandEditData,
        BrandEditDataFactory $brandEditDataFactory,
        ParameterFacade $parameterFacade,
        CategoryDataFactory $categoryDataFactory,
        AvailabilityFacade $availabilityFacade,
        OrderFacade $orderFacade,
        OrderRepository $orderRepository,
        ImageFacade $imageFacade,
        OrderStatusFacade $orderStatusFacade,
        PaymentFacade $paymentFacade,
        TransportFacade $transportFacade,
        Localization $localization,
        OrderItemFacade $orderItemFacade,
        Domain $domain,
        WarrantyClaimFacade $warrantyClaimFacade,
        PricingGroupRepository $pricingGroupRepository,
        ProductPriceCalculation $productPriceCalculation,
        CountryFacade $countryFacade
    )
    {
        $this->categoryFacade = $categoryFacade;
        $this->productEditDataFactory = $productEditDataFactory;
        $this->productFacade = $productFacade;
        $this->brandFacade = $brandFacade;
        $this->brandEditData = $brandEditData;
        $this->brandEditDataFactory = $brandEditDataFactory;
        $this->parameterFacade = $parameterFacade;
        $this->categoryDataFactory = $categoryDataFactory;
        $this->availabilityFacade = $availabilityFacade;
        $this->orderFacade = $orderFacade;
        $this->orderRepository = $orderRepository;
        $this->imageFacade = $imageFacade;
        $this->orderStatusFacade = $orderStatusFacade;
        $this->paymentFacade = $paymentFacade;
        $this->trasnportFacade = $transportFacade;
        $this->localization = $localization;
        $this->orderItemFacade = $orderItemFacade;
        $this->domain = $domain;
        $this->warrantyClaimFacade = $warrantyClaimFacade;
        $this->countryFacade = $countryFacade;
        $this->pricingGroupRepository = $pricingGroupRepository;
        $this->productPriceCalculation = $productPriceCalculation;

    }

    public function downloadTransAction()
    {
        if ($this->domain->getId() == 100) {
            return $this->file('translations/messages.ro.po');
        } else {
            return $this->file('translations/messages.' . $this->domain->getLocale() . '.po');
        }
    }

    public function getCategoriesTreeAction(Request $request)
    {
        $details = $this->categoryFacade->getCategoriesTree($request->get('locale'));
        return new JsonResponse(array('categories' => $details));
    }

    public function getOrderAction(Request $request)
    {
        if ($request->get('id') == 'all') {
            $order = [];
            foreach ($this->orderRepository->getAll() as $o) {
                $order[] = $this->orderFacade->exportToJson($o->getId());
            }
        } else {
            $order = $this->orderFacade->exportToJson($request->get('id'));
        }

        return new JsonResponse($order);

    }

    public function getOrderItemsAction(Request $request)
    {
        $order = $this->orderRepository->getById($request->get('id'));
        $orderItems = $order->getProductItems();
        $result = [];
        foreach ($orderItems as $item) {
            $result[$item->getId()] = $item->getName();
        }
        return new JsonResponse($result);
    }

    public function getWarrantyClaimAction(Request $request)
    {
        $warranty = $this->warrantyClaimFacade->exportToJson($request->get('id'));
        return new JsonResponse($warranty);
    }

    public function registerWarrantyClaimAction(Request $request)
    {
        try {
            $warrantyData = new WarrantyData($request->get('info'));
            $warrantyData->delivery_country = $this->countryFacade->getFirstByName($warrantyData->delivery_country);
            $warrantyData->delivery_date = new \DateTime($warrantyData->delivery_date);
            $warrantyData->order_date = new \DateTime($warrantyData->order_date);
            $warranty = $this->warrantyClaimFacade->create($warrantyData);
            $result = $warranty->getId();
        } catch (\Exception $ex) {
            $result = $ex->getMessage();
        }
        return new JsonResponse($result);
    }

    public function updateWarrantyClaimAction(Request $request)
    {
        try {
            $warrantyData = new WarrantyData($request->get('info'));
            $warrantyData->delivery_country = $this->countryFacade->getFirstByName($warrantyData->delivery_country);
            $warrantyData->delivery_date = new \DateTime($warrantyData->delivery_date);
            $warrantyData->order_date = new \DateTime($warrantyData->order_date);
            $warranty = $this->warrantyClaimFacade->edit($request->get('id'), $warrantyData);
            $result = $warranty->getId();
        } catch (\Exception $ex) {
            $result = $ex->getMessage();
        }
        return new JsonResponse($result);
    }

    public function getPricingGroupsAction()
    {
        $result = [];
        foreach ($this->pricingGroupRepository->getPricingGroupsByDomainId($this->domain->getId()) as $pricingGroup)
        {
            $result[$pricingGroup->getId()] = $pricingGroup->getName();
        }
        return new JsonResponse($result);
    }

    public function importProductAction(Request $request)
    {
        set_time_limit(0);
        $success = true;

        $product = $this->productFacade->findByMasterProductId($request->get('product_edit_form')['productData']['masterProductId']);

        if ($product === null) {
            $productEditData = $this->productEditDataFactory->createDefault();
            $form = $this->createForm(ProductEditFormType::class, $productEditData, ['product' => null]);
            $form->handleRequest($request);

            $this->addExtraData($request, $productEditData, $product);
            $product = $this->productFacade->create($productEditData);
            $this->saveParameters($request, $product);

        } else {
            //Delete Images First
            $productEditData = $this->productEditDataFactory->createFromProduct($product);
            $productEditData->imagesToDelete = $this->imageFacade->getAllImagesByEntity($product);
            $this->productFacade->edit($product->getId(), $productEditData);
            //END DELETED IMAGES

            $product = $this->productFacade->findByMasterProductId($request->get('product_edit_form')['productData']['masterProductId']);
            $productEditData = $this->productEditDataFactory->createFromProduct($product);
            $form = $this->createForm(ProductEditFormType::class, $productEditData, ['product' => $product]);
            $form->handleRequest($request);
            $this->addExtraData($request, $productEditData, $product);
            $product = $this->productFacade->edit($product->getId(), $form->getData());
            $this->saveParameters($request, $product);

        }

        return new JsonResponse(array('success' => $success));
    }

    private function addExtraData(Request $request, ProductEditData $productEditData, $product)
    {

        $productEditData->productData->availability = $this->availabilityFacade->getById(2);


        $productEditData->productFeatures = $request->get('product_edit_form')['is_featured'];
        $productEditData->productSpecials = $request->get('product_edit_form')['is_special'];
        $productEditData->productOnSales = $request->get('product_edit_form')['is_on_sale'];


        if ($request->get('brand'))
            $productEditData->productData->brand = $this->findOrCreateBrandByName($request->get('brand'));

        if ($product != null) {
            $categoriesDomains = $product->getCategoriesIndexedByDomainId();
            foreach ($categoriesDomains as $domainId => $categories) {
                if (!array_key_exists($domainId, $request->get('product_edit_form')['productData']['categoriesByDomainId'])) {
                    $productEditData->productData->categoriesByDomainId[$domainId] = $categories;
                }
            }
        }


        // if( null !== $request->get('categories')) {
        //     $categoriesToImport = json_decode($request->get('categories'));
        //     $categories = [];
        //     foreach ($categoriesToImport as $category) {
        //         if($category->name_en) {
        //             $categories[] = $this->findOrCreateCategoryByName($category);
        //         }
        //     }

        //     $productEditData->productData->categoriesByDomainId[1] =  $categories;
        //     $productEditData->productData->categoriesByDomainId[2] =  $categories;

        // }
    }


    private function findOrCreateBrandByName($name)
    {
        $brand = $this->brandFacade->findByName($name);

        if ($brand === null) {
            $brandEditData = $this->brandEditDataFactory->createDefault();
            $brandEditData->brandData->name = $name;
            $brand = $this->brandFacade->create($brandEditData);
        }

        return $brand;
    }

    private function saveParameters(Request $request, $product)
    {
        $productParameterValuesData = [];
        $savedParameters = [];
        $parameters = json_decode($request->get('parameters'));
        if ($parameters != '' && count($parameters) > 0) {
            foreach ($parameters as $parameterToImport) {
                $names = (array)$parameterToImport->names;
                $parameter = $this->parameterFacade->findParameterByNames($names);
                if ($parameter === null) {
                    $parameterEditData = new ParameterData([], true);
                    $parameterEditData->name = $names;
                    $parameterEditData->visible = true;
                    $parameter = $this->parameterFacade->create($parameterEditData);
                }

                if (!in_array($parameter->getId(), $savedParameters)) {
                    foreach ($names as $locale => $name) {
                        $productParameterValueData = new ProductParameterValueData();
                        $productParameterValueData->parameter = $parameter;
                        $productValueData = new ParameterValueData();
                        $productValueData->text = $parameterToImport->value;
                        $productValueData->locale = $locale;
                        $productParameterValueData->parameterValueData = $productValueData;

                        $productParameterValuesData[] = $productParameterValueData;
                    }

                    $savedParameters[] = $parameter->getId();
                }

            }

            $this->productFacade->saveParameters($product, $productParameterValuesData);

        }

        return $product;
    }

    public function statusesAction(Request $request)
    {
        return new JsonResponse($this->orderStatusFacade->getAllNames());
    }

    public function createStatusAction(Request $request)
    {
        $status_array = array();
        $status = $request->get('status');
        if (is_null($status) || $status == '') {
            return new JsonResponse(array('success' => false));
        }
        foreach ($this->localization->getLocalesOfAllDomains() as $key => $value) {
            $status_array[$key] = $status;
        }
        try {
            $new_status_id = $this->orderStatusFacade->create(new OrderStatusData($status_array))->getId();
            return new JsonResponse(array('success' => true, 'data' => $new_status_id));
        } catch (Exception $exception) {
            return new JsonResponse(array('success' => false, 'data' => $exception->getMessage()));
        }
    }

    public function paymentMethodsAction(Request $request)
    {
        return new JsonResponse($this->paymentFacade->getVisibleOnCurrentDomainJson());
    }

    public function shipmentMethodsAction(Request $request)
    {
        return new JsonResponse($this->trasnportFacade->getAllJson());
    }

    public function updateOrderAction(Request $request)
    {
        $id = $request->get('id');

        $order = $this->orderFacade->getById($id);
        $orderData = new OrderData();
        $orderData->setFromEntity($order);
        $success = true;
        $hash = '';

        if ($order != null) {
            $orderDataFromApi = json_decode($request->get('order_data'));
            // var_dump($orderDataFromApi);
            // die();

            //note
            if (isset($orderDataFromApi->note) && $orderDataFromApi->note != '') {
                $orderData->note = $orderDataFromApi->note;
            }

            //status
            if (isset($orderDataFromApi->order_status_id) && $orderDataFromApi->order_status_id != '') {
                // die('ERDH');
                // $statuses = $this->orderStatusFacade->getAllNames();
                // $orderStatusId = array_search($orderDataFromApi->status, $statuses);

                // if($orderStatusId > 0) {
                $orderStatus = $this->orderStatusFacade->getById($orderDataFromApi->order_status_id);
                $orderData->status = $orderStatus;
                // }
            }

            if (isset($orderDataFromApi->payment_method) && $orderDataFromApi->payment_method != '') {
                $orderPaymentData = new \Shopsys\ShopBundle\Model\Order\Item\OrderPaymentData();
                $orderPaymentData->payment = $this->paymentFacade->getById($orderDataFromApi->payment_method->id);
                $orderPaymentData->name = $orderPaymentData->payment->getName();
                $price = $orderDataFromApi->payment_method->value != null ? $orderDataFromApi->payment_method->value : 0;
                $orderPaymentData->priceWithoutVat = $price;
                $orderPaymentData->priceWithVat = $price;
                $orderPaymentData->vatPercent = 0;
                $orderPaymentData->quantity = 1;
                $orderData->orderPayment = $orderPaymentData;
            }

            if (isset($orderDataFromApi->shipping_method) && $orderDataFromApi->shipping_method != '') {
                $orderTransport = new \Shopsys\ShopBundle\Model\Order\Item\OrderTransportData();
                $orderTransport->transport = $this->trasnportFacade->getById($orderDataFromApi->shipping_method->id);
                $orderTransport->name = $orderTransport->transport->getName();
                $price = $orderDataFromApi->shipping_method->value != null ? $orderDataFromApi->shipping_method->value : 0;
                $orderTransport->priceWithoutVat = $price;
                $orderTransport->priceWithVat = $price;
                $orderTransport->vatPercent = 0;
                $orderTransport->quantity = 1;
                $orderData->orderTransport = $orderTransport;
            }


            //customer
            if (isset($orderDataFromApi->customer)) {
                if (isset($orderDataFromApi->customer->firstname) && $orderDataFromApi->customer->firstname != '') {
                    $orderData->firstName = $orderDataFromApi->customer->firstname;
                }

                if (isset($orderDataFromApi->customer->lastname) && $orderDataFromApi->customer->lastname != '') {
                    $orderData->lastName = $orderDataFromApi->customer->lastname;
                }

                if (isset($orderDataFromApi->customer->email) && $orderDataFromApi->customer->email != '') {
                    $orderData->email = $orderDataFromApi->customer->email;
                }

                if (isset($orderDataFromApi->customer->phone_number) && $orderDataFromApi->customer->phone_number != '') {
                    $orderData->telephone = $orderDataFromApi->customer->phone_number;
                }

                if (isset($orderDataFromApi->customer->company) && $orderDataFromApi->customer->company != '') {
                    $orderData->companyName = $orderDataFromApi->customer->company;
                }
            }


            //billing_info
            if (isset($orderDataFromApi->billing_info)) {
                if (isset($orderDataFromApi->billing_info->street) && $orderDataFromApi->billing_info->street != '') {
                    $orderData->street = $orderDataFromApi->billing_info->street;
                }

                if (isset($orderDataFromApi->billing_info->city) && $orderDataFromApi->billing_info->city != '') {
                    $orderData->city = $orderDataFromApi->billing_info->city;
                }


                if (isset($orderDataFromApi->billing_info->postal_code) && $orderDataFromApi->billing_info->postal_code != '') {
                    $orderData->postcode = $orderDataFromApi->billing_info->postal_code;
                }
            }

            //shipping_info
            if (isset($orderDataFromApi->shipping_info)) {
                $orderData->deliveryAddressSameAsBillingAddress = false;


                if (isset($orderDataFromApi->shipping_info->firstname) && $orderDataFromApi->shipping_info->firstname != '') {
                    $orderData->deliveryFirstName = $orderDataFromApi->shipping_info->firstname;
                } else {
                    $orderData->deliveryFirstName = $order->getFirstname();
                }

                if (isset($orderDataFromApi->shipping_info->lastname) && $orderDataFromApi->shipping_info->lastname != '') {
                    $orderData->deliveryLastName = $orderDataFromApi->shipping_info->lastname;
                } else {
                    $orderData->deliveryLastName = $order->getLastName();
                }

                if (isset($orderDataFromApi->shipping_info->street) && $orderDataFromApi->shipping_info->street != '') {
                    $orderData->deliveryStreet = $orderDataFromApi->shipping_info->street;
                } else {
                    $orderData->deliveryStreet = $order->getStreet();
                }

                if (isset($orderDataFromApi->shipping_info->city) && $orderDataFromApi->shipping_info->city != '') {
                    $orderData->deliveryCity = $orderDataFromApi->shipping_info->city;
                } else {
                    $orderData->deliveryCity = $order->getCity();
                }


                if (isset($orderDataFromApi->shipping_info->postal_code) && $orderDataFromApi->shipping_info->postal_code != '') {
                    $orderData->deliveryPostcode = $orderDataFromApi->shipping_info->postal_code;
                } else {
                    $orderData->deliveryPostcode = $order->getPostCode();
                }

                $orderData->deliveryCountry = $order->getCountry();

                if (isset($orderDataFromApi->shipping_info->phone_number) && $orderDataFromApi->shipping_info->phone_number != '') {
                    $orderData->deliveryTelephone = $orderDataFromApi->shipping_info->phone_number;
                }

                if (isset($orderDataFromApi->shipping_info->company) && $orderDataFromApi->shipping_info->company != '') {
                    $orderData->deliveryCompanyName = $orderDataFromApi->shipping_info->company;
                }

            }


            //orderProducts
            // if(isset($orderDataFromApi->order_products) && count($orderDataFromApi->order_products) > 0) {
            //     $orderProductsToSave  = [];
            //     $orderData->itemsWithoutTransportAndPayment = $orderProductsToSave;
            // }

            $orderProductsToSave = [];

            if (isset($orderDataFromApi->discounts) && count($orderDataFromApi->discounts) > 0) {
                foreach ($orderDataFromApi->discounts as $discount) {
                    $discountToSave = new OrderItemData();
                    $discountToSave->name = $discount->title;
                    $discountToSave->priceWithVat = $discount->value;
                    $discountToSave->priceWithoutVat = $discount->value;
                    $discountToSave->vatPercent = 0;
                    $discountToSave->quantity = 1;
                    $orderProductsToSave['new_' . rand() . time() . $discount->title] = $discountToSave;
                }
            }

            $orderData->itemsWithoutTransportAndPayment = $orderProductsToSave;

            if(isset($orderDataFromApi->pricing_group)) {
                $orderData->pricingGroup = $orderDataFromApi->pricing_group;
            }

            // createOrderProductInOrder

            $order = $this->orderFacade->edit($id, $orderData);

            if (isset($orderDataFromApi->order_products) && count($orderDataFromApi->order_products) > 0) {
                foreach ($orderDataFromApi->order_products as $orderProduct) {
                    $product = $this->productFacade->getById($orderProduct->id);

                    if(isset($orderProduct->helper) && $orderProduct->helper != ''){
                        $itemHelper = $orderProduct->helper;
                        $itemHelperForCalculation = json_decode($itemHelper);

                        $price = ((array)$itemHelperForCalculation->pricingGroups)[$orderData->pricingGroup]->coefficient * $itemHelperForCalculation->prices->priceWithVat;

                    } else {
                        $itemHelper = ['prices' => [], 'pricingGroups'];

                        foreach ($this->pricingGroupRepository->getPricingGroupsByDomainId($this->domain->getId()) as $key => $item) {
                            $itemHelper['pricingGroups'][$item->getId()]['coefficient'] = $item->getCoefficient();
                            $itemHelper['pricingGroups'][$item->getId()]['name'] = $item->getName();
                        }

                        $priceCalculation = $this->productPriceCalculation->calculatePrice($product, $this->domain->getId(), $this->pricingGroupRepository->getDefault($this->domain->getId()));

                        $itemHelper['prices']['priceWithoutVat'] = $priceCalculation->getPriceWithoutVat();
                        $itemHelper['prices']['priceWithVat'] = $priceCalculation->getPriceWithVat();
                        $itemHelper['prices']['vatAmount'] = $priceCalculation->getVatAmount();
                        $price = $itemHelper['pricingGroups'][$orderData->pricingGroup]['coefficient'] * $priceCalculation->getPriceWithVat();
                        $itemHelper = json_encode($itemHelper);
                    }

                    $this->orderItemFacade->createOrderProductInOrderWithQuantity($order->getId(), $product->getId(), $orderProduct->quantity, $price, $itemHelper);
                }
            }

            $hash = $this->orderFacade->exportToJson($request->get('id'))[$order->getId()]['info']['orders_hash'];

        } else {
            $success = false;
        }


        return new JsonResponse(['success' => $success, 'hash' => $hash]);


    }


//    private function findOrCreateCategoryByName($categoryToIMP) {
//        $category = $this->categoryFacade->getByDomainAndName(1, 'en', $categoryToIMP->name_en);
//
//        if($category === null) {
//            $categoryEditData = $this->categoryDataFactory->createDefault();
//            $categoryEditData->name['en'] = $categoryToIMP->name_en;
//            $categoryEditData->name['cs'] = $categoryToIMP->name_cz;
//            $category = $this->categoryFacade->create($categoryEditData);
//        }
//
//        return $category;
//    }


    //    public function importProductAction2(Request $request) {

    //        $productData = $request->get('product_data');

    //        $productEditData = $this->productEditDataFactory->createDefault();
    //      $this->fillProductEditData($productEditData, $productData);
    //      $product = $this->productFacade->create($productEditData);

    //        return new JsonResponse(array('success' => true, 'product_id' => $product->getId()));
    //    }


    //    private function fillProductEditData(ProductEditData $productEditData, array $externalProductData)
    // {
    //     $productEditData->productData->name['en'] = $externalProductData['name_en'];
    //        $productEditData->productData->name['cs'] = $externalProductData['name_cz'];
    //     // $productEditData->productData->price = $externalProductData['price_without_vat'];
    //     // $productEditData->productData->vat =$externalProductData['vat_percent'];
    //     $productEditData->productData->ean = $externalProductData['ean'];
    //        $productEditData->productData->partno = $externalProductData['part_number'];

    //     $productEditData->descriptions[1] = $externalProductData['description_en'];
    //     $productEditData->shortDescriptions[1]  = $externalProductData['short_description_en'];
    //        $productEditData->descriptions[2] = $externalProductData['description_cz'];
    //        $productEditData->shortDescriptions[2]  = $externalProductData['short_description_cz'];
    //     $productEditData->seoTitles[1] = $externalProductData['title_en'];
    //     $productEditData->seoMetaDescriptions[1]  = $externalProductData['meta_description_en'];
    //        $productEditData->seoTitles[2] = $externalProductData['title_cz'];
    //        $productEditData->seoMetaDescriptions[2]  = $externalProductData['meta_description_cz'];
    //     $productEditData->productData->usingStock = false;
    //     // $productEditData->productData->stockQuantity = $externalProductData['stock_quantity'];
    //        $productEditData->productData->availability = $this->availabilityFacade->getById(2);

    //        if($externalProductData['brand'])
    //            $productEditData->productData->brand = $this->findOrCreateBrandByName($externalProductData['brand']);

    //        if(isset($externalProductData['categories'])) {
    //            $categories = [];
    //            foreach ($externalProductData['categories'] as $category) {
    //                if($category['name_en']) {
    //                    $categories[] = $this->findOrCreateCategoryByName($category);
    //                }
    //            }

    //            $productEditData->productData->categoriesByDomainId[self::DOMAIN_ID] =  $categories;

    //        }

    //        $productEditData->parametes = [];
    // }


    //    public function importProductImagesAction(Request $request) {
    //        $product = $this->productFacade->getById($request->get('product_id'));
    //        $productEditDataA = $this->productEditDataFactory->createFromProduct($product);
    //        $productEditData = $this->productEditDataFactory->createFromProduct($product);

    //        $form = $this->createForm(ProductEditFormType::class, $productEditDataA, ['product' => $product]);
    //        $form->handleRequest($request);
    //        $productEditData->imagesToUpload = $form->getData()->imagesToUpload;

    //        $this->productFacade->edit($product->getId(), $productEditData);


    //        return new JsonResponse(array('success' =>  $form->getData()->imagesToUpload));
    //    }


}
