<?php

namespace Shopsys\ShopBundle\Controller\Api;

use Shopsys\ShopBundle\Model\Product\Detail\ProductDetailFactory;
use Symfony\Component\HttpFoundation\JsonResponse;
use Shopsys\ShopBundle\Model\Category\CategoryFacade;
use Shopsys\ShopBundle\Component\Domain\Domain;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Shopsys\ShopBundle\Model\Product\Listing\ProductListOrderingModeForListFacade;
use Shopsys\ShopBundle\Model\Product\Filter\ProductFilterData;
use Shopsys\ShopBundle\Model\Category\Category;
use Shopsys\ShopBundle\Model\Product\Filter\ProductFilterConfigFactory;
use Shopsys\ShopBundle\Form\Front\Product\ProductFilterFormType;
use Shopsys\ShopBundle\Model\Product\ProductOnCurrentDomainFacade;
use Shopsys\ShopBundle\Component\Image\ImageFacade;
use Shopsys\ShopBundle\Model\Product\ProductRepository;
use Shopsys\ShopBundle\Component\Router\FriendlyUrl\FriendlyUrlFacade;
use Shopsys\ShopBundle\Model\Product\Brand\BrandFacade;
use Shopsys\ShopBundle\Model\Product\ProductFacade;
use Shopsys\ShopBundle\Model\Product\ProductEditDataFactory;



class ProductController extends Controller
{

    const ORDER_MODELS = ['priority', 'name_asc', 'name_desc', 'price_asc', 'price_desc'];


    public function __construct(
        CategoryFacade $categoryFacade,
        Domain $domain,
        ProductListOrderingModeForListFacade $productListOrderingModeForListFacade,
        ProductFilterConfigFactory $productFilterConfigFactory,
        ProductOnCurrentDomainFacade $productOnCurrentDomainFacade,
        ImageFacade $imageFacade,
        ProductRepository $productRepository,
        ProductDetailFactory $productDetailFactory,
        FriendlyUrlFacade $friendlyUrlFacade,
        BrandFacade $brandFacade,
        ProductFacade $productFacade,
        ProductEditDataFactory $productEditDataFactory
    ) {
        $this->categoryFacade = $categoryFacade;
        $this->domain = $domain;
        $this->productListOrderingModeForListFacade = $productListOrderingModeForListFacade;
        $this->productFilterConfigFactory = $productFilterConfigFactory;
        $this->productOnCurrentDomainFacade = $productOnCurrentDomainFacade;
        $this->imageFacade = $imageFacade;
        $this->productRepository = $productRepository;
        $this->productDetailFactory = $productDetailFactory;
        $this->friendlyUrlFacade = $friendlyUrlFacade;
        $this->brandFacade = $brandFacade;
        $this->productFacade = $productFacade;
        $this->productEditDataFactory = $productEditDataFactory;
    }

    public function listProductsAction()
    {

        $products = [];

        
        foreach ($this->productRepository->getAll() as $product) {

            $images = $this->imageFacade->getAllImagesByEntity($product);
            $imageUrl = '';
            if(count($images) > 0) {
                try {
                    $imageUrl = $this->imageFacade->getImageUrl($this->domain->getCurrentDomainConfig(), $images[0]);
                } catch (\Shopsys\ShopBundle\Component\Image\Exception\ImageNotFoundException $e) {
                    $imageUrl = '';
                }

            }


            $productDeatial = $this->productDetailFactory->getDetailForProduct($product);

            $descriptions = $productDeatial->getProductDomainsIndexedByDomainId();

            if(array_key_exists($this->domain->getCurrentDomainConfig()->getId(), $descriptions)) {
                $description = $descriptions[$this->domain->getCurrentDomainConfig()->getId()];
                $description = $description->getDescription();

            } else {
                $description = '';
            }

            $categories = [];
            $categoriesDomains = $product->getCategoriesIndexedByDomainId();
            if(array_key_exists($this->domain->getCurrentDomainConfig()->getId(), $categoriesDomains)) {
                foreach($categoriesDomains[$this->domain->getCurrentDomainConfig()->getId()] as $category) {
                    $categories[] = $category->getName();
                }
            }

            $friendlyUrl = $this->friendlyUrlFacade->findMainFriendlyUrl($this->domain->getCurrentDomainConfig()->getId(), 'front_product_detail' , $product);
            if($friendlyUrl != null) {
                $url = $this->domain->getCurrentDomainConfig()->getUrl() . '/' . $friendlyUrl->getSlug();
            } else {
                $url = '';
            }

            $products[] = [
                'product_id' => $product->getId(),
                'master_product_id' => $product->getMasterProductId(),
                'product_part' => $product->getPartNo(),
                'product_name' => $product->getName(),
                'product_pid' => $product->getProductPid(),
                'manufacturer' => $product->getBrand() === null ? '' : $product->getBrand()->getName(),
                'part_no' => $product->getPartNo(),
                'price' => $productDeatial->getSellingPrice()->getPriceWithVat(),
                'stock_quantity' => $product->getStockQuantity(),
                'image_url' => $imageUrl,
                'description' => $description,
                'url' => $url,
                'active' => !$product->isHidden(),
                'categories' => $categories,
                'eans' => [$product->getEan()],


            ];
        }

        return new JsonResponse($products);
    }


    public function searchProductsAction(Request $request)
    {
        $products = [];

        if($request->get('ean') != null ){
            $ean=$request->get('ean');
        }else{
            $ean=0;
        }

        if($request->get('master_id') != null ){
            $master_id=$request->get('master_id');
        }else{
            $master_id=0;
        }

        if($request->get('supplier') != null ){
            $supplier=$request->get('supplier');
        }else{
            $supplier=0;
        }

        if($request->get('name') != null ){
            $name=$request->get('name');
        }else{
            $name=0;
        }

        $domain_id=$this->domain->getCurrentDomainConfig()->getId();
        foreach ($this->productRepository->getSearch($domain_id,$ean,$master_id,$supplier,$name) as $product) {

            $images = $this->imageFacade->getAllImagesByEntity($product);
            $imageUrl = '';
            if(count($images) > 0) {
                try {
                    $imageUrl = $this->imageFacade->getImageUrl($this->domain->getCurrentDomainConfig(), $images[0]);
                } catch (\Shopsys\ShopBundle\Component\Image\Exception\ImageNotFoundException $e) {
                    $imageUrl = '';
                }

            }


            $productDeatial = $this->productDetailFactory->getDetailForProduct($product);

            $descriptions = $productDeatial->getProductDomainsIndexedByDomainId();

            if(array_key_exists($this->domain->getCurrentDomainConfig()->getId(), $descriptions)) {
                $description = $descriptions[$this->domain->getCurrentDomainConfig()->getId()];
                $description = $description->getDescription();

            } else {
                $description = '';
            }

            $categories = [];
            $categoriesDomains = $product->getCategoriesIndexedByDomainId();
            if(array_key_exists($this->domain->getCurrentDomainConfig()->getId(), $categoriesDomains)) {
                foreach($categoriesDomains[$this->domain->getCurrentDomainConfig()->getId()] as $category) {
                    $categories[] = $category->getName();
                }
            }

            $friendlyUrl = $this->friendlyUrlFacade->findMainFriendlyUrl($this->domain->getCurrentDomainConfig()->getId(), 'front_product_detail' , $product);
            if($friendlyUrl != null) {
                $url = $this->domain->getCurrentDomainConfig()->getUrl() . '/' . $friendlyUrl->getSlug();
            } else {
                $url = '';
            }

            $products[] = [
                'product_id' => $product->getId(),
                'master_product_id' => $product->getMasterProductId(),
                'product_part' => $product->getPartNo(),
                'product_name' => $product->getName(),
                'product_pid' => $product->getProductPid(),
                'manufacturer' => $product->getBrand() === null ? '' : $product->getBrand()->getName(),
                'part_no' => $product->getPartNo(),
                'price' => $productDeatial->getSellingPrice()->getPriceWithVat(),
                'stock_quantity' => $product->getStockQuantity(),
                'image_url' => $imageUrl,
                'description' => $description,
                'url' => $url,
                'active' => !$product->isHidden(),
                'categories' => $categories,
                'eans' => [$product->getEan()],


            ];
        }

        return new JsonResponse($products);
    }

    public function listProductsByBrandAction(Request $request, $id) {

        $response = [];

        $requestPage = $request->get('page');
        $minimalPrice = $request->get('minimalPrice');
        $maximalPrice = $request->get('maximalPrice');
        $page = $requestPage === null ? 1 : (int)$requestPage;

        $orderingModeId = $request->get('orderBy');

        if (!in_array($orderingModeId, ProductController::ORDER_MODELS, true)) {
            $orderingModeId = ProductController::ORDER_MODELS[0];
        }

        $productFilterData = new ProductFilterData();

        $brand = $this->brandFacade->getById($id);

        $productFilterData->brands[] = $brand;

        $productFilterConfig = $this->createProductFilterConfig();
        $filterForm = $this->createForm(ProductFilterFormType::class, $productFilterData, [
            'product_filter_config' => $productFilterConfig,
        ]);

        $filterForm->handleRequest($request);

        if($minimalPrice != '') {
            $minimalPrice = str_replace(',', '.', $minimalPrice);
            $productFilterData->minimalPrice = $minimalPrice;
        }

        if($maximalPrice != '' && $maximalPrice >= 0) {
            $maximalPrice = str_replace(',', '.', $maximalPrice);
            $productFilterData->maximalPrice = $maximalPrice;
        }


        $paginationResult = $this->productOnCurrentDomainFacade->getPaginatedProductDetailsForSearch(
            null,
            $productFilterData,
            $orderingModeId,
            $page,
            $page === null ? null : 10
        );

        $lowestPrice = 0;

        $productFilterDataForPriceRange = new ProductFilterData();
        $productFilterDataForPriceRange->brands = $productFilterData->brands;

        $paginationResultPriceAsc = $this->productOnCurrentDomainFacade->getPaginatedProductDetailsForSearch(
            null,
            $productFilterDataForPriceRange,
            'price_asc',
            null,
            1
        );




        if(count($paginationResultPriceAsc->getResults()) > 0) {
            $lowestPrice = $paginationResultPriceAsc->getResults()[0]->getSellingPrice()->getPriceWithVat();
        }

        $highestPrice = 0;

        $paginationResultPriceDesc = $this->productOnCurrentDomainFacade->getPaginatedProductDetailsForSearch(
            null,
            $productFilterDataForPriceRange,
            'price_desc',
            null,
            1,
            1
        );

        if(count($paginationResultPriceDesc->getResults()) > 0) {
            $highestPrice = $paginationResultPriceDesc->getResults()[0]->getSellingPrice()->getPriceWithVat();
        }

        $response['count'] = $paginationResult->getTotalCount();
        $response['pageCount'] = $paginationResult->getPageCount();
        $response['lowestPrice'] = $lowestPrice;
        $response['highestPrice'] = $highestPrice;

        $response['products'] = $this->convertToArrayFormat($paginationResult->getResults());




        return new JsonResponse($response);

    }

    public function listProductsByCategoryAction(Request $request, $id)
    {

        $response = [];
        $category = $this->categoryFacade->getVisibleOnDomainById($this->domain->getId(), $id);

        $requestPage = $request->get('page');
        $minimalPrice = $request->get('minimalPrice');
        $maximalPrice = $request->get('maximalPrice');
        $brandsToFilter = $request->get('brands');
        $page = $requestPage === null ? 1 : (int)$requestPage;

        $orderingModeId = $request->get('orderBy');

        if (!in_array($orderingModeId, ProductController::ORDER_MODELS, true)) {
            $orderingModeId = ProductController::ORDER_MODELS[0];
        }

        $productFilterData = new ProductFilterData();

        $productFilterConfig = $this->createProductFilterConfigForCategory($category);
        $filterForm = $this->createForm(ProductFilterFormType::class, $productFilterData, [
            'product_filter_config' => $productFilterConfig,
        ]);

        $filterForm->handleRequest($request);

        if($minimalPrice != '') {
            $minimalPrice = str_replace(',', '.', $minimalPrice);
            $productFilterData->minimalPrice = $minimalPrice;
        }

        if($maximalPrice != '' && $maximalPrice >= 0) {
            $maximalPrice = str_replace(',', '.', $maximalPrice);
            $productFilterData->maximalPrice = $maximalPrice;
        }

        if($brandsToFilter != '') {
            $brandIds = explode(',', $brandsToFilter);
            foreach ($brandIds as $brandId) {
                $brand = $this->brandFacade->getById($brandId);
                if($brand)
                    $productFilterData->brands[] = $brand;
            }
        }

        $paginationResult = $this->productOnCurrentDomainFacade->getPaginatedProductDetailsInCategory(
            $productFilterData,
            $orderingModeId,
            $page,
            $page === null ? null : 10,
            $id
        );

        $lowestPrice = 0;

        $productFilterDataForPriceRange = new ProductFilterData();
        $productFilterDataForPriceRange->brands = $productFilterData->brands;

        $paginationResultPriceAsc = $this->productOnCurrentDomainFacade->getPaginatedProductDetailsInCategory(
            $productFilterDataForPriceRange,
            'price_asc',
            null,
            1,
            $id
        );

        $productFilterDataForBrands = clone($productFilterData);
        $productFilterDataForBrands->brands = [];

        $brands = $this->productOnCurrentDomainFacade->getBrandsInCategory(
            $productFilterDataForBrands,
            'price_asc',
            null,
            null,
            $id
        );



        if(count($paginationResultPriceAsc->getResults()) > 0) {
            $lowestPrice = $paginationResultPriceAsc->getResults()[0]->getSellingPrice()->getPriceWithVat();
        }

        $highestPrice = 0;

        $paginationResultPriceDesc = $this->productOnCurrentDomainFacade->getPaginatedProductDetailsInCategory(
            $productFilterDataForPriceRange,
            'price_desc',
            null,
            1,
            $id
        );

        if(count($paginationResultPriceDesc->getResults()) > 0) {
            $highestPrice = $paginationResultPriceDesc->getResults()[0]->getSellingPrice()->getPriceWithVat();
        }

        $response['count'] = $paginationResult->getTotalCount();
        $response['pageCount'] = $paginationResult->getPageCount();
        $response['lowestPrice'] = $lowestPrice;
        $response['highestPrice'] = $highestPrice;
        $response['brands'] = [];

        foreach ($brands->getResults() as $brand) {
            $response['brands'][$brand->getId()] = $brand->getName();
        }

        $response['products'] = $this->convertToArrayFormat($paginationResult->getResults());




        return new JsonResponse($response);
    }

    private function createProductFilterConfigForCategory(Category $category)
    {
        return $this->productFilterConfigFactory->createForCategory(
            $this->domain->getId(),
            $this->domain->getLocale(),
            $category
        );
    }

    private function createProductFilterConfig($keyword = null) {
        return $this->productFilterConfigFactory->createForSearch(
            $this->domain->getId(),
            $this->domain->getLocale(),
            $keyword
        );
    }


    public function bestSellingAction()
    {

        $data=[];
        foreach ($this->productRepository->getBest() as $productDetail) {
            $getbyid=$this->productRepository->getId($productDetail['id']);

            foreach($getbyid as $product){

                $images = $this->imageFacade->getAllImagesByEntity($product);
                $imageUrl = '';
                if(count($images) > 0) {
                    try {
                        $imageUrl = $this->imageFacade->getImageUrl($this->domain->getCurrentDomainConfig(), $images[0]);
                    } catch (\Shopsys\ShopBundle\Component\Image\Exception\ImageNotFoundException $e) {
                        $imageUrl = '';
                    }

                }


                $productDeatial = $this->productDetailFactory->getDetailForProduct($product);

                $descriptions = $productDeatial->getProductDomainsIndexedByDomainId();

                if(array_key_exists($this->domain->getCurrentDomainConfig()->getId(), $descriptions)) {
                    $description = $descriptions[$this->domain->getCurrentDomainConfig()->getId()];
                    $description = $description->getDescription();

                } else {
                    $description = '';
                }

                $categories = [];
                $categoriesDomains = $product->getCategoriesIndexedByDomainId();
                if(array_key_exists($this->domain->getCurrentDomainConfig()->getId(), $categoriesDomains)) {
                    foreach($categoriesDomains[$this->domain->getCurrentDomainConfig()->getId()] as $category) {
                        $categories[] = $category->getName();
                    }
                }

                $friendlyUrl = $this->friendlyUrlFacade->findMainFriendlyUrl($this->domain->getCurrentDomainConfig()->getId(), 'front_product_detail' , $product);
                if($friendlyUrl != null) {
                    $url = $this->domain->getCurrentDomainConfig()->getUrl() . '/' . $friendlyUrl->getSlug();
                } else {
                    $url = '';
                }

                $data[] = [
                    'product_id' => $product->getId(),
                    'master_product_id' => $product->getMasterProductId(),
                    'product_part' => $product->getPartNo(),
                    'product_name' => $product->getName(),
                    'product_pid' => $product->getProductPid(),
                    'manufacturer' => $product->getBrand() === null ? '' : $product->getBrand()->getName(),
                    'part_no' => $product->getPartNo(),
                    'price' => $product->getPrice(),
                    'stock_quantity' => $product->getStockQuantity(),
                    'image_url' => $imageUrl,
                    'description' => $description,
                    'url' => $url,
                    'active' => !$product->isHidden(),
                    'categories' => $categories,
                    'eans' => [$product->getEan()],


                ];

            }

        }

        return new JsonResponse($data);

    }

    private function convertToArrayFormat($results) {
        $resultsArray = [];

        foreach($results as $result) {
            $product = $result->getProduct();
            $data = [];
            $data['id'] = $product->getId();
            $data['name'] = $product->getName();
            $data['price'] = $result->getSellingPrice()->getPriceWithVat();
            $data['ean'] = $product->getEan();
            $data['partNo'] = $product->getPartno();
            $data['url'] = '';
            $data['images'] = [];
            $data['parameters'] = [];

            $friendlyUrl = $this->friendlyUrlFacade->findMainFriendlyUrl($this->domain->getCurrentDomainConfig()->getId(), 'front_product_detail' , $product);
            if($friendlyUrl != null) {
                $data['url'] = $this->domain->getCurrentDomainConfig()->getUrl() . '/' . $friendlyUrl->getSlug();
            }

            $images = $this->imageFacade->getAllImagesByEntity($product);
            foreach ($images as $image) {

                try {
                    $url = $this->imageFacade->getImageUrl($this->domain->getCurrentDomainConfig(), $image);
                } catch (\Shopsys\ShopBundle\Component\Image\Exception\ImageNotFoundException $e) {
                    $url = 'URL NOT FUND';
                }

                $data['images'][] = ['id' => $image->getId(), 'extension' => $image->getExtension(), 'url' => $url];
                if($url != 'URL NOT FUND') {
                    break;

                }
            }

            $descriptions = $result->getProductDomainsIndexedByDomainId();

            // $parameters = $result->getParameters();

            if($product->getBrand() != null) {
                $data['parameters'][]['Brand'] = $product->getBrand()->getName();
            }

            // foreach ($parameters as $parameter) {
            //     $data['parameters'][] = [ $parameter->getParameter()->getName()  => $parameter->getValue()->getText()];
            // }





            if(array_key_exists($this->domain->getCurrentDomainConfig()->getId(), $descriptions)) {
                $description = $descriptions[$this->domain->getCurrentDomainConfig()->getId()];
                $data['productDomain'] = [
                    'short_description' => $description->getShortDescription(),
                    'description' => $description->getDescription(),
                    'product_feature' => $description->getProductFeature(),
                    'product_special' => $description->getProductSpecial(),
                    'product_on_sale' => $description->getProductOnSale()

                ];
            } else {
                $data['productDomain'] = [];
            }



            $resultsArray[] = $data;

        }

        return $resultsArray;
    }

    public function updateRatings() {
        set_time_limit(0);




        $client = new \GuzzleHttp\Client();

        $response = $client->get('http://138.68.133.194/masterProducts/getRatings');

        foreach (json_decode($response->getBody()->getContents()) as $key => $value) {
            $product = $this->productFacade->findByMasterProductId($key);

            if($product != null) {
                $this->productFacade->updateRating($product->getId(), (float) $value);
            }
        }

        return new JsonResponse(['success' => $response->getBody()]);
    }


    public function updatePricesAction(Request $request) {
        set_time_limit(0);
        $success = true;
        $message = '';
        $notUpdatedProducts = [];

        $productsWithPrices = json_decode($request->get('products'));

        // try {

        $params = [];

        foreach ($productsWithPrices as $productWithPrice) {
            $product = $this->productFacade->findByMasterProductId($productWithPrice->master_product_id);
            if($product != null) {

                if(isset($productWithPrice->price)) {
                    $params['price'] = $productWithPrice->price;
                }

                if(isset($productWithPrice->quantity)) {
                    $params['stockQuantity'] = $productWithPrice->quantity;
                }

                if(isset($productWithPrice->buy_price)) {
                    $params['productBuyPrice'] = $productWithPrice->buy_price;
                }

                if(isset($productWithPrice->supplier)) {
                    $params['productSource'] = $productWithPrice->supplier;
                }

                $this->productFacade->updatePriceQuantityForProduct($product->getId(), $params);

            } else {
                $notUpdatedProducts[] = $productWithPrice->master_product_id;
            }
        }


        // foreach ($productsWithPrices as $productWithPrice) {
        //     $product = $this->productFacade->findByMasterProductId($productWithPrice->master_product_id);
        //     if($product != null) {
        //         $productEditData = $this->productEditDataFactory->createFromProduct($product);

        //         if(isset($productWithPrice->price)) {
        //             $productEditData->productData->price = $productWithPrice->price;
        //         }

        //         if(isset($productWithPrice->quantity)) {
        //             $productEditData->productData->stockQuantity = $productWithPrice->quantity;
        //         }

        //         if(isset($productWithPrice->buy_price)) {
        //             $productEditData->productData->productBuyPrice = $productWithPrice->buy_price;
        //         }

        //         if(isset($productWithPrice->supplier)) {
        //             $productEditData->productData->productSource = $productWithPrice->supplier;
        //         }

        //         $this->productFacade->edit($product->getId(), $productEditData);
        //     } else {
        //         $notUpdatedProducts[] = $productWithPrice->master_product_id;
        //     }
        // }
        // } catch (\Exception $e) {
        //     $message = $e->getMessage();
        //     $success = false;
        // }


        return new JsonResponse(['success' => $success, 'not_updated_products' => $notUpdatedProducts, 'message' => $message]);
    }
}