<?php

namespace Shopsys\ShopBundle\Controller\Front;

use Nette\Utils\Json;
use Shopsys\ShopBundle\Component\Controller\FrontBaseController;
use Shopsys\ShopBundle\Component\Domain\Domain;
use Shopsys\ShopBundle\Form\Front\Customer\CustomerFormType;
use Shopsys\ShopBundle\Form\Front\WarrantyClaim\WarrantyClaimForm;
use Shopsys\ShopBundle\Model\Country\CountryFacade;
use Shopsys\ShopBundle\Form\Front\Customer\CustomerGdprFormType;
use Shopsys\ShopBundle\Model\Customer\CustomerData;
use Shopsys\ShopBundle\Model\Customer\CustomerFacade;
use Shopsys\ShopBundle\Model\Order\Item\OrderItemFacade;
use Shopsys\ShopBundle\Model\Order\Item\OrderItemPriceCalculation;
use Shopsys\ShopBundle\Model\Order\OrderFacade;
use Shopsys\ShopBundle\Model\Payment\PaymentFacade;
use Shopsys\ShopBundle\Model\Security\LoginAsUserFacade;
use Shopsys\ShopBundle\Model\Security\Roles;
use Shopsys\ShopBundle\Model\Transport\TransportFacade;
use Shopsys\ShopBundle\Model\WarrantyClaim\WarrantyData;
use Shopsys\ShopBundle\Model\WarrantyClaim\WarrantyClaimFacade;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use GuzzleHttp\Client;

class CustomerController extends FrontBaseController
{
    /**
     * @var \Shopsys\ShopBundle\Model\Customer\CustomerFacade
     */
    private $customerFacade;

    /**
     * @var \Shopsys\ShopBundle\Component\Domain\Domain
     */
    private $domain;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\Item\OrderItemPriceCalculation
     */
    private $orderItemPriceCalculation;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\OrderFacade
     */
    private $orderFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Security\LoginAsUserFacade
     */
    private $loginAsUserFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\WarrantyClaim\WarrantyClaimFacade
     */
    private $warrantyClaimFacade;

    private $paymentFacade;

    private $transportFacade;
    private $countryFacade;
    private $orderItemFacade;

    private $warrantyClaimRegisterUrl = "http://orders.turbado.eu/api/warranty/store/external";

    public function __construct(
        CustomerFacade $customerFacade,
        OrderFacade $orderFacade,
        Domain $domain,
        OrderItemPriceCalculation $orderItemPriceCalculation,
        LoginAsUserFacade $loginAsUserFacade,
        WarrantyClaimFacade $warrantyClaimFacade,
        PaymentFacade $paymentFacade,
        TransportFacade $transportFacade,
        CountryFacade $countryFacade,
        OrderItemFacade $orderItemFacade
    )
    {
        $this->customerFacade = $customerFacade;
        $this->orderFacade = $orderFacade;
        $this->domain = $domain;
        $this->orderItemPriceCalculation = $orderItemPriceCalculation;
        $this->loginAsUserFacade = $loginAsUserFacade;
        $this->warrantyClaimFacade = $warrantyClaimFacade;
        $this->paymentFacade = $paymentFacade;
        $this->transportFacade = $transportFacade;
        $this->countryFacade = $countryFacade;
        $this->orderItemFacade = $orderItemFacade;
    }

    public function personalDataSettingsAction(Request $request)
    {


        //  echo '<pre>',print_r($request,2),'</pre>';
        //  exit();

        if (!$this->isGranted(Roles::ROLE_CUSTOMER)) {
            $this->getFlashMessageSender()->addErrorFlash(t('You have to be logged in to enter this page'));
            return $this->redirectToRoute('front_login');
        }

//        $user = $this->getUser();
//        $customerData = new CustomerData();
//        $customerData->setFromEntity($user);
//
        $form = $this->createForm(CustomerGdprFormType::class, Array(), [
            'domain_id' => $this->domain->getId(),
        ]);

//        $form->handleRequest($request);
//
//        if ($form->isValid()) {
//            $customerData = $form->getData();
//
//            $this->customerFacade->editByCustomer($user->getId(), $customerData);
//
//            $this->getFlashMessageSender()->addSuccessFlash(t('Your data had been successfully updated'));
//            return $this->redirectToRoute('front_customer_edit');
//        }
//
//        if ($form->isSubmitted() && !$form->isValid()) {
//            $this->getFlashMessageSender()->addErrorFlash(t('Please check the correctness of all data filled.'));
//        }

        return $this->render('@ShopsysShop/Front/Content/Customer/personalDataSettings.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    public function editAction(Request $request)
    {
        if (!$this->isGranted(Roles::ROLE_CUSTOMER)) {
            $this->getFlashMessageSender()->addErrorFlash(t('You have to be logged in to enter this page'));
            return $this->redirectToRoute('front_login');
        }

        $user = $this->getUser();
        $customerData = new CustomerData();
        $customerData->setFromEntity($user);

        $form = $this->createForm(CustomerFormType::class, $customerData, [
            'domain_id' => $this->domain->getId(),
        ]);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $customerData = $form->getData();

            $this->customerFacade->editByCustomer($user->getId(), $customerData);

            $this->getFlashMessageSender()->addSuccessFlash(t('Your data had been successfully updated'));
            return $this->redirectToRoute('front_customer_edit');
        }

        if ($form->isSubmitted() && !$form->isValid()) {
            $this->getFlashMessageSender()->addErrorFlash(t('Please check the correctness of all data filled.'));
        }

        return $this->render($this->getTemplate()->getTemplatePath('/Front/Content/Customer/edit.html.twig'), [
            'form' => $form->createView(),
        ]);
    }

    public function ordersAction()
    {
        if (!$this->isGranted(Roles::ROLE_CUSTOMER)) {
            $this->getFlashMessageSender()->addErrorFlash(t('You have to be logged in to enter this page'));
            return $this->redirectToRoute('front_login');
        }

        $user = $this->getUser();
        /* @var $user \Shopsys\ShopBundle\Model\Customer\User */

        $orders = $this->orderFacade->getCustomerOrderList($user);
        return $this->render($this->getTemplate()->getTemplatePath('/Front/Content/Customer/orders.html.twig'), [
            'orders' => $orders,
        ]);
    }

    public function warrantiesAction(Request $request)
    {
        /*        if (!$this->isGranted(Roles::ROLE_CUSTOMER)) {
                    $this->getFlashMessageSender()->addErrorFlash(t('You have to be logged in to enter this page'));
                    return $this->redirectToRoute('front_login');
                }*/

        $user = $this->getUser();
        /* @var $user \Shopsys\ShopBundle\Model\Customer\User */
        $warranties = $this->customerFacade->getWarranties($user->getId());
        return $this->render('@ShopsysShop/Front/Content/Customer/warranties.html.twig', [
            'warranties' => $warranties,
        ]);
    }

    public function warrantyDetailsAction($warrantyNumber = null)
    {
        /*
                if (!$this->isGranted(Roles::ROLE_CUSTOMER)) {
                    $this->getFlashMessageSender()->addErrorFlash(t('You have to be logged in to enter this page'));
                    return $this->redirectToRoute('front_login');
                }*/

        try {
            $warranty = $this->warrantyClaimFacade->getByWarrantyId($warrantyNumber);
        } catch (\Exception $ex) {
            $this->getFlashMessageSender()->addErrorFlash(t('Warranty not found'));
            return $this->redirectToRoute('front_customer_warranties');
        }

        return $this->render('@ShopsysShop/Front/Content/Customer/warrantyDetails.html.twig', [
            'warranty' => $warranty
        ]);
    }

    public function newWarrantyAction(Request $request)
    {
        $orderId = $request->get('orderId');
        $order = $this->orderFacade->getById($orderId);
        $items = $order->getProductItems();

        /*if (!$this->isGranted(Roles::ROLE_CUSTOMER)) {
            $this->getFlashMessageSender()->addErrorFlash(t('You have to be logged in to enter this page'));
            return $this->redirectToRoute('front_login');
        }*/

        $user = $order->getCustomer();
        $warrantyData = new WarrantyData(['customer_form' => $user]);
        $form = $this->createForm(WarrantyClaimForm::class, $warrantyData, [
            'domain_id' => $this->domain->getId(),
            'payment_facade' => $this->paymentFacade,
            'transport_facade' => $this->transportFacade,
            'country_facade' => $this->countryFacade,
            'items' => $items
        ]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $warrantyData = $form->getData();
            $warrantyData->domain_id = $this->domain->getId();
            $warrantyData->customer = $user;
            $warrantyData->status = 'New';
            $warranty = $this->warrantyClaimFacade->create($warrantyData);
            $shopIds = [1 => '20', 2 => '9', 3 => '10', 4 => '7', 5 => '8'];

            if (array_key_exists($warranty->getDomainId(), $shopIds)) {
                $client = new Client();
                $client->post($this->warrantyClaimRegisterUrl, [
                    'json' => [
                        'info' => $this->warrantyClaimFacade->exportToJson($warranty->getId()),
                    ]]);
            }

            $this->getFlashMessageSender()->addSuccessFlash(t('Your warranty was saved!'));
            return $this->redirectToRoute('front_customer_warranty_details', ['warrantyNumber' => $warranty->getId()]);
        }
        return $this->render('@ShopsysShop/Front/Content/Customer/newWarranty.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @param string $orderNumber
     */
    public function orderDetailRegisteredAction($orderNumber)
    {
        return $this->orderDetailAction(null, $orderNumber);
    }

    /**
     * @param string $urlHash
     */
    public function orderDetailUnregisteredAction($urlHash)
    {
        return $this->orderDetailAction($urlHash, null);
    }

    /**
     * @param string $urlHash
     * @param string $orderNumber
     */
    private function orderDetailAction($urlHash = null, $orderNumber = null)
    {
        if ($orderNumber !== null) {
            if (!$this->isGranted(Roles::ROLE_CUSTOMER)) {
                $this->getFlashMessageSender()->addErrorFlash(t('You have to be logged in to enter this page'));
                return $this->redirectToRoute('front_login');
            }

            $user = $this->getUser();
            try {
                $order = $this->orderFacade->getByOrderNumberAndUser($orderNumber, $user);
                /* @var $order \Shopsys\ShopBundle\Model\Order\Order */
            } catch (\Shopsys\ShopBundle\Model\Order\Exception\OrderNotFoundException $ex) {
                $this->getFlashMessageSender()->addErrorFlash(t('Order not found'));
                return $this->redirectToRoute('front_customer_orders');
            }
        } else {
            $order = $this->orderFacade->getByUrlHashAndDomain($urlHash, $this->domain->getId());
            /* @var $order \Shopsys\ShopBundle\Model\Order\Order */
        }

        $orderItemTotalPricesById = $this->orderItemPriceCalculation->calculateTotalPricesIndexedById($order->getItems());

        return $this->render('@ShopsysShop/Front/Content/Customer/orderDetail.html.twig', [
            'order' => $order,
            'orderItemTotalPricesById' => $orderItemTotalPricesById,
        ]);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAsRememberedUserAction(Request $request)
    {
        try {
            $this->loginAsUserFacade->loginAsRememberedUser($request);
        } catch (\Shopsys\ShopBundle\Model\Customer\Exception\UserNotFoundException $e) {
            $adminFlashMessageSender = $this->get('shopsys.shop.component.flash_message.sender.admin');
            /* @var $adminFlashMessageSender \Shopsys\ShopBundle\Component\FlashMessage\FlashMessageSender */
            $adminFlashMessageSender->addErrorFlash(t('User not found.'));

            return $this->redirectToRoute('admin_customer_list');
        } catch (\Shopsys\ShopBundle\Model\Security\Exception\LoginAsRememberedUserException $e) {
            throw $this->createAccessDeniedException('', $e);
        }

        return $this->redirectToRoute('front_homepage');
    }
}
