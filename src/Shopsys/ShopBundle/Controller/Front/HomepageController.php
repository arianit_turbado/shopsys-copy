<?php

namespace Shopsys\ShopBundle\Controller\Front;

use Shopsys\ShopBundle\Component\Controller\FrontBaseController;
use Shopsys\ShopBundle\Component\CustomTemplate\CustomTemplate;
use Shopsys\ShopBundle\Component\Domain\Domain;
use Shopsys\ShopBundle\Model\Customer\CurrentCustomer;
use Shopsys\ShopBundle\Model\Order\OrderFacade;
use Shopsys\ShopBundle\Model\Pricing\Currency\CurrencyFacade;
use Shopsys\ShopBundle\Model\Product\TopProduct\TopProductFacade;
use Shopsys\ShopBundle\Model\Seo\SeoSettingFacade;
use Shopsys\ShopBundle\Model\Slider\SliderItemFacade;
use Shopsys\ShopBundle\Model\Product\ProductRepository;
use Shopsys\ShopBundle\Component\Image\ImageFacade;
use Shopsys\ShopBundle\Model\Product\Detail\ProductDetailFactory;
use Shopsys\ShopBundle\Component\Router\FriendlyUrl\FriendlyUrlFacade;
use Shopsys\ShopBundle\Model\Product\ProductOnCurrentDomainFacade;
use Shopsys\ShopBundle\Model\LegalConditions\LegalConditionsFacade;
use Symfony\Component\HttpFoundation\Request;
use Shopsys\ShopBundle\Model\WarrantyClaim\WarrantyClaimFacade;

class HomepageController extends FrontBaseController
{
    /**
     * @var \Shopsys\ShopBundle\Model\Customer\CurrentCustomer
     */
    private $currentCustomer;

    /**
     * @var \Shopsys\ShopBundle\Model\Product\TopProduct\TopProductFacade
     */
    private $topProductFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Seo\SeoSettingFacade
     */
    private $seoSettingFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Slider\SliderItemFacade
     */
    private $sliderItemFacade;

    /**
     * @var \Shopsys\ShopBundle\Component\Domain\Domain
     */
    private $domain;

    /**
     * @var CurrencyFacade
     */
    private $currencyFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\LegalConditions\LegalConditionsFacade
     */
    private $legalConditionsFacade;


    /**
     * @var \Shopsys\ShopBundle\Model\Product\ProductOnCurrentDomainFacade
     */
    private $productOnCurrentDomainFacade;

    private $orderFacade;

    private $warrantyClaimFacade;

    public function __construct(
        CurrentCustomer $currentCustomer,
        SliderItemFacade $sliderItemFacade,
        TopProductFacade $topProductsFacade,
        SeoSettingFacade $seoSettingFacade,
        ProductRepository $productRepository,
        ImageFacade $imageFacade,
        ProductDetailFactory $productDetailFactory,
        FriendlyUrlFacade $friendlyUrlFacade,
        Domain $domain,
        CurrencyFacade $currencyFacade,
        ProductOnCurrentDomainFacade $productOnCurrentDomainFacade,
        LegalConditionsFacade $legalConditionsFacade,
        OrderFacade $orderFacade,
        WarrantyClaimFacade $warrantyClaimFacade
    )
    {
        $this->currentCustomer = $currentCustomer;
        $this->sliderItemFacade = $sliderItemFacade;
        $this->topProductFacade = $topProductsFacade;
        $this->seoSettingFacade = $seoSettingFacade;
        $this->productRepository = $productRepository;
        $this->imageFacade = $imageFacade;
        $this->productDetailFactory = $productDetailFactory;
        $this->friendlyUrlFacade = $friendlyUrlFacade;
        $this->domain = $domain;
        $this->currencyFacade = $currencyFacade;
        $this->productOnCurrentDomainFacade = $productOnCurrentDomainFacade;
        $this->legalConditionsFacade = $legalConditionsFacade;
        $this->orderFacade = $orderFacade;
        $this->warrantyClaimFacade = $warrantyClaimFacade;
    }

    public function indexAction()
    {
        $sliderItems = $this->sliderItemFacade->getAllVisibleOnCurrentDomain();
        $topProductsDetails = $this->topProductFacade->getAllOfferedProductDetails(
            $this->domain->getId(),
            $this->currentCustomer->getPricingGroup()
        );


        //bestSelling FOr a Week

        $domainId = $this->domain->getId();


        $data = [];
        foreach ($this->productRepository->getBest($domainId) as $productDetail) {
            $getbyid = $this->productRepository->getId($productDetail['id']);


            $productDetails = $this->productOnCurrentDomainFacade->getVisibleProductDetailById($productDetail['id']);

            foreach ($getbyid as $product) {

                $images = $this->imageFacade->getAllImagesByEntity($product);
                $imageUrl = '';
                if (count($images) > 0) {
                    try {
                        $imageUrl = $this->imageFacade->getImageUrl($this->domain->getCurrentDomainConfig(), $images[0]);
                    } catch (\Shopsys\ShopBundle\Component\Image\Exception\ImageNotFoundException $e) {
                        $imageUrl = '';
                    }

                }


                $productDeatial = $this->productDetailFactory->getDetailForProduct($product);

                $descriptions = $productDeatial->getProductDomainsIndexedByDomainId();

                if (array_key_exists($this->domain->getCurrentDomainConfig()->getId(), $descriptions)) {
                    $description = $descriptions[$this->domain->getCurrentDomainConfig()->getId()];
                    $description = $description->getDescription();

                } else {
                    $description = '';
                }

                $categories = [];
                $categoriesDomains = $product->getCategoriesIndexedByDomainId();
                if (array_key_exists($this->domain->getCurrentDomainConfig()->getId(), $categoriesDomains)) {
                    foreach ($categoriesDomains[$this->domain->getCurrentDomainConfig()->getId()] as $category) {
                        $categories[] = $category->getName();
                    }
                }

                $friendlyUrl = $this->friendlyUrlFacade->findMainFriendlyUrl($this->domain->getCurrentDomainConfig()->getId(), 'front_product_detail', $product);
                if ($friendlyUrl != null) {
                    $url = $this->domain->getCurrentDomainConfig()->getUrl() . '/' . $friendlyUrl->getSlug();
                } else {
                    $url = '';
                }

                $data[] = [
                    'product_id' => $product->getId(),
                    'master_product_id' => $product->getMasterProductId(),
                    'product_part' => $product->getPartNo(),
                    'product_name' => $product->getName(),
                    'product_pid' => $product->getProductPid(),
                    'manufacturer' => $product->getBrand() === null ? '' : $product->getBrand()->getName(),
                    'part_no' => $product->getPartNo(),
                    'price' => $product->getPrice(),
                    'stock_quantity' => $product->getStockQuantity(),
                    'image_url' => $imageUrl,
                    'description' => $description,
                    'url' => $url,
                    'active' => !$product->isHidden(),
                    'categories' => $categories,
                    'eans' => [$product->getEan()],
                    'product_entity' => $product,
                    'productDetail' => $productDetails
                ];

            }

        }

        //Featured Products

        $data_feature = [];
        foreach ($this->productRepository->getFeature($domainId) as $productDetail) {

            $getbyid = $this->productRepository->getId($productDetail->getProductId(), $domainId);


            $productDetails = $this->productOnCurrentDomainFacade->getVisibleProductDetail($productDetail->getProductId());


            foreach ($getbyid as $product) {

                $images = $this->imageFacade->getAllImagesByEntity($product);
                $imageUrl = '';
                if (count($images) > 0) {
                    try {
                        $imageUrl = $this->imageFacade->getImageUrl($this->domain->getCurrentDomainConfig(), $images[0]);
                    } catch (\Shopsys\ShopBundle\Component\Image\Exception\ImageNotFoundException $e) {
                        $imageUrl = '';
                    }

                }


                $productDeatial = $this->productDetailFactory->getDetailForProduct($product);

                $descriptions = $productDeatial->getProductDomainsIndexedByDomainId();

                if (array_key_exists($this->domain->getCurrentDomainConfig()->getId(), $descriptions)) {
                    $description = $descriptions[$this->domain->getCurrentDomainConfig()->getId()];
                    $description = $description->getDescription();

                } else {
                    $description = '';
                }

                $categories = [];
                $categoriesDomains = $product->getCategoriesIndexedByDomainId();
                if (array_key_exists($this->domain->getCurrentDomainConfig()->getId(), $categoriesDomains)) {
                    foreach ($categoriesDomains[$this->domain->getCurrentDomainConfig()->getId()] as $category) {
                        $categories[] = $category->getName();
                    }
                }

                $friendlyUrl = $this->friendlyUrlFacade->findMainFriendlyUrl($this->domain->getCurrentDomainConfig()->getId(), 'front_product_detail', $product);
                if ($friendlyUrl != null) {
                    $url = $this->domain->getCurrentDomainConfig()->getUrl() . '/' . $friendlyUrl->getSlug();
                } else {
                    $url = '';
                }

                $data_feature[] = [
                    'product_id' => $product->getId(),
                    'master_product_id' => $product->getMasterProductId(),
                    'product_part' => $product->getPartNo(),
                    'product_name' => $product->getName(),
                    'product_pid' => $product->getProductPid(),
                    'manufacturer' => $product->getBrand() === null ? '' : $product->getBrand()->getName(),
                    'part_no' => $product->getPartNo(),
                    'price' => $product->getPrice(),
                    'stock_quantity' => $product->getStockQuantity(),
                    'image_url' => $imageUrl,
                    'description' => $description,
                    'url' => $url,
                    'active' => !$product->isHidden(),
                    'categories' => $categories,
                    'eans' => [$product->getEan()],
                    'product_entity' => $product,
                    'productDetail' => $productDetails
                ];

            }

        }

        //Special Products

        $data_special = [];
        foreach ($this->productRepository->getSpecial($domainId) as $productDetail) {

            $getbyid = $this->productRepository->getId($productDetail->getProductId(), $domainId);
            $productDetails = $this->productOnCurrentDomainFacade->getVisibleProductDetail($productDetail->getProductId());
            foreach ($getbyid as $product) {

                $images = $this->imageFacade->getAllImagesByEntity($product);
                $imageUrl = '';
                if (count($images) > 0) {
                    try {
                        $imageUrl = $this->imageFacade->getImageUrl($this->domain->getCurrentDomainConfig(), $images[0]);
                    } catch (\Shopsys\ShopBundle\Component\Image\Exception\ImageNotFoundException $e) {
                        $imageUrl = '';
                    }

                }


                $productDeatial = $this->productDetailFactory->getDetailForProduct($product);

                $descriptions = $productDeatial->getProductDomainsIndexedByDomainId();

                if (array_key_exists($this->domain->getCurrentDomainConfig()->getId(), $descriptions)) {
                    $description = $descriptions[$this->domain->getCurrentDomainConfig()->getId()];
                    $description = $description->getDescription();

                } else {
                    $description = '';
                }

                $categories = [];
                $categoriesDomains = $product->getCategoriesIndexedByDomainId();
                if (array_key_exists($this->domain->getCurrentDomainConfig()->getId(), $categoriesDomains)) {
                    foreach ($categoriesDomains[$this->domain->getCurrentDomainConfig()->getId()] as $category) {
                        $categories[] = $category->getName();
                    }
                }

                $friendlyUrl = $this->friendlyUrlFacade->findMainFriendlyUrl($this->domain->getCurrentDomainConfig()->getId(), 'front_product_detail', $product);
                if ($friendlyUrl != null) {
                    $url = $this->domain->getCurrentDomainConfig()->getUrl() . '/' . $friendlyUrl->getSlug();
                } else {
                    $url = '';
                }

                $data_special[] = [
                    'product_id' => $product->getId(),
                    'master_product_id' => $product->getMasterProductId(),
                    'product_part' => $product->getPartNo(),
                    'product_name' => $product->getName(),
                    'product_pid' => $product->getProductPid(),
                    'manufacturer' => $product->getBrand() === null ? '' : $product->getBrand()->getName(),
                    'part_no' => $product->getPartNo(),
                    'price' => $product->getPrice(),
                    'stock_quantity' => $product->getStockQuantity(),
                    'image_url' => $imageUrl,
                    'description' => $description,
                    'url' => $url,
                    'active' => !$product->isHidden(),
                    'categories' => $categories,
                    'eans' => [$product->getEan()],
                    'product_entity' => $product,
                    'productDetail' => $productDetails
                ];

            }

        }

        //On_sale Products

        $data_on_sale = [];
        foreach ($this->productRepository->getOnSale($domainId) as $productDetail) {

            $getbyid = $this->productRepository->getId($productDetail->getProductId(), $domainId);
            $productDetails = $this->productOnCurrentDomainFacade->getVisibleProductDetail($productDetail->getProductId());
            foreach ($getbyid as $product) {

                $images = $this->imageFacade->getAllImagesByEntity($product);
                $imageUrl = '';
                if (count($images) > 0) {
                    try {
                        $imageUrl = $this->imageFacade->getImageUrl($this->domain->getCurrentDomainConfig(), $images[0]);
                    } catch (\Shopsys\ShopBundle\Component\Image\Exception\ImageNotFoundException $e) {
                        $imageUrl = '';
                    }

                }


                $productDeatial = $this->productDetailFactory->getDetailForProduct($product);

                $descriptions = $productDeatial->getProductDomainsIndexedByDomainId();

                if (array_key_exists($this->domain->getCurrentDomainConfig()->getId(), $descriptions)) {
                    $description = $descriptions[$this->domain->getCurrentDomainConfig()->getId()];
                    $description = $description->getDescription();

                } else {
                    $description = '';
                }

                $categories = [];
                $categoriesDomains = $product->getCategoriesIndexedByDomainId();
                if (array_key_exists($this->domain->getCurrentDomainConfig()->getId(), $categoriesDomains)) {
                    foreach ($categoriesDomains[$this->domain->getCurrentDomainConfig()->getId()] as $category) {
                        $categories[] = $category->getName();
                    }
                }

                $friendlyUrl = $this->friendlyUrlFacade->findMainFriendlyUrl($this->domain->getCurrentDomainConfig()->getId(), 'front_product_detail', $product);
                if ($friendlyUrl != null) {
                    $url = $this->domain->getCurrentDomainConfig()->getUrl() . '/' . $friendlyUrl->getSlug();
                } else {
                    $url = '';
                }

                $data_on_sale[] = [
                    'product_id' => $product->getId(),
                    'master_product_id' => $product->getMasterProductId(),
                    'product_part' => $product->getPartNo(),
                    'product_name' => $product->getName(),
                    'product_pid' => $product->getProductPid(),
                    'manufacturer' => $product->getBrand() === null ? '' : $product->getBrand()->getName(),
                    'part_no' => $product->getPartNo(),
                    'price' => $product->getPrice(),
                    'stock_quantity' => $product->getStockQuantity(),
                    'image_url' => $imageUrl,
                    'description' => $description,
                    'url' => $url,
                    'active' => !$product->isHidden(),
                    'categories' => $categories,
                    'eans' => [$product->getEan()],
                    'product_entity' => $product,
                    'productDetail' => $productDetails
                ];

            }

        }

        return $this->render($this->getTemplate()->getTemplatePath('/Front/Content/Default/index.html.twig'), [
            'sliderItems' => $sliderItems,
            'topProductsDetails' => $topProductsDetails,
            'topSelling' => $data,
            'feature' => $data_feature,
            'special' => $data_special,
            'on_sale' => $data_on_sale,
            'title' => $this->seoSettingFacade->getTitleMainPage($this->domain->getId()),
            'metaDescription' => $this->seoSettingFacade->getDescriptionMainPage($this->domain->getId()),
            'activeCurrency' => $this->currencyFacade->getDomainDefaultCurrencyByDomainId($this->domain->getId())->getCode()
        ]);
    }

    public function gdprAction()
    {

        return $this->render('@ShopsysShop/Front/Content/Default/gdpr.html.twig', [
            'title' => $this->seoSettingFacade->getTitleMainPage($this->domain->getId()),
            'metaDescription' => $this->seoSettingFacade->getDescriptionMainPage($this->domain->getId()),
            'links' => Array(1 => 'privacy policy', 2 => 'conditions'),
            'termsAndConditionsArticle' => $this->legalConditionsFacade->findTermsAndConditions($this->domain->getId()),
            'privacyPolicyArticle' => $this->legalConditionsFacade->findPrivacyPolicy($this->domain->getId()),
        ]);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function rmaAction(Request $request)
    {
        // new RMA
        if ($request->get('rmaType') == 1) {
            echo 'request new RMA';
            $orderNumber = $request->get('rmaOrder');
            $email = $request->get('rmaEmail');
            try {
                if ($email == '' || $orderNumber == '') {
                    throw new \Exception('Please provide e-mail and order number');
                }
                $order = $this->orderFacade->getByOrderNumber($orderNumber);
                if ($order->getEmail() != $email) {
                    throw new \Exception("No order with number: {$orderNumber} and email: {$email} was found");
                }
                return $this->redirectToRoute('front_customer_new_warranty', ['orderId' => $order->getId()]);

            } catch (\Exception $ex) {
                $request->getSession()
                    ->getFlashBag()
                    ->add('error', $ex->getMessage());

                return $this->render('@ShopsysShop/Front/Content/Rma/rma.html.twig', [
                    'title' => $this->seoSettingFacade->getTitleMainPage($this->domain->getId()),
                    'metaDescription' => $this->seoSettingFacade->getDescriptionMainPage($this->domain->getId()),
                    'links' => Array(1 => 'privacy policy', 2 => 'conditions'),
                    'termsAndConditionsArticle' => $this->legalConditionsFacade->findTermsAndConditions($this->domain->getId()),
                    'privacyPolicyArticle' => $this->legalConditionsFacade->findPrivacyPolicy($this->domain->getId()),
                ]);
            }
        };

        // existing RMA
        if ($request->get('rmaType') == 2) {
            try{
                $warranty = $this->warrantyClaimFacade->getByWarrantyNumber($request->get('rmaNumber'));
                return $this->redirectToRoute('front_customer_warranty_details', ['warrantyNumber' => $warranty->getId()]);
            } catch (\Exception $ex){
                $request->getSession()
                    ->getFlashBag()
                    ->add('error', $ex->getMessage());

                return $this->render('@ShopsysShop/Front/Content/Rma/rma.html.twig', [
                    'title' => $this->seoSettingFacade->getTitleMainPage($this->domain->getId()),
                    'metaDescription' => $this->seoSettingFacade->getDescriptionMainPage($this->domain->getId()),
                    'links' => Array(1 => 'privacy policy', 2 => 'conditions'),
                    'termsAndConditionsArticle' => $this->legalConditionsFacade->findTermsAndConditions($this->domain->getId()),
                    'privacyPolicyArticle' => $this->legalConditionsFacade->findPrivacyPolicy($this->domain->getId()),
                ]);
            }
        };

        return $this->render('@ShopsysShop/Front/Content/Rma/rma.html.twig', [
            'title' => $this->seoSettingFacade->getTitleMainPage($this->domain->getId()),
            'metaDescription' => $this->seoSettingFacade->getDescriptionMainPage($this->domain->getId()),
            'links' => Array(1 => 'privacy policy', 2 => 'conditions'),
            'termsAndConditionsArticle' => $this->legalConditionsFacade->findTermsAndConditions($this->domain->getId()),
            'privacyPolicyArticle' => $this->legalConditionsFacade->findPrivacyPolicy($this->domain->getId()),
        ]);
    }
}
