<?php

namespace Shopsys\ShopBundle\Controller\Front;

use Shopsys\ShopBundle\Component\Controller\ErrorService;
use Shopsys\ShopBundle\Component\Controller\FrontBaseController;
use Shopsys\ShopBundle\Component\Domain\Domain;
use Shopsys\ShopBundle\Form\Front\Cart\AddProductFormType;
use Shopsys\ShopBundle\Form\Front\Cart\AddProductSmallFormType;
use Shopsys\ShopBundle\Form\Front\Cart\CartFormType;
use Shopsys\ShopBundle\Model\Cart\AddProductResult;
use Shopsys\ShopBundle\Model\Cart\CartFacade;
use Shopsys\ShopBundle\Model\Customer\CurrentCustomer;
use Shopsys\ShopBundle\Model\Module\ModuleList;
use Shopsys\ShopBundle\Model\Order\Preview\OrderPreviewFactory;
use Shopsys\ShopBundle\Model\Product\Accessory\ProductAccessoryFacade;
use Shopsys\ShopBundle\Model\Product\Detail\ProductDetailFactory;
use Shopsys\ShopBundle\Model\Product\Product;
use Shopsys\ShopBundle\Model\TransportAndPayment\FreeTransportAndPaymentFacade;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Csrf\CsrfToken;
use Shopsys\ShopBundle\Model\Pricing\Group\PricingGroupSettingFacade;
use Shopsys\ShopBundle\Model\Pricing\Group\PricingGroupRepository;
use Shopsys\ShopBundle\Model\Customer\User;

use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class CartController extends FrontBaseController
{
    const AFTER_ADD_WINDOW_ACCESSORIES_LIMIT = 3;

    const RECALCULATE_ONLY_PARAMETER_NAME = 'recalculateOnly';

    /**
     * @var \Shopsys\ShopBundle\Model\Cart\CartFacade
     */
    private $cartFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Customer\CurrentCustomer
     */
    private $currentCustomer;

    /**
     * @var \Shopsys\ShopBundle\Component\Domain\Domain
     */
    private $domain;

    /**
     * @var \Shopsys\ShopBundle\Model\Product\Accessory\ProductAccessoryFacade
     */
    private $productAccessoryFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Product\Detail\ProductDetailFactory
     */
    private $productDetailFactory;

    /**
     * @var \Shopsys\ShopBundle\Model\TransportAndPayment\FreeTransportAndPaymentFacade
     */
    private $freeTransportAndPaymentFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\Preview\OrderPreviewFactory
     */
    private $orderPreviewFactory;

    /**
     * @var \Shopsys\ShopBundle\Model\Pricing\Group\PricingGroupSettingFacade
     */
    private $pricingGroupSettingFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Product\Pricing\ProductCalculatedPriceRepository
     */
    private $productCalculatedPriceRepository;

    /**
     * @var \Shopsys\ShopBundle\Component\Controller\ErrorService
     */
    private $errorService;

    /**
     * @var \Shopsys\ShopBundle\Model\Customer\User
     */
    private $user;

    public function __construct(
        ProductAccessoryFacade $productAccessoryFacade,
        CartFacade $cartFacade,
        CurrentCustomer $currentCustomer,
        Domain $domain,
        FreeTransportAndPaymentFacade $freeTransportAndPaymentFacade,
        ProductDetailFactory $productDetailFactory,
        OrderPreviewFactory $orderPreviewFactory,
        ErrorService $errorService,
        PricingGroupSettingFacade $pricingGroupSettingFacade,
        PricingGroupRepository $pricingGroupRepository,
        User $user
    )
    {
        $this->productAccessoryFacade = $productAccessoryFacade;
        $this->cartFacade = $cartFacade;
        $this->currentCustomer = $currentCustomer;
        $this->domain = $domain;
        $this->freeTransportAndPaymentFacade = $freeTransportAndPaymentFacade;
        $this->productDetailFactory = $productDetailFactory;
        $this->orderPreviewFactory = $orderPreviewFactory;
        $this->errorService = $errorService;

        $this->pricingGroupSettingFacade = $pricingGroupSettingFacade;
        $this->pricingGroupRepository = $pricingGroupRepository;
        $this->user = $user;
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function indexAction(Request $request)
    {

        // reset chosen shipping/payment/b2b warranty options
        // $this->pricingGroupSettingFacade->setDefaultPricingGroupForSelectedDomain($this->pricingGroupRepository->getDefault());

        $this->user->setPricingGroup($this->pricingGroupRepository->findById($this->pricingGroupRepository->getDefault($this->domain->getId())->getId()));
        $_SESSION['order']['pricing_group'] = $this->pricingGroupRepository->getDefault($this->domain->getId())->getId();
        $_SESSION['order']['company_order'] = 'false';

        $cart = $this->cartFacade->getCartOfCurrentCustomer();

        if ($cart->isEmpty()) {
            $this->cartFacade->cleanAdditionalData();
        }

        $cartFormData = ['quantities' => []];
        foreach ($cart->getItems() as $cartItem) {
            $cartFormData['quantities'][$cartItem->getId()] = $cartItem->getQuantity();
        }

        $form = $this->createForm(CartFormType::class, $cartFormData);
        $form->handleRequest($request);

        $contactFormData = [];

        /* new order();
        $contactFormData->setEmail('martin.oravec@turbado.eu');
         */

        $contactForm = $this->createFormBuilder($contactFormData)
            ->add('email', EmailType::class, array('label' => 'Email'))
            ->add('tel', TextType::class, array('label' => 'Telephone'))
            ->add('next', SubmitType::class, array('label' => 'Next with Genius')
            )->getForm();

        $contactForm->handleRequest($request);

        $addressFormData = [];
        $addressForm = $this->createFormBuilder($addressFormData)
            ->add('email', EmailType::class, array('label' => 'Email'))
            ->add('tel', TextType::class, array('label' => 'Telephone'))->getForm();

        $addressForm->handleRequest($request);

        $invalidCart = false;
        if ($form->isValid()) {
            try {
                $this->cartFacade->changeQuantities($form->getData()['quantities']);

                if (!$request->get(self::RECALCULATE_ONLY_PARAMETER_NAME, false)) {
                    return $this->redirectToRoute('front_order_index');
                }
            } catch (\Shopsys\ShopBundle\Model\Cart\Exception\InvalidQuantityException $ex) {
                $invalidCart = true;
            }
        } elseif ($form->isSubmitted()) {
            $invalidCart = true;
        }

        if ($invalidCart) {
            $this->getFlashMessageSender()->addErrorFlash(
                t('Please make sure that you entered right quantity of all items in cart.')
            );
        }

        $cartItems = $cart->getItems();
        $domainId = $this->domain->getId();

        $orderPreview = $this->orderPreviewFactory->createForCurrentUser();
        $productsPrice = $orderPreview->getProductsPrice();
        $remainingPriceWithVat = $this->freeTransportAndPaymentFacade->getRemainingPriceWithVat(
            $productsPrice->getPriceWithVat(),
            $domainId
        );

         // needs to be fixed -
         // cant find how can I get product ids all together with getPriceWithoutVat, getPriceWithVat, getVatAmount
         $KKK = Array();
         foreach (  $orderPreview->getQuantifiedProducts() as $key => $item) {
             $KKK[] = $item->getProduct()->getId();
         }


        $_SESSION['itemHelper'] = Array();
        $tmp = 0;
        foreach ( $orderPreview->getQuantifiedItemsPrices() as $key => $item) {
           $_SESSION['itemHelper']['prices'][$KKK[$tmp]]['priceWithoutVat'] = $item->getUnitPrice()->getPriceWithoutVat();
           $_SESSION['itemHelper']['prices'][$KKK[$tmp]]['priceWithVat'] = $item->getUnitPrice()->getPriceWithVat();
           $_SESSION['itemHelper']['prices'][$KKK[$tmp]]['vatAmount'] = $item->getUnitPrice()->getVatAmount();
           $tmp++;
        }
        // needs to be fixed -
        foreach ($this->pricingGroupRepository->getPricingGroupsByDomainId($this->domain->getId()) as $key => $item) {
            $_SESSION['itemHelper']['pricingGroups'][$item->getId()]['coefficient'] = $item->getCoefficient();
            $_SESSION['itemHelper']['pricingGroups'][$item->getId()]['name'] = $item->getName();
        }
       // echo '<pre>',print_r($_SESSION['itemHelper'],2),'</pre>';
        return $this->render($this->getTemplate()->getTemplatePath('/Front/Content/Cart/index.html.twig'), [
            'cart' => $cart,
            'cartItems' => $cartItems,
            'cartItemPrices' => $orderPreview->getQuantifiedItemsPrices(),
            'form' => $form->createView(),
            'contact' => $contactForm->createView(),
            'address' => $addressForm->createView(),
            'isFreeTransportAndPaymentActive' => $this->freeTransportAndPaymentFacade->isActive($domainId),
            'isPaymentAndTransportFree' => $this->freeTransportAndPaymentFacade->isFree($productsPrice->getPriceWithVat(), $domainId),
            'remainingPriceWithVat' => $remainingPriceWithVat,
            'cartItemDiscounts' => $orderPreview->getQuantifiedItemsDiscounts(),
            'productsPrice' => $productsPrice,
        ]);
    }

    public function boxAction()
    {
        $orderPreview = $this->orderPreviewFactory->createForCurrentUser();

        return $this->render($this->getTemplate()->getTemplatePath('/Front/Inline/Cart/cartBox.html.twig'), [
            'cart' => $this->cartFacade->getCartOfCurrentCustomer(),
            'productsPrice' => $orderPreview->getProductsPrice(),
        ]);
    }

    /**
     * @param \Shopsys\ShopBundle\Model\Product\Product $product
     * @param string $type
     */
    public function addProductFormAction(Product $product, $type = 'normal')
    {
        $form = $this->createForm(AddProductFormType::class, ['productId' => $product->getId()], [
            'action' => $this->generateUrl('front_cart_add_product'),
        ]);

        return $this->render($this->getTemplate()->getTemplatePath('/Front/Inline/Cart/addProduct.html.twig'), [
            'form' => $form->createView(),
            'product' => $product,
            'type' => $type,
        ]);
    }

    public function addProductSmallFormAction(Product $product, $type = 'normal')
    {
        $form = $this->createForm(AddProductSmallFormType::class, ['productId' => $product->getId()], [
            'action' => $this->generateUrl('front_cart_add_product')
        ]);

        return $this->render('@ShopsysShop/Front/Inline/Cart/addProductSmall.html.twig', [
            'form' => $form->createView(),
            'product' => $product,
            'type' => $type
        ]);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function addProductAction(Request $request)
    {
        $form = $this->createForm(AddProductFormType::class);
        $form->handleRequest($request);

        if ($form->isValid()) {
            try {
                $formData = $form->getData();

                $addProductResult = $this->cartFacade->addProductToCart($formData['productId'], (int)$formData['quantity']);

                $this->sendAddProductResultFlashMessage($addProductResult);
            } catch (\Shopsys\ShopBundle\Model\Product\Exception\ProductNotFoundException $ex) {
                $this->getFlashMessageSender()->addErrorFlash(t('Selected product no longer available or doesn\'t exist.'));
            } catch (\Shopsys\ShopBundle\Model\Cart\Exception\InvalidQuantityException $ex) {
                $this->getFlashMessageSender()->addErrorFlash(t('Please enter valid quantity you want to add to cart.'));
            } catch (\Shopsys\ShopBundle\Model\Cart\Exception\CartException $ex) {
                $this->getFlashMessageSender()->addErrorFlash(t('Unable to add product to cart'));
            }
        } else {
            // Form errors list in flash message is temporary solution.
            // We need to determine couse of error when adding product to cart.
            $flashMessageBag = $this->get('shopsys.shop.component.flash_message.bag.front');
            $formErrors = $this->errorService->getAllErrorsAsArray($form, $flashMessageBag);
            $this->getFlashMessageSender()->addErrorFlashTwig(
                t('Unable to add product to cart:<br/><ul><li>{{ errors|raw }}</li></ul>'),
                [
                    'errors' => implode('</li><li>', $formErrors),
                ]
            );
        }

        if ($request->headers->get('referer')) {
            $redirectTo = $request->headers->get('referer');
        } else {
            $redirectTo = $this->generateUrl('front_homepage');
        }

        return $this->redirect($redirectTo);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function addProductAjaxAction(Request $request)
    {
        $form = $this->createForm(AddProductFormType::class);

        $form->handleRequest($request);

        if ($form->isValid()) {
            try {
                $formData = $form->getData();
                $addProductResult = $this->cartFacade->addProductToCart($formData['productId'], (int)$formData['quantity']);
                $this->sendAddProductResultFlashMessage($addProductResult);

                $accessories = $this->productAccessoryFacade->getTopOfferedAccessories(
                    $addProductResult->getCartItem()->getProduct(),
                    $this->domain->getId(),
                    $this->currentCustomer->getPricingGroup(),
                    self::AFTER_ADD_WINDOW_ACCESSORIES_LIMIT
                );
                $accessoryDetails = $this->productDetailFactory->getDetailsForProducts($accessories);

                return $this->render('@ShopsysShop/Front/Inline/Cart/afterAddWindow.html.twig', [
                    'accessoryDetails' => $accessoryDetails,
                    'ACCESSORIES_ON_BUY' => ModuleList::ACCESSORIES_ON_BUY,
                ]);
            } catch (\Shopsys\ShopBundle\Model\Product\Exception\ProductNotFoundException $ex) {
                $this->getFlashMessageSender()->addErrorFlash(t('Selected product no longer available or doesn\'t exist.'));
            } catch (\Shopsys\ShopBundle\Model\Cart\Exception\InvalidQuantityException $ex) {
                $this->getFlashMessageSender()->addErrorFlash(t('Please enter valid quantity you want to add to cart.'));
            } catch (\Shopsys\ShopBundle\Model\Cart\Exception\CartException $ex) {
                $this->getFlashMessageSender()->addErrorFlash(t('Unable to add product to cart'));
            }
        } else {
            // Form errors list in flash message is temporary solution.
            // We need to determine couse of error when adding product to cart.
            $flashMessageBag = $this->get('shopsys.shop.component.flash_message.bag.front');
            $formErrors = $this->errorService->getAllErrorsAsArray($form, $flashMessageBag);
            $this->getFlashMessageSender()->addErrorFlashTwig(
                t('Unable to add product to cart:<br/><ul><li>{{ errors|raw }}</li></ul>'),
                [
                    'errors' => implode('</li><li>', $formErrors),
                ]
            );
        }

        return $this->forward('ShopsysShopBundle:Front/FlashMessage:index');
    }

    /**
     * @param \Shopsys\ShopBundle\Model\Cart\AddProductResult $addProductResult
     */
    private function sendAddProductResultFlashMessage(
        AddProductResult $addProductResult
    )
    {
        if ($addProductResult->getIsNew()) {
            $this->getFlashMessageSender()->addSuccessFlashTwig(
                t('Product <strong>{{ name }}</strong> ({{ quantity|formatNumber }} {{ unitName }}) added to the cart'),
                [
                    'name' => $addProductResult->getCartItem()->getName(),
                    'quantity' => $addProductResult->getAddedQuantity(),
                    'unitName' => $addProductResult->getCartItem()->getProduct()->getUnit()->getName(),
                ]
            );
        } else {
            $this->getFlashMessageSender()->addSuccessFlashTwig(
                t('Product <strong>{{ name }}</strong> added to the cart (total amount {{ quantity|formatNumber }} {{ unitName }})'),
                [
                    'name' => $addProductResult->getCartItem()->getName(),
                    'quantity' => $addProductResult->getCartItem()->getQuantity(),
                    'unitName' => $addProductResult->getCartItem()->getProduct()->getUnit()->getName(),
                ]
            );
        }
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param int $cartItemId
     */
    public function deleteAction(Request $request, $cartItemId)
    {
        $cartItemId = (int)$cartItemId;
        $token = new CsrfToken('front_cart_delete_' . $cartItemId, $request->query->get('_token'));

        if ($this->get('security.csrf.token_manager')->isTokenValid($token)) {
            try {
                $productName = $this->cartFacade->getProductByCartItemId($cartItemId)->getName();

                $this->cartFacade->deleteCartItem($cartItemId);

                $this->getFlashMessageSender()->addSuccessFlashTwig(
                    t('Product {{ name }} removed from cart'),
                    ['name' => $productName]
                );
            } catch (\Shopsys\ShopBundle\Model\Cart\Exception\InvalidCartItemException $ex) {
                $this->getFlashMessageSender()->addErrorFlash(t('Unable to remove item from cart. The item is probably already removed.'));
            }
        } else {
            $this->getFlashMessageSender()->addErrorFlash(
                t('Unable to remove item from cart. The link for removing it probably expired, try it again.')
            );
        }

        return $this->redirectToRoute('front_cart');
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function b2bAction(Request $request)
    {
        $defaultB2BForDomains = [1 => 3834, 4 => 4232];

        if(array_key_exists($this->domain->getId(), $defaultB2BForDomains)) {
            $_SESSION['order']['pricing_group'] = $defaultB2BForDomains[$this->domain->getId()];
        } else {
            $_SESSION['order']['pricing_group'] = $this->pricingGroupSettingFacade->getDefaultPricingGroupByCurrentDomain()->getId();
        }

        $_SESSION['order']['company_order'] = 'true';
        unset($_SESSION['order']['genius']);

        return $this->redirect($this->generateUrl('front_order_index'));
    }
}