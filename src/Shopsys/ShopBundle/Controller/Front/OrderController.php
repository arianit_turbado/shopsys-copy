<?php

namespace Shopsys\ShopBundle\Controller\Front;

//use Proxies\__CG__\Shopsys\ShopBundle\Model\Pricing\Group\PricingGroup;

use Codeception\Module\Cli;
use Shopsys\ShopBundle\Component\Controller\FrontBaseController;
use Shopsys\ShopBundle\Component\Domain\Domain;
use Shopsys\ShopBundle\Component\HttpFoundation\DownloadFileResponse;
use Shopsys\ShopBundle\Form\Front\Order\DomainAwareOrderFlowFactory;
use Shopsys\ShopBundle\Model\Cart\CartFacade;
use Shopsys\ShopBundle\Model\Customer\User;
use Shopsys\ShopBundle\Model\Customer\UserRepository;
use Shopsys\ShopBundle\Model\Customer\CurrentCustomer;

use Shopsys\ShopBundle\Model\LegalConditions\LegalConditionsFacade;
use Shopsys\ShopBundle\Model\Newsletter\NewsletterFacade;
use Shopsys\ShopBundle\Model\Order\FrontOrderData;
use Shopsys\ShopBundle\Model\Order\Mail\OrderMailFacade;
use Shopsys\ShopBundle\Model\Order\OrderData;
use Shopsys\ShopBundle\Model\Order\OrderDataMapper;
use Shopsys\ShopBundle\Model\Order\OrderFacade;
use Shopsys\ShopBundle\Model\Order\Preview\OrderPreview;
use Shopsys\ShopBundle\Model\Order\Preview\OrderPreviewFactory;
use Shopsys\ShopBundle\Model\Order\Status\OrderStatusFacade;
use Shopsys\ShopBundle\Model\Order\Watcher\TransportAndPaymentWatcherService;
use Shopsys\ShopBundle\Model\Payment\BPayment\BPaymentForm;
use Shopsys\ShopBundle\Model\Payment\BPayment\BPaymentItem;
use Shopsys\ShopBundle\Model\Payment\BPayment\BPaymentMerchant;
use Shopsys\ShopBundle\Model\Payment\BPayment\BPaymentReturnUrl;
use Shopsys\ShopBundle\Model\Payment\PaymentEditData;
use Shopsys\ShopBundle\Model\Payment\PaymentFacade;
use Shopsys\ShopBundle\Model\Payment\PaymentPriceCalculation;
use Shopsys\ShopBundle\Model\Pricing\Currency\CurrencyFacade;

use Shopsys\ShopBundle\Model\Pricing\Group\PricingGroupRepository;
use Shopsys\ShopBundle\Model\Pricing\Group\PricingGroupFacade;
use Shopsys\ShopBundle\Model\Transport\TransportFacade;
use Shopsys\ShopBundle\Model\Transport\TransportPriceCalculation;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Shopsys\ShopBundle\Model\Order\PromoCode\CurrentPromoCodeFacade;
use Shopsys\ShopBundle\Model\Payment\BPayment\BPaymentEnvelope;
use Shopsys\ShopBundle\Model\Payment\PayPal\PayPalPaymentFacade;

use GoPay\Payments;
use Shopsys\ShopBundle\Model\Payment\GoPay\GoPayForm;

use Shopsys\ShopBundle\Model\ShopInfo\ShopInfoSettingFacade;

class OrderController extends FrontBaseController
{
    const SESSION_CREATED_ORDER = 'created_order_id';

    /**
     * @var array
     */
    protected $paymentPoviders = [
        "CreditCard" => 1,
        "CashOnDelivery" => 2,
        "Cash" => 3,
        "BPayment" => 4,
        "GoPay" => 5,
        "PayPal" => 6,
        "BankTransfer" => 7
    ];

    protected $orderStatusesEnum = [
        "New" => 1,
        "InProgress" => 2,
        "Done " => 3,
        "Canceled" => 4,
        "Payed" => 5
    ];

    /**
     * @var \Shopsys\ShopBundle\Form\Front\Order\DomainAwareOrderFlowFactory
     */
    private $domainAwareOrderFlowFactory;

    /**
     * @var \Shopsys\ShopBundle\Model\Cart\CartFacade
     */
    private $cartFacade;

    /**
     * @var \Shopsys\ShopBundle\Component\Domain\Domain
     */
    private $domain;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\Mail\OrderMailFacade
     */
    private $orderMailFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\OrderDataMapper
     */
    private $orderDataMapper;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\OrderFacade
     */
    private $orderFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\Preview\OrderPreviewFactory
     */
    private $orderPreviewFactory;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\Watcher\TransportAndPaymentWatcherService
     */
    private $transportAndPaymentWatcherService;

    /**
     * @var \Shopsys\ShopBundle\Model\Payment\PaymentFacade
     */
    private $paymentFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Payment\PaymentPriceCalculation
     */
    private $paymentPriceCalculation;

    /**
     * @var \Shopsys\ShopBundle\Model\Pricing\Currency\CurrencyFacade
     */
    private $currencyFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Pricing\Group\PricingGroupFacade
     */
    private $pricingGroupFacade;


    /**
     * @var \Shopsys\ShopBundle\Model\Pricing\Group\PricingGroupRepository
     */
    private $pricingGroupRepository;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\PromoCode\CurrentPromoCodeFacade
     */
    private $promoCodeFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Customer\UserRepository
     */
    private $userRepository;

    /**
     * @var \Shopsys\ShopBundle\Model\Customer\CurrentCustomer
     */
    private $currentCustomer;

    /**
     * @var \Shopsys\ShopBundle\Model\Customer\User
     */
    private $user;


    /**
     * @var \Shopsys\ShopBundle\Model\Transport\TransportFacade
     */
    private $transportFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Transport\TransportPriceCalculation
     */
    private $transportPriceCalculation;

    /**
     * @var \Symfony\Component\HttpFoundation\Session\Session
     */
    private $session;

    /**
     * @var \Shopsys\ShopBundle\Model\LegalConditions\LegalConditionsFacade
     */
    private $legalConditionsFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Newsletter\NewsletterFacade
     */
    private $newsletterFacade;

    private $goPayments;

    /**
     * @var \Shopsys\ShopBundle\Model\ShopInfo\ShopInfoSettingFacade;
     */
    private $shopInfoSettingFacade;

    public function __construct(
        OrderFacade $orderFacade,
        OrderStatusFacade $orderStatusFacade,
        CartFacade $cartFacade,
        OrderPreviewFactory $orderPreviewFactory,
        TransportPriceCalculation $transportPriceCalculation,
        PaymentPriceCalculation $paymentPriceCalculation,
        Domain $domain,
        TransportFacade $transportFacade,
        PaymentFacade $paymentFacade,
        CurrencyFacade $currencyFacade,
        PricingGroupFacade $pricingGroupFacade,
        PricingGroupRepository $pricingGroupRepository,
        CurrentPromoCodeFacade $currentPromoCodeFacade,
        UserRepository $userRepository,
        CurrentCustomer $currentCustomer,
        User $user,

        OrderDataMapper $orderDataMapper,
        DomainAwareOrderFlowFactory $domainAwareOrderFlowFactory,
        Session $session,
        TransportAndPaymentWatcherService $transportAndPaymentWatcherService,
        OrderMailFacade $orderMailFacade,
        LegalConditionsFacade $legalConditionsFacade,
        NewsletterFacade $newsletterFacade,
        Payments $goPayments,

        ShopInfoSettingFacade $shopInfoSettingFacade
    )
    {
        $this->orderFacade = $orderFacade;
        $this->orderStatusFacade = $orderStatusFacade;
        $this->cartFacade = $cartFacade;
        $this->orderPreviewFactory = $orderPreviewFactory;
        $this->transportPriceCalculation = $transportPriceCalculation;
        $this->paymentPriceCalculation = $paymentPriceCalculation;
        $this->domain = $domain;
        $this->transportFacade = $transportFacade;
        $this->paymentFacade = $paymentFacade;
        $this->currencyFacade = $currencyFacade;


        $this->pricingGroupFacade = $pricingGroupFacade;
        $this->pricingGroupRepository = $pricingGroupRepository;
        $this->currentPromoCodeFacade = $currentPromoCodeFacade;
        $this->user = $user;
        $this->userRepository = $userRepository;
        $this->currentCustomer = $currentCustomer;
        $this->orderDataMapper = $orderDataMapper;
        $this->domainAwareOrderFlowFactory = $domainAwareOrderFlowFactory;
        $this->session = $session;
        $this->transportAndPaymentWatcherService = $transportAndPaymentWatcherService;
        $this->orderMailFacade = $orderMailFacade;
        $this->legalConditionsFacade = $legalConditionsFacade;
        $this->newsletterFacade = $newsletterFacade;
        $this->goPayments = $goPayments;

        $this->shopInfoSettingFacade = $shopInfoSettingFacade;
    }

    /**
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function indexAction()
    {

        //if(isset($_SESSION['order']['genius'])) echo '<pre>',print_r($_SESSION['order']['genius'],2),'</pre>';
        //if (isset($_SESSION['_sf2_attributes']['craue_form_flow']['order'])) echo '<pre>',print_r($_SESSION['_sf2_attributes']['craue_form_flow']['order'],2),'</pre>';
        $flashMessageBag = $this->get('shopsys.shop.component.flash_message.bag.front');
        /* @var $flashMessageBag \Shopsys\ShopBundle\Component\FlashMessage\Bag */
        $cart = $this->cartFacade->getCartOfCurrentCustomer();
        $cartItems = $cart->getItems();

        $orderPreview = $this->orderPreviewFactory->createForCurrentUser();
        $productsPrice = $orderPreview->getProductsPrice();

        if ($cart->isEmpty()) {
            return $this->redirectToRoute('front_cart');
        }

        $user = $this->getUser();
        $frontOrderFormData = new FrontOrderData();
        $frontOrderFormData->deliveryAddressSameAsBillingAddress = true;
        if ($user instanceof User) {
            $this->orderFacade->prefillFrontOrderData($frontOrderFormData, $user);
        }
        $domainId = $this->domain->getId();
        $frontOrderFormData->domainId = $domainId;
        $currency = $this->currencyFacade->getDomainDefaultCurrencyByDomainId($domainId);
        $frontOrderFormData->currency = $currency;

        $orderFlow = $this->domainAwareOrderFlowFactory->create();
        if ($orderFlow->isBackToCartTransition()) {

            return $this->redirectToRoute('front_cart');
        }

        $orderFlow->bind($frontOrderFormData);

        $orderFlow->saveSentStepData();
        $form = $orderFlow->createForm();
        $payment = $frontOrderFormData->payment;
        $transport = $frontOrderFormData->transport;

        $orderPreview = $this->orderPreviewFactory->createForCurrentUser($transport, $payment);

        $isValid = $orderFlow->isValid($form);

        // FormData are filled during isValid() call
        $orderData = $this->orderDataMapper->getOrderDataFromFrontOrderData($frontOrderFormData);

        if ($this->pricingGroupRepository->checkIfB2b() == 'true' && $orderFlow->getCurrentStep() == 3) {
            $payment = null;
            $transport = null;
            if ($frontOrderFormData->pricingGroup == $this->pricingGroupRepository->getDefault($this->domain->getId())->getId()) {
                $_SESSION['order']['pricing_group'] = $frontOrderFormData->pricingGroup;
                $_SESSION['order']['company_order'] = 'false';

                $form = $orderFlow->createForm();
            }

            if ($_SESSION['order']['pricing_group'] != $frontOrderFormData->pricingGroup) {
                $isValid = false;
                $_SESSION['order']['pricing_group'] = $frontOrderFormData->pricingGroup;
                //echo "changing pricing group";
            }

            $this->user->setPricingGroup($this->pricingGroupRepository->findById($frontOrderFormData->pricingGroup));
        }

        $payments = $this->paymentFacade->getVisibleOnCurrentDomain();
        $transportsToFilter = $this->transportFacade->getVisibleOnCurrentDomain($payments);
        $transports = [];


        if($this->pricingGroupRepository->checkIfB2b() == 'true') {
            foreach ($transportsToFilter as $transportsToFilter) {
                if($transportsToFilter->getId() == 13) {
                    $transports[$transportsToFilter->getId()] = $transportsToFilter;
                }
            }
        } else {
            foreach ($transportsToFilter as $transportsToFilter) {
                if($transportsToFilter->getId() != 13) {
                    $transports[$transportsToFilter->getId()] = $transportsToFilter;
                }
            }
        }


        if ($orderFlow->getCurrentStep() == 2) {
            if (isset($_SESSION['order']['base_price'])) {

                if (($payment != null || $transport != null) && $this->pricingGroupRepository->checkIfB2b() == 'true') {
                    $payment = null;
                    $transport = null;
                    //   echo "clearing payment,transport";
                    //   echo '<pre>',print_r( $_SESSION,2),'</pre>';
                    if (isset($_SESSION['_sf2_attributes']['craue_form_flow']['order']['flow_order']['data']['4'])) {
                        unset($_SESSION['_sf2_attributes']['craue_form_flow']['order']['flow_order']['data']['4']);
                    }
                    if (isset($_SESSION['_sf2_attributes']['craue_form_flow']['order']['flow_order']['data']['5'])) {
                        unset($_SESSION['_sf2_attributes']['craue_form_flow']['order']['flow_order']['data']['5']);
                    }
                    if (isset($_SESSION['_sf2_attributes']['_csrf/transport_form'])) {
                        unset($_SESSION['_sf2_attributes']['_csrf/transport_form']);
                    }
                    if (isset($_SESSION['_sf2_attributes']['_csrf/payment_form'])) {
                        unset($_SESSION['_sf2_attributes']['_csrf/payment_form']);
                    }
                }

                $_SESSION['order']['base_price'] = $this->orderPreviewFactory->createForCurrentUser($transport, $payment)->getTotalPrice()->getPriceWithVat();
            } else {
                $_SESSION['order']['base_price'] = $this->orderPreviewFactory->createForCurrentUser($transport, $payment)->getTotalPrice()->getPriceWithVat();
            }
        }


        $this->checkTransportAndPaymentChanges($orderData, $orderPreview, $transports, $payments);
        if ($isValid) {

            if ($orderFlow->nextStep()) {
                $form = $orderFlow->createForm();
            } elseif ($flashMessageBag->isEmpty()) {


                $order = $this->orderFacade->createOrderFromFront($orderData);


                if ($frontOrderFormData->newsletterSubscription) {
                    $this->newsletterFacade->addSubscribedEmail($frontOrderFormData->email);
                }
                $orderFlow->reset();
                try {
                    $this->sendMail($order);
                } catch (\Shopsys\ShopBundle\Model\Mail\Exception\MailException $e) {
                    $this->getFlashMessageSender()->addErrorFlash(
                        t('Unable to send some e-mails, please contact us for order verification.')
                    );
                }
                $this->session->set(self::SESSION_CREATED_ORDER, $order->getId());
                if (isset($_SESSION['order']['pricing_group'])) unset($_SESSION['order']['pricing_group']);
                return $this->redirectToRoute('front_order_sent');
            }
        }

        if ($form->isSubmitted() && !$form->isValid() && $form->getErrors()->count() === 0) {
            $form->addError(new FormError(t('Please check the correctness of all data filled.')));
        }


        if($orderFlow->getCurrentStep() == 6) {
            $sale = 0;
            foreach ($orderPreview->getQuantifiedItemsDiscounts() as $key => $value) {
                if (method_exists($value,"getPriceWithVat")) {
                    if($value->getPriceWithVat() !== null){
                        $sale += $value->getPriceWithVat();
                    }
                }
            }
            if($sale > 0 ) {
                $_SESSION['orderSale'] = -$sale;
            }
        }

        return $this->render($this->getTemplate()->getTemplatePath('/Front/Content/Order/index.html.twig'), [
            'form' => $form->createView(),
            'flow' => $orderFlow,
            'transport' => $transport,
            'payment' => $payment,
            'payments' => $payments,
            'cart' => $cart,
            'addressDetails' => (isset($_SESSION['_sf2_attributes']['craue_form_flow']['order']['flow_order']['data']['3'])) ? $_SESSION['_sf2_attributes']['craue_form_flow']['order']['flow_order']['data']['3'] : '',
            'contactDetails' => (isset($_SESSION['_sf2_attributes']['craue_form_flow']['order']['flow_order']['data']['2'])) ? $_SESSION['_sf2_attributes']['craue_form_flow']['order']['flow_order']['data']['2'] : '',
            'cartItems' => $cartItems,
            'cartItemPrices' => $orderPreview->getQuantifiedItemsPrices(),
            'b2bPricesDiff' => $this->pricingGroupRepository->getB2bPricesDiff($this->domain->getId()),
            'totalPrice' => $this->orderPreviewFactory->createForCurrentUser($transport, $payment)->getTotalPrice()->getPriceWithVat(),
            'productsPrice' => $productsPrice,
            'cartItemDiscounts' => $orderPreview->getQuantifiedItemsDiscounts(),
            'company_order' => $this->pricingGroupRepository->checkIfB2b(),
            'transportsPrices' => $this->transportPriceCalculation->getCalculatedPricesIndexedByTransportId(
                $transports,
                $currency,
                $orderPreview->getProductsPrice(),
                $domainId
            ),
            'paymentsPrices' => $this->paymentPriceCalculation->getCalculatedPricesIndexedByPaymentId(
                $payments,
                $currency,
                $orderPreview->getProductsPrice(),
                $domainId
            ),
            'termsAndConditionsArticle' => $this->legalConditionsFacade->findTermsAndConditions($this->domain->getId()),
            'privacyPolicyArticle' => $this->legalConditionsFacade->findPrivacyPolicy($this->domain->getId()),
        ]);
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function previewAction(Request $request)
    {
        $transportId = $request->get('transportId');
        $paymentId = $request->get('paymentId');

        if ($transportId === null) {
            $transport = null;
        } else {
            $transport = $this->transportFacade->getById($transportId);
        }

        if ($paymentId === null) {
            $payment = null;
        } else {
            $payment = $this->paymentFacade->getById($paymentId);
        }

        $orderPreview = $this->orderPreviewFactory->createForCurrentUser($transport, $payment);

        return $this->render($this->getTemplate()->getTemplatePath('/Front/Content/Order/preview.html.twig'), [
            'orderPreview' => $orderPreview,
        ]);
    }

    /**
     * @param \Shopsys\ShopBundle\Model\Order\OrderData $orderData
     * @param \Shopsys\ShopBundle\Model\Order\Preview\OrderPreview $orderPreview
     * @param \Shopsys\ShopBundle\Model\Transport\Transport[] $transports
     * @param \Shopsys\ShopBundle\Model\Payment\Payment[] $payments
     */
    private function checkTransportAndPaymentChanges(
        OrderData $orderData,
        OrderPreview $orderPreview,
        array $transports,
        array $payments
    )
    {
        $transportAndPaymentCheckResult = $this->transportAndPaymentWatcherService->checkTransportAndPayment(
            $orderData,
            $orderPreview,
            $transports,
            $payments
        );

        if ($transportAndPaymentCheckResult->isTransportPriceChanged()) {
            $this->getFlashMessageSender()->addInfoFlashTwig(
                t('The price of shipping {{ transportName }} changed during ordering process. Check your order, please.'),
                [
                    'transportName' => $orderData->transport->getName(),
                ]
            );
        }
        if ($transportAndPaymentCheckResult->isPaymentPriceChanged()) {
            $this->getFlashMessageSender()->addInfoFlashTwig(
                t('The price of payment {{ paymentName }} changed during ordering process. Check your order, please.'),
                [
                    'paymentName' => $orderData->payment->getName(),
                ]
            );
        }
    }

    public function saveOrderFormAction()
    {
        $flow = $this->domainAwareOrderFlowFactory->create();
        $flow->bind(new FrontOrderData());
        $form = $flow->createForm();
        $flow->saveCurrentStepData($form);

        return new Response();
    }

    public function payLaterAction(Request $request)
    {
        $orderId = $request->get('order_id');
        if ($orderId === null) {
            return $this->redirectToRoute('front_homepage');
        }

        $order = $this->orderFacade->getById($orderId);
        if ($order->getStatus()->getType() == $this->orderStatusesEnum["Payed"]) {
            $request->getSession()
                ->getFlashBag()
                ->add('error', "Order has already been payed!");
            return $this->redirectToRoute('front_homepage');
        }

        return $this->resolvePaymentForm($orderId);
    }

    public function sentAction()
    {
        $orderId = $this->session->get(self::SESSION_CREATED_ORDER, null);
        $this->session->remove(self::SESSION_CREATED_ORDER);

        if ($orderId === null) {
            return $this->redirectToRoute('front_cart');
        }

        $order = $this->orderFacade->getById($orderId);

        if ($order->getDomainId() == 1 && $_SERVER['REMOTE_ADDR'] != '172.19.0.1') {
//            $client = new \GuzzleHttp\Client();
//
//            $response = $client->request('POST', 'http://orders.turbado.eu/api/registerOrder', [
//                'form_params' => [
//                    'shop_id' => '20',
//                    'order_id' => $orderId,
//                ]
//            ]);
        }


        unset($_SESSION['order']['genius']);
        return $this->resolvePaymentForm($orderId);
    }

    /**
     * @param int $orderId
     * @return string
     */
    protected function resolvePaymentForm($orderId)
    {
        $order = $this->orderFacade->getById($orderId);
        switch ($order->getPayment()->getId()) {
            case $this->paymentPoviders["BPayment"]:
                $paymentForm = $this->BPayment($orderId);
                return $this->render('@ShopsysShop/Front/Content/Order/payment.html.twig', [
                    'formContent' => $paymentForm
                ]);
            case $this->paymentPoviders["PayPal"]:
                $paymentForm = $this->paypalForm($orderId);
                return $this->render('@ShopsysShop/Front/Content/Order/payment.html.twig', [
                    'formContent' => $paymentForm
                ]);
            case $this->paymentPoviders['GoPay']:
                $returnUrl = "{$this->domain->getUrl()}/order/check_gopay_status";
                $response = $this->goPayments->createPayment($order->getGoPayRequest($returnUrl, $this->domain->getUrl()));
                if ($response->hasSucceed()) {
                    $paymentForm = GoPayForm::getPaymentForm($response->json['gw_url'], $this->goPayments->urlToEmbedJs());
                    return $this->render('@ShopsysShop/Front/Content/Order/payment.html.twig', [
                        'formContent' => $paymentForm
                    ]);
                } else {
                    return $this->render('@ShopsysShop/Front/Content/Error/error.html.twig', [
                        'status_text' => 'Order payment failed',
                        'status_code' => 500
                    ]);
                }
                break;
            case $this->paymentPoviders["BankTransfer"]:
                $paymentForm = $this->bankTransferFrom($orderId);
                return $this->render('@ShopsysShop/Front/Content/Order/payment.html.twig', [
                    'formContent' => $paymentForm
                ]);
            case $this->paymentPoviders["CreditCard"]:
                if ($this->domain->getId() == 1 || $this->domain->getId() == 3) {
                    $paymentForm = $this->paypalForm($orderId);
                    return $this->render('@ShopsysShop/Front/Content/Order/payment.html.twig', [
                        'formContent' => $paymentForm
                    ]);
                } elseif ($this->domain->getId() == 2) {
                    $paymentForm = $this->BPayment($orderId);
                    return $this->render('@ShopsysShop/Front/Content/Order/payment.html.twig', [
                        'formContent' => $paymentForm
                    ]);
                } elseif ($this->domain->getId() == 4 || $this->domain->getId() == 5) {
                    $returnUrl = "{$this->domain->getUrl()}/order/check_gopay_status";
                    $response = $this->goPayments->createPayment($order->getGoPayRequest($returnUrl, $this->domain->getUrl()));
                    if ($response->hasSucceed()) {
                        $paymentForm = GoPayForm::getPaymentForm($response->json['gw_url'], $this->goPayments->urlToEmbedJs());
                        return $this->render('@ShopsysShop/Front/Content/Order/payment.html.twig', [
                            'formContent' => $paymentForm
                        ]);
                    } else {
                        return $this->render('@ShopsysShop/Front/Content/Error/error.html.twig', [
                            'status_text' => 'Order payment failed',
                            'status_code' => 500
                        ]);
                    }
                }
                break;
        }

        return $this->render('@ShopsysShop/Front/Content/Order/sent.html.twig', [
            'pageContent' => $this->orderFacade->getOrderSentPageContent($orderId),
            'order' => $this->orderFacade->getById($orderId),
        ]);
    }

    public function termsAndConditionsAction()
    {
        return $this->getTermsAndConditionsResponse();
    }

    public function termsAndConditionsDownloadAction()
    {
        $response = $this->getTermsAndConditionsResponse();

        return new DownloadFileResponse(
            $this->legalConditionsFacade->getTermsAndConditionsDownloadFilename(),
            $response->getContent()
        );
    }

    public function payAction(Request $request)
    {
        $orderId = $request->get('order_id');
        $paymentMethod = $request->get('paymentMethod');
        $status = $request->get('status');
        if ($status == 'ERROR') {
            $request->getSession()
                ->getFlashBag()
                ->add('error', $request->get('errordescription'));
            // return $this->redirectToRoute('front_index');
            return $this->redirectToRoute('front_homepage');
        }

        $order = $this->orderFacade->getById($orderId);

        if ($paymentMethod == 'BPayment') {
            // Not sure if this should be done in all methods
            $orderStatus = $this->orderStatusFacade->getById($this->orderStatusesEnum["Payed"]);
            $order->setStatus($orderStatus);
            $entityManager = $this->getDoctrine()->getEntityManager();
            $entityManager->persist($order);
            $entityManager->flush($order);
        } else {
            $orderStatus = $this->orderStatusFacade->getById(3);
            $order->setStatus($orderStatus);
        }

        if ($order->getDomainId() == 1) {
            $client = new \GuzzleHttp\Client();

            $response = $client->request('POST', 'http://orders.turbado.eu/api/updateOrder', [
                'form_params' => [
                    'shop_id' => '20',
                    'order_id' => $orderId,
                ]
            ]);
        }

        return $this->render('@ShopsysShop/Front/Content/Order/sent.html.twig', [
            'pageContent' => $this->orderFacade->getOrderSentPageContent($orderId),
            'order' => $this->orderFacade->getById($orderId),
        ]);
    }

    public function paypalResponseAction(Request $request)
    {
        $entityManager = $this->getDoctrine()->getEntityManager();
        $orderId = $request->get('order_id');
        $domainId = $this->orderFacade->getById($orderId)->getDomainId();
        $paymentId = $request->get('paymentId');
        $payerId = $request->get('PayerID');
        $status = $request->get('success') == "true" ? true : false;

        $paypalFacade = new PayPalPaymentFacade($this->orderFacade);

        $result = false;
        if ($status == true) {
            try {
                $result = $paypalFacade->executePayment($domainId, $paymentId, $payerId, $this->getPayPalEnv());
            } catch (\Exception $ex) {
                $result = false;
            }
        } else {
            // Cancel
            $orderId = $request->get('order_id');
            $order = $this->orderFacade->getById($orderId);

            $status = $this->orderStatusFacade->getById($this->orderStatusesEnum["Canceled"]);
            $order->setStatus($status);
            $entityManager->persist($order);
            $entityManager->flush($order);
//            if ($order->getDomainId() == 1) {
//                $client = new \GuzzleHttp\Client();
//
//                $response = $client->request('POST', 'http://orders.turbado.eu/api/updateOrder', [
//                    'form_params' => [
//                        'shop_id' => '20',
//                        'order_id' => $orderId,
//                    ]
//                ]);
//            }
            $this->restoreCartAfterCancel($orderId);

            $request->getSession()
                ->getFlashBag()
                ->add('error', 'Your order has been canceled!');
            return $this->redirectToRoute('front_homepage');
        }

        if ($result != true) {
            // Error
            $request->getSession()
                ->getFlashBag()
                ->add('error', "Payment failed");
            return $this->redirectToRoute('front_homepage');
        }

        // Success
        $orderStatus = $this->orderStatusFacade->getById($this->orderStatusesEnum["Payed"]);
        $order = $this->orderFacade->getById($orderId);
        $order->setStatus($orderStatus);
        $entityManager->persist($order);
        $entityManager->flush($order);

        // return $this->render('@ShopsysShop/Front/Content/Order/complete.html.twig', [
        //     'order' => $this->orderFacade->getById($orderId),
        //     'orderId' => $orderId
        // ]);

        return $this->render('@ShopsysShop/Front/Content/Order/sent.html.twig', [
            'pageContent' => $this->orderFacade->getOrderSentPageContent($orderId),
            'order' => $order,
        ]);
    }

    public function cancelAction(Request $request)
    {
        $orderId = $request->get('order_id');
        $order = $this->orderFacade->getById($orderId);
        $status = $this->orderStatusFacade->getById(4);
        $order->setStatus($status);
        $paymentMethod = $request->get('paymentMethod');

        if ($paymentMethod == 'BPayment') {
            // Not sure if this should be done in all methods
            $entityManager = $this->getDoctrine()->getEntityManager();
            $entityManager->persist($order);
            $entityManager->flush($order);
        }

//        if ($order->getDomainId() == 1) {
//            $client = new \GuzzleHttp\Client();

//            $response = $client->request('POST', 'http://orders.turbado.eu/api/updateOrder', [
//                'form_params' => [
//                    'shop_id' => '20',
//                    'order_id' => $orderId,
//                ]
//            ]);
//        }


        $this->restoreCartAfterCancel($order->getId());
        $request->getSession()
            ->getFlashBag()
            ->add('error', 'Your order has been canceled!');
        // return $this->redirectToRoute('front_index');
        return $this->redirectToRoute('front_homepage');
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    private function getTermsAndConditionsResponse()
    {
        return $this->render('@ShopsysShop/Front/Content/Order/legalConditions.html.twig', [
            'termsAndConditionsArticle' => $this->legalConditionsFacade->findTermsAndConditions($this->domain->getId()),
        ]);
    }

    /**
     * @param \Shopsys\ShopBundle\Model\Order\Order $order
     */
    private function sendMail($order)
    {
        $mailTemplate = $this->orderMailFacade->getMailTemplateByStatusAndDomainId($order->getStatus(), $order->getDomainId());
        if ($mailTemplate->isSendMail()) {
            $this->orderMailFacade->sendEmail($order);
        }
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function addGeniusAction(Request $request)
    {

        $step = (int)$_POST['step'];
        switch ($step) {
            case 1:
                $_SESSION['order']['genius'][$step] = 'true';
                break;
            case 2:
                $_SESSION['order']['genius'][$step] = 'true';
                break;
            case 3:
                $_SESSION['order']['genius'][$step] = 'true';
                $this->currentPromoCodeFacade->setEnteredPromoCode('genius');
                break;
        }

        return new Response(json_encode($step));
    }

    /**
     * @param \Symfony\Component\HttpFoundation\Request $request
     */
    public function removeGeniusAction(Request $request)
    {
        $step = (int)$_POST['step'];
        switch ($step) {
            case 1:
                if (isset($_SESSION['order']['genius'][$step])) {
                    unset($_SESSION['order']['genius'][$step]);
                }
                break;
            case 2:
                if (isset($_SESSION['order']['genius'][$step])) {
                    unset($_SESSION['order']['genius'][$step]);
                }
                break;
            case 3:
                if (isset($_SESSION['order']['genius'][$step])) {
                    unset($_SESSION['order']['genius'][$step]);
                    $this->currentPromoCodeFacade->removeEnteredPromoCode('genius');
                }
                break;
        }

        return new Response(json_encode($step));
    }

    private function paypalForm($order_id)
    {
        $paypalFacade = new PayPalPaymentFacade($this->orderFacade);
        $returnUrlSuccess = "{$this->domain->getUrl()}/order/pay/paypal?order_id={$order_id}&success=true";
        $returnUrlError = "{$this->domain->getUrl()}/order/pay/paypal?order_id={$order_id}&success=false";

        $form = $paypalFacade->redirectToPayPal($order_id, $returnUrlSuccess, $returnUrlError, $this->getPayPalEnv(), $this->getDomainPaypalInfo());
        return $form;
    }

    private function BPayment($order_id)
    {
        $order = $this->orderFacade->getById($order_id);
        $email = 'rilind.mehmeti@turbado.eu';
        $logoUrl = 'https://turbado.de/html/img/logo.png';
        $returnUrlSuccess = "{$this->domain->getUrl()}/order/pay?order_id={$order_id}&paymentMethod=BPayment";
        $returnUrlCancel = "{$this->domain->getUrl()}/order/cancel?order_id={$order_id}&paymentMethod=BPayment";
        $returnUrlError = "{$this->domain->getUrl()}/order/pay?order_id={$order_id}";

        $returnUrlSuccess1 = "https://test.borgun.is/SecurePay/SuccessPage.aspx";
        $returnUrlCancel1 = "https://test.borgun.is/SecurePay/SuccessPage.aspx";
        $returnUrlError1 = "https://test.borgun.is/SecurePay/SuccessPage.aspx";

        // if ($order->getPayment()->getId() == 4) {
        $paymentForm = $order->getBPaymentForm(new BPaymentMerchant(['email' => $email,
            'logo' => $logoUrl]),
            new BPaymentReturnUrl(['returnurlsuccess' => $returnUrlSuccess,
                'returnurlcancel' => $returnUrlCancel,
                'returnurlerror' => $returnUrlError]), BPaymentEnvelope::$environments['test']);
        return $paymentForm;
        // }
        return false;
    }

    private function BPaymentHTML($generalInfo, $returnurls, $merchant, $products, $orderId = '', $language = 'EN')
    {
        $generalInfo = ['merchantId' => '9275444', 'paymentGatewayId' => '16', 'secretkey' => '99887766'];
        $envelope = new BPaymentEnvelope($generalInfo, $orderId, $language);
        $envelope->setUrls($returnurls);
        $envelope->setMerchant($merchant);
        foreach ($products as $product) {
            $envelope->addItem(new BPaymentItem($product['name'], $product['total'], $product['qty']));
        }
        $envelope->calculateHash();
        $paymentForm = BPaymentForm::auto_submit(BPaymentEnvelope::$environments['test'], $envelope->get());
        return $paymentForm;
    }

    public function checkGoPayStatusAction(Request $request)
    {
        $response = json_decode($this->goPayments->getStatus($request->get('id')));

        if ($response->state == 'PAID') {
            return $this->redirectToRoute('front_order_pay', ['order_id' => $response->order_number]);
        } else {
            return $this->redirectToRoute('front_order_cancel', ['order_id' => $response->order_number]);
        }
    }

    public function payBankTransferAction(Request $request)
    {
        $orderId = $request->get('order_id');
        if ($orderId === null) {
            return $this->redirectToRoute('front_cart');
        }
        $genius_order = $this->orderFacade->getOrderType($orderId, $this->domain->getId()) == 1;
        return $this->render('@ShopsysShop/Front/Content/Order/banktransfer.html.twig', [
            'shopinfo' => $this->getDomainBankTransferInfo(),
            'genius_offer' => $genius_order,
            'order' => $this->orderFacade->getById($orderId),
            "customer" => $this->orderFacade->getById($orderId)->getCustomer(),
            "products" => $this->orderFacade->getById($orderId)->getProductItems()
        ]);
    }

    private function bankTransferFrom($order_id)
    {
        $redirectUrl = "{$this->domain->getUrl()}/order/pay/banktransfer?order_id={$order_id}";
        $form = "<script>window.location.href='{$redirectUrl}';</script>";
        return $form;
    }

    public function getDomainPaypalInfo()
    {
        $data = [];
        $id = $this->domain->getId();
        switch ($id) {
            case 1: //eleka.de
                $data["locale_code"] = "DE";
                $data["unique_id"] = "eleka_de";
                $data["name"] = $this->domain->getName();
                break;
            case 2: //scbudapest.hu
                $data["locale_code"] = "HU";
                $data["unique_id"] = "scbudapest_hu";
                $data["name"] = $this->domain->getName();
                break;
            case 3: //scpoznan.pl
                $data["locale_code"] = "PL";
                $data["unique_id"] = "scpoznan_pl";
                $data["name"] = $this->domain->getName();
                break;
            case 4: //exafoto.sk
                $data["locale_code"] = "SK";
                $data["unique_id"] = "exafoto_sk";
                $data["name"] = $this->domain->getName();
                break;
            case 5: //exafoto.cz
                $data["locale_code"] = "CZ";
                $data["unique_id"] = "exafoto_cz";
                $data["name"] = $this->domain->getName();
                break;
            case 100: //english.en
                $data["locale_code"] = "GB";
                $data["unique_id"] = "english_en";
                $data["name"] = $this->domain->getName();
                break;
            default:
                throw new \Exception("Uknown domain id #{$id}");
        }
        return $data;
    }

    public function getPayPalEnv()
    {
        switch ($this->domain->getId()) {
            case 1: //eleka.de
                return PayPalPaymentFacade::MODE_LIVE;
            case 2: //scbudapest.hu
                return PayPalPaymentFacade::MODE_TEST;
            case 3: //scpoznan.pl
                return PayPalPaymentFacade::MODE_LIVE;
            case 4: //exafoto.sk
                return PayPalPaymentFacade::MODE_TEST;
            case 5: //exafoto.cz
                return PayPalPaymentFacade::MODE_TEST;
            case 100: //english.en
                return PayPalPaymentFacade::MODE_TEST;
            default:
                return PayPalPaymentFacade::MODE_TEST;
        }
        return $data;
    }

    public function getDomainBankTransferInfo()
    {
        $data = [];
        $id = $this->domain->getId();
        $data["id"] = $id;
        $data['phone'] = $this->shopInfoSettingFacade->getPhoneNumber($id);
        $data['email'] = $this->shopInfoSettingFacade->getEmail($id);
        $data["web"] = $this->domain->getUrl();
        $data["name"] = $this->domain->getName();
        $data["genius"] = [];

        switch ($id) {
            case 1: //eleka.de
                $data["address"] = "Suite 2, First Floor, Kenwood House <br/>77A Shenley Rd., Borehamwood WD6 1AG <br/>United Kingdom<br/>";
                $data["address_2"] = "Dunckerstr. 86<br />10437 Berlin<br />Deutschland<br />";
                $data["company_number"] = "09152610";
                $data["ico"] = '50796097'; //IČO
                $data["tax_id"] = ""; //DIČ
                $data["vat_id"] = ""; //IČ DPH
                $data["bank_name"] = "";
                $data["bank_account"] = "190328169";
                $data["iban"] = "DE08100500000190328169";
                $data["swift_bic"] = "BELADEBEXXX";

                $data["genius"]["address"] = "Suite 2, First Floor, Kenwood House <br/>77A Shenley Rd., Borehamwood WD6 1AG <br/>United Kingdom<br/>";
                $data["genius"]["address_2"] = "Dunckerstr. 86<br />10437 Berlin<br />Deutschland<br />";
                $data["genius"]["company_number"] = "09152610";
                $data["genius"]["ico"] = '50796097'; //IČO
                $data["genius"]["tax_id"] = ""; //DIČ
                $data["genius"]["vat_id"] = ""; //IČ DPH
                $data["genius"]["bank_name"] = "";
                $data["genius"]["bank_account"] = "190328169";
                $data["genius"]["iban"] = "DE08100500000190328169";
                $data["genius"]["swift_bic"] = "BELADEBEXXX";
                break;
            case 2: //scbudapest.hu
                $data["address"] = "Suite 2, First Floor, Kenwood House <br/>77A Shenley Rd., Borehamwood WD6 1AG <br/>United Kingdom<br/>";
                $data["address_2"] = "Üllői út 390.<br /> 1184 Budapest<br /> Hungary";
                $data["company_number"] = "09152610";
                $data["ico"] = '50796097'; //IČO
                $data["tax_id"] = "24671378-2-43"; //DIČ
                $data["vat_id"] = "HU24671378"; //IČ DPH
                $data["bank_name"] = "Raiffeisen Bank Zrt. 0009";
                $data["bank_account"] = "12011856-01549575-00100009";
                $data["iban"] = "HU75 1201 1856 0154 9575 0010 0009";
                $data["swift_bic"] = "";

                $data["genius"]["address"] = "Suite 2, First Floor, Kenwood House <br/>77A Shenley Rd., Borehamwood WD6 1AG <br/>United Kingdom<br/>";
                $data["genius"]["address_2"] = "Dunckerstr. 86<br />10437 Berlin<br />Deutschland<br />";
                $data["genius"]["company_number"] = "09152610";
                $data["genius"]["ico"] = '50796097'; //IČO
                $data["genius"]["tax_id"] = ""; //DIČ
                $data["genius"]["vat_id"] = ""; //IČ DPH
                $data["genius"]["bank_name"] = "";
                $data["genius"]["bank_account"] = "190328169";
                $data["genius"]["iban"] = "DE08100500000190328169";
                $data["genius"]["swift_bic"] = "BELADEBEXXX";
                break;
            case 3: //scpoznan.pl
                $data["address"] = "Suite 2, First Floor, Kenwood House <br/>77A Shenley Rd., Borehamwood WD6 1AG <br/>United Kingdom<br/>";
                $data["address_2"] = "Hradbová 19<br/>040 01 Košice, Slovakia<br/>";
                $data["company_number"] = "09152610";
                $data["ico"] = '50796097'; //IČO
                $data["tax_id"] = ""; //DIČ
                $data["vat_id"] = ""; //IČ DPH
                $data["bank_name"] = "ING Bank Śląski S.A.";
                $data["bank_account"] = "08 1050 1520 1000 0024 0879 6833";
                $data["iban"] = "PL 08 1050 1520 1000 0024 0879 6833";
                $data["swift_bic"] = "INGBPLPW";

                $data["genius"]["address"] = "Suite 2, First Floor, Kenwood House <br/>77A Shenley Rd., Borehamwood WD6 1AG <br/>United Kingdom<br/>";
                $data["genius"]["address_2"] = "Biuro obsługi sklepu<br />ul. Wierzbięcice 9<br />61-569 Poznań<br />Polska";
                $data["genius"]["company_number"] = "09152610";
                $data["genius"]["ico"] = '50796097'; //IČO
                $data["genius"]["tax_id"] = ""; //DIČ
                $data["genius"]["vat_id"] = ""; //IČ DPH
                $data["genius"]["bank_name"] = "ING Bank Śląski S.A.";
                $data["genius"]["bank_account"] = "08 1050 1520 1000 0024 0879 6833";
                $data["genius"]["iban"] = "PL 08 1050 1520 1000 0024 0879 6833";
                $data["genius"]["swift_bic"] = "INGBPLPW";
                break;
            case 4: //exafoto.sk
                $data["address"] = "Suite 2, First Floor, Kenwood House <br/>77A Shenley Rd., Borehamwood WD6 1AG <br/>United Kingdom<br/>";
                $data["address_2"] = "Hradbová 19<br/>040 01 Košice, Slovakia<br/>";
                $data["company_number"] = "‎09152610";
                $data["ico"] = '50796097'; //IČO
                $data["tax_id"] = ""; //DIČ
                $data["vat_id"] = ""; //IČ DPH
                $data["bank_name"] = "Fio banka, a. s.";
                $data["bank_account"] = "‎2700795288/8330";
                $data["iban"] = "SK30 8330 0000 0027 0079 5288";
                $data["swift_bic"] = "FIOZSKBAXXX";

                $data["genius"]["address"] = "Suite 2, First Floor, Kenwood House <br/>77A Shenley Rd., Borehamwood WD6 1AG <br/>United Kingdom<br/>";
                $data["genius"]["address_2"] = "Dunckerstr. 86<br />10437 Berlin<br />Deutschland<br />";
                $data["genius"]["company_number"] = "09152610";
                $data["genius"]["ico"] = '50796097'; //IČO
                $data["genius"]["tax_id"] = ""; //DIČ
                $data["genius"]["vat_id"] = ""; //IČ DPH
                $data["genius"]["bank_name"] = "";
                $data["genius"]["bank_account"] = "190328169";
                $data["genius"]["iban"] = "DE08100500000190328169";
                $data["genius"]["swift_bic"] = "BELADEBEXXX";
                break;
            case 5: //exafoto.cz
                $data["address"] = "Suite 2, First Floor, Kenwood House <br/>77A Shenley Rd., Borehamwood WD6 1AG <br/>United Kingdom<br/>";
                $data["address_2"] = "V Zátiší 810/1 Mariánske Hory<br />709 00 Ostrava<br />Česká republika<br />";
                $data["company_number"] = "09152610";
                $data["ico"] = '50796097'; //IČO
                $data["tax_id"] = ""; //DIČ
                $data["vat_id"] = ""; //IČ DPH
                $data["bank_name"] = "Fio banka, a. s.";
                $data["bank_account"] = "2501052289 / 2010";
                $data["iban"] = "CZ9020100000002501052289";
                $data["swift_bic"] = "FIOBCZPPXXX";

                $data["genius"]["address"] = "Suite 2, First Floor, Kenwood House <br/>77A Shenley Rd., Borehamwood WD6 1AG <br/>United Kingdom<br/>";
                $data["genius"]["address_2"] = "Dunckerstr. 86<br />10437 Berlin<br />Deutschland<br />";
                $data["genius"]["company_number"] = "09152610";
                $data["genius"]["ico"] = '50796097'; //IČO
                $data["genius"]["tax_id"] = ""; //DIČ
                $data["genius"]["vat_id"] = ""; //IČ DPH
                $data["genius"]["bank_name"] = "";
                $data["genius"]["bank_account"] = "190328169";
                $data["genius"]["iban"] = "DE08100500000190328169";
                $data["genius"]["swift_bic"] = "BELADEBEXXX";
                break;
            case 100: //english.en
                $data["address"] = "Suite 2, First Floor, Kenwood House <br/>77A Shenley Rd., Borehamwood WD6 1AG <br/>United Kingdom<br/>";
                $data["address_2"] = "Hradbová 19<br/>040 01 Košice, Slovakia<br/>";
                $data["company_number"] = "09152610d"; //IČO
                $data["tax_id"] = "2023322180"; //DIČ
                $data["vat_id"] = "SK2023322180"; //IČ DPH
                $data["bank_name"] = "Fio banka, a. s.";
                $data["bank_account"] = "2400795294/8330";
                $data["iban"] = "SK2783300000002400795294";
                $data["swift_bic"] = "FIOZSKBAXXX";

                $data["genius"]["address"] = "Suite 2, First Floor, Kenwood House <br/>77A Shenley Rd., Borehamwood WD6 1AG <br/>United Kingdom<br/>";
                $data["genius"]["address_2"] = "Dunckerstr. 86<br />10437 Berlin<br />Deutschland<br />";
                $data["genius"]["company_number"] = "09152610";
                $data["genius"]["ico"] = '50796097'; //IČO
                $data["genius"]["tax_id"] = ""; //DIČ
                $data["genius"]["vat_id"] = ""; //IČ DPH
                $data["genius"]["bank_name"] = "";
                $data["genius"]["bank_account"] = "190328169";
                $data["genius"]["iban"] = "DE08100500000190328169";
                $data["genius"]["swift_bic"] = "BELADEBEXXX";
                break;
            default:
                throw new \Exception("Uknown domain id #{$id}");
        }

        return $data;
    }

    private function restoreCartAfterCancel($orderId)
    {
        $order = $this->orderFacade->getById($orderId);
        $products = $order->getProductItems();
        foreach ($products as $product){
            if(is_null($product->getProduct()))
            {
                continue;
            }
            $this->cartFacade->addProductToCart($product->getProduct()->getId(), $product->getQuantity());
        }
    }
}