<?php

namespace Shopsys\ShopBundle\Controller\Front;

use Shopsys\ShopBundle\Component\Controller\FrontBaseController;
use Shopsys\ShopBundle\Model\Article\Article;
use Shopsys\ShopBundle\Model\Article\ArticleFacade;
use Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryFacade;

class ArticleController extends FrontBaseController
{
    /**
     * @var \Shopsys\ShopBundle\Model\Article\ArticleFacade
     */
    private $articleFacade;

    public function __construct(ArticleFacade $articleFacade, ArticleCategoryFacade $articleCategoryFacade)
    {
        $this->articleFacade = $articleFacade;
        $this->articleCategoryFacade = $articleCategoryFacade;
    }

    /**
     * @param int $id
     */
    public function detailAction($id)
    {
        $article = $this->articleFacade->getVisibleById($id);

        return $this->render($this->getTemplate()->getTemplatePath('/Front/Content/Article/detail.html.twig'), [
            'article' => $article,
        ]);
    }

    public function menuAction()
    {
        $articles = $this->articleFacade->getVisibleArticlesForPlacementOnCurrentDomain(Article::PLACEMENT_TOP_MENU);

        return $this->render($this->getTemplate()->getTemplatePath('/Front/Content/Article/menu.html.twig'), [
            'articles' => $articles,
        ]);
    }

    public function footerAction()
    {
        $articles = $this->articleFacade->getVisibleArticlesForPlacementOnCurrentDomainGroupedByCategory(Article::PLACEMENT_FOOTER, $this->articleCategoryFacade->getAll());

        return $this->render($this->getTemplate()->getTemplatePath('/Front/Content/Article/footer.html.twig'), [
            'groupedArticles' => $articles,
        ]);
    }

    public function headerAction()
    {
        $articles = $this->articleFacade->getVisibleArticlesForPlacementOnCurrentDomainGroupedByCategory(Article::PLACEMENT_TOP_MENU, $this->articleCategoryFacade->getAll());

        return $this->render($this->getTemplate()->getTemplatePath('/Front/Content/Article/header.html.twig'), [
            'groupedArticles' => $articles,
        ]);
    }


    public function footer2Action()
    {
        $articles = $this->articleFacade->getVisibleArticlesForPlacementOnCurrentDomain(Article::PLACEMENT_FOOTER);

        return $this->render('@ShopsysShop/Front/Content/Article/footer_2.html.twig', [
            'articles' => $articles,
        ]);
    }
}
