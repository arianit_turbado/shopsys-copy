<?php

namespace Shopsys\ShopBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use ShopSys\MigrationBundle\Component\Doctrine\Migrations\AbstractMigration;

class Version20180306162246 extends AbstractMigration
{
    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->sql('ALTER TABLE products ADD product_pid VARCHAR(30) DEFAULT NULL');
        $this->sql('ALTER TABLE products ADD product_source VARCHAR(30) DEFAULT NULL');
        $this->sql('ALTER TABLE products ADD product_buy_price NUMERIC(20, 6) DEFAULT NULL');
        $this->sql('ALTER TABLE orders ADD updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
    }

    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
