<?php

namespace Shopsys\ShopBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use ShopSys\MigrationBundle\Component\Doctrine\Migrations\AbstractMigration;

class Version20180527134636 extends AbstractMigration
{
    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->sql('
            CREATE TABLE warranty_claims (
                id SERIAL NOT NULL,
                customer_id INT DEFAULT NULL,
                item_id INT DEFAULT NULL,
                delivery_country_id INT DEFAULT NULL,
                transport_id INT NOT NULL,
                administrator_id INT DEFAULT NULL,
                number VARCHAR(30) NOT NULL,
                created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
                updated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
                order_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
                delivery_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
                serial_number VARCHAR(255) NOT NULL,
                unlocking_code VARCHAR(255) NOT NULL,
                problem_description TEXT DEFAULT NULL,
                physical_condition TEXT DEFAULT NULL,
                additional_comment TEXT DEFAULT NULL,
                delivery_first_name VARCHAR(100) NOT NULL,
                delivery_last_name VARCHAR(100) NOT NULL,
                delivery_company_name VARCHAR(100) DEFAULT NULL,
                delivery_telephone VARCHAR(30) DEFAULT NULL,
                delivery_street VARCHAR(100) NOT NULL,
                delivery_city VARCHAR(100) NOT NULL,
                delivery_postcode VARCHAR(30) NOT NULL,
                domain_id INT NOT NULL,
                administrator_name TEXT DEFAULT NULL,
                PRIMARY KEY(id)
            )');
        $this->sql('CREATE UNIQUE INDEX UNIQ_D35D20B096901F54 ON warranty_claims (number)');
        $this->sql('CREATE INDEX IDX_D35D20B09395C3F3 ON warranty_claims (customer_id)');
        $this->sql('CREATE INDEX IDX_D35D20B0126F525E ON warranty_claims (item_id)');
        $this->sql('CREATE INDEX IDX_D35D20B0E76AA954 ON warranty_claims (delivery_country_id)');
        $this->sql('CREATE INDEX IDX_D35D20B09909C13F ON warranty_claims (transport_id)');
        $this->sql('CREATE INDEX IDX_D35D20B04B09E92C ON warranty_claims (administrator_id)');
        $this->sql('
            ALTER TABLE
                warranty_claims
            ADD
                CONSTRAINT FK_D35D20B09395C3F3 FOREIGN KEY (customer_id) REFERENCES users (id) ON DELETE
            SET
                NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->sql('
            ALTER TABLE
                warranty_claims
            ADD
                CONSTRAINT FK_D35D20B0126F525E FOREIGN KEY (item_id) REFERENCES order_items (id) ON DELETE
            SET
                NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->sql('
            ALTER TABLE
                warranty_claims
            ADD
                CONSTRAINT FK_D35D20B0E76AA954 FOREIGN KEY (delivery_country_id) REFERENCES countries (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->sql('
            ALTER TABLE
                warranty_claims
            ADD
                CONSTRAINT FK_D35D20B09909C13F FOREIGN KEY (transport_id) REFERENCES transports (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->sql('
            ALTER TABLE
                warranty_claims
            ADD
                CONSTRAINT FK_D35D20B04B09E92C FOREIGN KEY (administrator_id) REFERENCES administrators (id) ON DELETE
            SET
                NULL NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->sql('ALTER TABLE product_domains ALTER product_feature SET DEFAULT \'false\'');
        $this->sql('ALTER TABLE product_domains ALTER product_feature SET NOT NULL');
        $this->sql('ALTER TABLE product_domains ALTER product_special SET DEFAULT \'false\'');
        $this->sql('ALTER TABLE product_domains ALTER product_special SET NOT NULL');
        $this->sql('ALTER TABLE product_domains ALTER product_on_sale SET DEFAULT \'false\'');
        $this->sql('ALTER TABLE product_domains ALTER product_on_sale SET NOT NULL');
    }

    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->sql('DROP TABLE warranty_claims');
    }
}
