<?php

namespace Shopsys\ShopBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use ShopSys\MigrationBundle\Component\Doctrine\Migrations\AbstractMigration;

class Version20180624184209 extends AbstractMigration
{
    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->sql('ALTER TABLE warranty_claims ADD status VARCHAR(255) DEFAULT NULL');
        $this->sql('ALTER TABLE warranty_claims ADD resolve_option INT DEFAULT NULL');
        $this->sql('ALTER TABLE parameter_values ALTER text TYPE VARCHAR(10000)');
    }

    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
