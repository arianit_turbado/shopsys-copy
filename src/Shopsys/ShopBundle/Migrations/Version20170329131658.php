<?php

namespace Shopsys\ShopBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use ShopSys\MigrationBundle\Component\Doctrine\Migrations\AbstractMigration;

class Version20170329131658 extends AbstractMigration
{
    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->sql('ALTER TABLE cron_modules DROP CONSTRAINT "cron_modules_pkey"');
        $this->sql('ALTER TABLE cron_modules RENAME COLUMN module_id TO service_id');
        $this->sql('ALTER TABLE cron_modules ADD PRIMARY KEY (service_id)');
    }

    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
