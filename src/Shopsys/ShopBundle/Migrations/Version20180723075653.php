<?php

namespace Shopsys\ShopBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use ShopSys\MigrationBundle\Component\Doctrine\Migrations\AbstractMigration;

class Version20180723075653 extends AbstractMigration
{
    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function up(Schema $schema)
    {


        $this->sql('ALTER TABLE orders ADD pricing_group INT DEFAULT NULL');
        $this->sql('ALTER TABLE order_items ALTER item_helper TYPE VARCHAR(255)');
        $this->sql('ALTER TABLE order_items ALTER item_helper DROP DEFAULT');

    }

    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
