<?php

namespace Shopsys\ShopBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use ShopSys\MigrationBundle\Component\Doctrine\Migrations\AbstractMigration;

class Version20180724222351 extends AbstractMigration
{
    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->sql('CREATE TABLE article_categories (id SERIAL NOT NULL, PRIMARY KEY(id))');
        $this->sql('
            CREATE TABLE article_category_translations (
                id SERIAL NOT NULL,
                translatable_id INT NOT NULL,
                name VARCHAR(255) DEFAULT NULL,
                locale VARCHAR(255) NOT NULL,
                PRIMARY KEY(id)
            )');
        $this->sql('CREATE INDEX IDX_701A466D2C2AC5D3 ON article_category_translations (translatable_id)');
        $this->sql('
            CREATE UNIQUE INDEX article_category_translations_uniq_trans ON article_category_translations (translatable_id, locale)');
        $this->sql('
            ALTER TABLE
                article_category_translations
            ADD
                CONSTRAINT FK_701A466D2C2AC5D3 FOREIGN KEY (translatable_id) REFERENCES article_categories (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->sql('DROP TABLE article_categories');
        $this->sql('DROP TABLE article_category_translations');
    }
}
