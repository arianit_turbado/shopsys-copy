<?php

namespace Shopsys\ShopBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use ShopSys\MigrationBundle\Component\Doctrine\Migrations\AbstractMigration;

class Version20180617221709 extends AbstractMigration
{
    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->sql('
            CREATE TABLE status_histories (
                id SERIAL NOT NULL,
                order_id INT NOT NULL,
                order_status_id INT NOT NULL,
                date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
                PRIMARY KEY(id)
            )');
        $this->sql('CREATE INDEX IDX_47644EE28D9F6D38 ON status_histories (order_id)');
        $this->sql('CREATE INDEX IDX_47644EE2D7707B45 ON status_histories (order_status_id)');
        $this->sql('
            ALTER TABLE
                status_histories
            ADD
                CONSTRAINT FK_47644EE28D9F6D38 FOREIGN KEY (order_id) REFERENCES orders (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->sql('
            ALTER TABLE
                status_histories
            ADD
                CONSTRAINT FK_47644EE2D7707B45 FOREIGN KEY (order_status_id) REFERENCES order_statuses (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
