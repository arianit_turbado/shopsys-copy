<?php

namespace Shopsys\ShopBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use ShopSys\MigrationBundle\Component\Doctrine\Migrations\AbstractMigration;

class Version20180418080313 extends AbstractMigration
{
    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function up(Schema $schema)
    {

        $this->sql('ALTER TABLE product_domains ADD product_feature boolean DEFAULT NULL');
        $this->sql('ALTER TABLE product_domains ADD product_special boolean DEFAULT NULL');
        $this->sql('ALTER TABLE product_domains ADD product_on_sale boolean DEFAULT NULL');
    }

    /**
     * @param \Doctrine\DBAL\Schema\Schema $schema
     */
    public function down(Schema $schema)
    {
    }
}
