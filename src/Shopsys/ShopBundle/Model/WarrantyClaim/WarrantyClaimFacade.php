<?php

namespace Shopsys\ShopBundle\Model\WarrantyClaim;

use Doctrine\ORM\EntityManager;
use Shopsys\ShopBundle\Model\Customer\CustomerFacade;
use Shopsys\ShopBundle\Model\Customer\User;
use Shopsys\ShopBundle\Model\Order\Item\OrderItem;
use Shopsys\ShopBundle\Model\Order\Item\OrderItemFacade;
use Shopsys\ShopBundle\Model\Transport\TransportFacade;

class WarrantyClaimFacade
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \Shopsys\ShopBundle\Model\Payment\PaymentRepository
     */
    private $warrantyClaimRepository;

    /**
     * @var \Shopsys\ShopBundle\Model\Customer\CustomerFacade
     */
    private $customerFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\Item\OrderItemFacade
     */
    private $orderItemFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Transport\TransportFacade
     */
    private $transportFacade;

    public function __construct(
        EntityManager $em,
        WarrantyClaimRepository $warrantyClaimRepository,
        CustomerFacade $customerFacade,
        OrderItemFacade $orderItemFacade,
        TransportFacade $transportFacade
    )
    {
        $this->em = $em;
        $this->warrantyClaimRepository = $warrantyClaimRepository;
        $this->customerFacade = $customerFacade;
        $this->orderItemFacade = $orderItemFacade;
        $this->transportFacade = $transportFacade;
    }

    public function getByWarrantyNumberAndUser($warranty_number, User $user = null)
    {
        return $this->warrantyClaimRepository->getByWarrantyNumberAndUser($warranty_number, $user);
    }

    public function getByWarrantyNumber($warranty_number)
    {
        return $this->warrantyClaimRepository->getByWarrantyNumber($warranty_number);
    }

    public function getByWarrantyId($warranty_id)
    {
        return $this->warrantyClaimRepository->getByWarrantyId($warranty_id);
    }

    public function exportToJson($warranty_number)
    {
        $shopIds = [1 => '20', 2 => '9', 3 => '10', 4 => '7', 5 => '8'];
        $warranty = $this->getByWarrantyId($warranty_number);
        if(empty($warranty->getOrderDate())) {
            $orderDate = null;
        } else {
            $orderDate = $warranty->getOrderDate()->format('Y-m-d H:i:s');
        }

        if(empty($warranty->getDeliveryDate())) {
            $orderDeliveryDate = null;
        } else {
            $orderDeliveryDate = $warranty->getDeliveryDate()->format('Y-m-d H:i:s');
        }

        $response = [
            'id' => $warranty->getId(),
            'number' => $warranty->getNumber(),
            'order_number' => $warranty->getItem()->getOrder()->getNumber(),
            'order_id' => $warranty->getItem()->getOrder()->getId(),
            'customer' => $warranty->getCustomerFullName(),
            'item' => $warranty->getItem()->getName(),
            'item_id' => $warranty->getItem()->getId(),
            'delivery_country' => $warranty->getDeliveryCountry()->getName(),
            'transport_id' => $warranty->getTransportId(),
            'order_date' => $orderDate,
            'delivery_date' => $orderDeliveryDate,
            'serial_number' => $warranty->getSerialNumber(),
            'unlocking_code' => $warranty->getUnlockingCode(),
            'problem_description' => $warranty->getProblemDescription(),
            'physical_condition' => $warranty->getPhysicalCondition(),
            'additional_comment' => $warranty->getAdditionalComment(),
            'delivery_first_name' => $warranty->getDeliveryFirstName(),
            'delivery_last_name' => $warranty->getDeliveryLastName(),
            'delivery_company_name' => $warranty->getDeliveryCompanyName(),
            'delivery_telephone' => $warranty->getDeliveryTelephone(),
            'delivery_street' => $warranty->getDeliveryStreet(),
            'delivery_city' => $warranty->getDeliveryCity(),
            'delivery_postcode' => $warranty->getDeliveryPostcode(),
            'shop_id' => $shopIds[$warranty->getDomainId()],
            'status' => $warranty->getStatus(),
            'email' => $warranty->getItem()->getOrder()->getEmail(),
            'resolve_option' => $warranty->getResolveOptionId()
        ];
        return $response;
    }

    public function create($warrantyData)
    {
        $transport = $this->transportFacade->getById($warrantyData->transport);
        return $this->warrantyClaimRepository->create($warrantyData, $transport);
    }

    public function edit($warrantyId, $warrantyData)
    {
        $transport = $this->transportFacade->getById($warrantyData->transport);
        return $this->warrantyClaimRepository->edit($warrantyId, $warrantyData, $transport);
    }
}
