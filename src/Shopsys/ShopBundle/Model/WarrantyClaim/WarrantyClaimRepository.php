<?php

namespace Shopsys\ShopBundle\Model\WarrantyClaim;

use Doctrine\ORM\EntityManager;
use Shopsys\ShopBundle\Model\Customer\CustomerFacade;
use Shopsys\ShopBundle\Model\Customer\User;
use Shopsys\ShopBundle\Model\WarrantyClaim\WarrantyClaim;
use Shopsys\ShopBundle\Model\Order\Item\OrderItem;

class WarrantyClaimRepository
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $customerFacade;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     * @param \Shopsys\ShopBundle\Model\Customer\CustomerFacade $customerFacade
     */
    public function __construct(EntityManager $em, CustomerFacade $customerFacade)
    {
        $this->em = $em;
        $this->customerFacade = $customerFacade;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getWarrantyClaimRepository()
    {
        return $this->em->getRepository(WarrantyClaim::class);
    }

    public function createWarrantyClaimQueryBuilder()
    {
        return $this->em->createQueryBuilder()
            ->select('w')
            ->from(WarrantyClaim::class, 'w');
    }

    public function getByWarrantyNumberAndUser($warrantyNumber, User $user = null)
    {
        $warranty = $this->createWarrantyClaimQueryBuilder()
            ->andWhere('w.number = :number')->setParameter(':number', $warrantyNumber);

        if (!is_null($user)) {
            $warranty = $warranty->andWhere()->andWhere('w.customer = :customer')->setParameter(':customer', $user);
        }

        $warranty = $warranty->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();

        if ($warranty === null) {
            $message = 'Warranty with number "' . $warrantyNumber . '" and Customer id "' . $user->getId() . '" not found.';
            throw new \Exception($message);
        }

        return $warranty;
    }

    public function getByWarrantyNumber($warranty_number)
    {
        $warranty = $this->createWarrantyClaimQueryBuilder()
            ->where('w.number = :number')->setParameter(':number', $warranty_number)
            ->setMaxResults(1)->getQuery()->getOneOrNullResult();

        if ($warranty === null) {
            $message = 'Warranty with number "' . $warranty_number . '" not found !';
            throw new \Exception($message);
        }
        return $warranty;
    }

    public function getByWarrantyId($warranty_id)
    {
        $warranty = $this->createWarrantyClaimQueryBuilder()
            ->where('w.id = :id')
            ->setParameter(':id', $warranty_id)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();

        if ($warranty === null) {
            $message = 'Warranty with Id "' . $warranty_id . '" not found !';
            throw new \Exception($message);
        }
        return $warranty;
    }

    public function create($warrantyData, $transport)
    {
        $shopIds = [1 => '20', 2 => '9', 3 => '10', 4 => '7', 5 => '8'];
        $customer = null;

        $item = $this->em->createQueryBuilder()
            ->select('i')
            ->from(OrderItem::class, 'i')
            ->where('i.id = :id')
            ->setParameter(':id', $warrantyData->item)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();

        if(!is_null($warrantyData->customer))
        {
            $customer = $this->customerFacade->getUserById($warrantyData->customer);
        } else {
            $customer = $item->getOrder()->getCustomer();
        }

        $warrantyData->setTransportByEntity($transport);
        $warrantyData->setItemByEntity($item, false);
        $warrantyData->setCustomerByEntity($customer);

        if(array_key_exists($warrantyData->domain_id, $shopIds)) {
            $shopId = $shopIds[$warrantyData->domain_id];
        } else {
            $shopId = $warrantyData->shop_id;
            $newDomainId = array_search($warrantyData->shop_id, $shopIds);
            $warrantyData->domain_id = $newDomainId;
        }

        $stamp = date('ymdHis');
        $orderId = $warrantyData->item->getOrder()->getNumber();
        $warrantyData->number = "RMA/{$stamp}/{$shopId}/{$orderId}";
        $warranty = new WarrantyClaim($warrantyData);
        $this->em->persist($warranty);
        /*var_dump($warranty->getId());
        var_dump($warranty->getItem()->getName());
        var_dump($warranty->getNumber());
        //var_dump($warranty->getCustomer()->getFullName());
        var_dump($warranty->getOrderDate());
        var_dump($warranty->getDeliveryDate());
        var_dump($warranty->getSerialNumber());
        var_dump($warranty->getUnlockingCode());
        var_dump($warranty->getProblemDescription());
        var_dump($warranty->getPhysicalCondition());
        var_dump($warranty->getAdditionalComment());
        var_dump($warranty->getDeliveryFirstName());
        var_dump($warranty->getDeliveryLastName());
        var_dump($warranty->getDeliveryCompanyName());
        var_dump($warranty->getDeliveryTelephone());
        var_dump($warranty->getDeliveryStreet());
        var_dump($warranty->getDeliveryCity());
        var_dump($warranty->getDeliveryPostcode());
        var_dump($warranty->getDomainId());
        var_dump($warranty->getAdministratorName());
        var_dump($warranty->getAdministrator());
        var_dump($warranty->getDeliveryCountry()->getName());
        var_dump($warranty->getTransport()->getName());*/
        //exit();
        $this->em->flush($warranty);
        return $warranty;
    }

    public function edit($warrantyId, $warrantyData, $transport)
    {
        $warranty = $this->em->getRepository(WarrantyClaim::class)->find($warrantyId);
        $customer = null;

        if(!is_null($warrantyData->customer))
        {
            $customer = $this->customerFacade->getUserById($warrantyData->customer);
        }

        $item = $this->em->createQueryBuilder()
            ->select('i')
            ->from(OrderItem::class, 'i')
            ->where('i.id = :id')
            ->setParameter(':id', $warrantyData->item)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();

        $warrantyData->setTransportByEntity($transport);
        $warrantyData->setItemByEntity($item, false);
        $warrantyData->setCustomerByEntity($customer);
        $warranty->updateFromWarrantyData($warrantyData);
        $this->em->flush($warranty);
        return $warranty;
    }
}
