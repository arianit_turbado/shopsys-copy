<?php

namespace Shopsys\ShopBundle\Model\WarrantyClaim;

use Doctrine\ORM\Mapping as ORM;
use Shopsys\ShopBundle\Model\Order\Item\OrderItem;
use DateTime;
use Shopsys\ShopBundle\Model\Administrator\Administrator;
use Shopsys\ShopBundle\Model\Country\Country;
use Shopsys\ShopBundle\Model\Transport\Transport;
use Shopsys\ShopBundle\Model\Customer\User;
use Shopsys\ShopBundle\Model\WarrantyClaim\WarrantyData;
use Symfony\Component\Validator\Constraints\Time;
use GuzzleHttp\Client;

/**
 * @ORM\Table(name="warranty_claims")
 * @ORM\Entity
 * @SuppressWarnings(PHPMD.ExcessivePublicCount)
 */

class WarrantyClaim
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=30, unique=true, nullable=false)
     */
    private $number;

    /**
     * @var \Shopsys\ShopBundle\Model\Customer\User|null
     *
     * @ORM\ManyToOne(targetEntity="Shopsys\ShopBundle\Model\Customer\User")
     * @ORM\JoinColumn(nullable=true, name="customer_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $customer;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\Item\OrderItem
     * @ORM\ManyToOne(targetEntity="Shopsys\ShopBundle\Model\Order\Item\OrderItem")
     * @ORM\JoinColumn(nullable=true, name="item_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $item;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $orderDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $deliveryDate;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $serialNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $status;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $resolveOption;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $unlockingCode;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $problemDescription;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $physicalCondition;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $additionalComment;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $deliveryFirstName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $deliveryLastName;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=100, nullable=true)
     */
    private $deliveryCompanyName;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=30, nullable=true)
     */
    private $deliveryTelephone;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=100)
     */
    private $deliveryStreet;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=100)
     */
    private $deliveryCity;

    /**
     * @var string|null
     *
     * @ORM\Column(type="string", length=30)
     */
    private $deliveryPostcode;

    /**
     * @var \Shopsys\ShopBundle\Model\Country\Country|null
     * @ORM\ManyToOne(targetEntity="Shopsys\ShopBundle\Model\Country\Country")
     * @ORM\JoinColumn(name="delivery_country_id", referencedColumnName="id", nullable=true)
     */
    private $deliveryCountry;

    /**
     * @var \Shopsys\ShopBundle\Model\Transport\Transport
     *
     * @ORM\ManyToOne(targetEntity="Shopsys\ShopBundle\Model\Transport\Transport")
     * @ORM\JoinColumn(nullable=false)
     */
    private $transport;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $domainId;

    /**
     * @var \Shopsys\ShopBundle\Model\Administrator\Administrator|null
     *
     * @ORM\ManyToOne(targetEntity="Shopsys\ShopBundle\Model\Administrator\Administrator")
     * @ORM\JoinColumn(nullable=true, name="administrator_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $administrator;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $administratorName;

    /**
     * @param \Shopsys\ShopBundle\Model\WarrantyClaim\WarrantyData $warrantyData
     * @param \Shopsys\ShopBundle\Model\Customer\User $user
     * @param \Shopsys\ShopBundle\Model\Transport\Transport $transport
     * @param \Shopsys\ShopBundle\Model\Order\Item\OrderItem $item
     */
    public function __construct(WarrantyData $warrantyData)
    {
        $this->customer = $warrantyData->customer;
        $this->transport = $warrantyData->transport;
        $this->item = $warrantyData->item;
        $this->number = $warrantyData->number;
        $this->deliveryStreet = $warrantyData->delivery_street;
        $this->deliveryTelephone = $warrantyData->delivery_telephone;
        $this->deliveryCompanyName = $warrantyData->delivery_company_name;
        $this->orderDate = $warrantyData->order_date;
        $this->deliveryDate = $warrantyData->delivery_date;
        $this->problemDescription = $warrantyData->problem_description;
        $this->deliveryLastName = $warrantyData->delivery_last_name;
        $this->deliveryFirstName = $warrantyData->delivery_first_name;
        $this->additionalComment = $warrantyData->additional_comment;
        $this->physicalCondition = $warrantyData->physical_condition;
        $this->serialNumber = $warrantyData->serial_number;
        $this->unlockingCode = $warrantyData->unlocking_code;
        $this->deliveryCity = $warrantyData->delivery_city;
        $this->deliveryPostcode = $warrantyData->delivery_postcode;
        $this->deliveryCountry = $warrantyData->delivery_country;
        $this->domainId = $warrantyData->domain_id;
        $this->createdAt = new DateTime();
        $this->updatedAt = $this->createdAt;
        $this->status = $warrantyData->status;
        $this->resolveOption = $warrantyData->resolve_option;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getItem()
    {
        return $this->item;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function getCustomer()
    {
        return $this->customer;
    }

    public function getCustomerFullName()
    {
        if(is_null($this->getCustomer())) {
            return null;
        } else {
            return $this->getCustomer()->getFullName();
        }
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    public function getOrderDate()
    {
        return $this->orderDate;
    }

    public function getDeliveryDate()
    {
        return $this->deliveryDate;
    }

    public function getSerialNumber()
    {
        return $this->serialNumber;
    }

    public function getUnlockingCode()
    {
        return $this->unlockingCode;
    }

    public function getProblemDescription()
    {
        return $this->problemDescription;
    }

    public function getPhysicalCondition()
    {
        return $this->physicalCondition;
    }

    public function getAdditionalComment()
    {
        return $this->additionalComment;
    }

    public function getDeliveryFirstName()
    {
        return $this->deliveryFirstName;
    }

    public function getDeliveryLastName()
    {
        return $this->deliveryLastName;
    }

    public function getDeliveryCompanyName()
    {
        return $this->deliveryCompanyName;
    }

    public function getDeliveryTelephone()
    {
        return $this->deliveryTelephone;
    }

    public function getDeliveryStreet()
    {
        return $this->deliveryStreet;
    }

    /**
     * @return null|string
     */
    public function getDeliveryCity(): ?string
    {
        return $this->deliveryCity;
    }

    /**
     * @return null|string
     */
    public function getDeliveryPostcode(): ?string
    {
        return $this->deliveryPostcode;
    }

    /**
     * @return int
     */
    public function getDomainId()
    {
        return $this->domainId;
    }

    /**
     * @return null|string
     */
    public function getAdministratorName(): ?string
    {
        return $this->administrator;
    }

    /**
     * @return null|\Shopsys\ShopBundle\Model\Administrator\Administrator
     */
    public function getAdministrator(): ?Administrator
    {
        return $this->administrator;
    }

    /**
     * @return null|\Shopsys\ShopBundle\Model\Country\Country
     */
    public function getDeliveryCountry(): ?Country
    {
        return $this->deliveryCountry;
    }

    /**
     * @return \Shopsys\ShopBundle\Model\Transport\Transport
     */
    public function getTransport(): Transport
    {
        return $this->transport;
    }

    public function getTransportId(){
        if(is_null($this->getTransport())) {
            return null;
        } else {
            return $this->getTransport()->getId();
        }
    }

    /**
     * @param null|string $additionalComment
     */
    public function setAdditionalComment(?string $additionalComment): void
    {
        $this->additionalComment = $additionalComment;
    }

    /**
     * @param null|\Shopsys\ShopBundle\Model\Administrator\Administrator $administrataor
     */
    public function setAdministrator(?Administrator $administrator): void
    {
        $this->administrator = $administrator;
    }

    /**
     * @param null|string $administratorName
     */
    public function setAdministratorName(?string $administratorName): void
    {
        $this->administratorName = $administratorName;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @param null|\Shopsys\ShopBundle\Model\Customer\User $customer
     */
    public function setCustomer(?User $customer): void
    {
        $this->customer = $customer;
    }

    /**
     * @param null|string $deliveryCity
     */
    public function setDeliveryCity(?string $deliveryCity): void
    {
        $this->deliveryCity = $deliveryCity;
    }

    /**
     * @param null|string $deliveryCompanyName
     */
    public function setDeliveryCompanyName(?string $deliveryCompanyName): void
    {
        $this->deliveryCompanyName = $deliveryCompanyName;
    }

    /**
     * @param null|\Shopsys\ShopBundle\Model\Country\Country $deliveryCountry
     */
    public function setDeliveryCountry(?Country $deliveryCountry): void
    {
        $this->deliveryCountry = $deliveryCountry;
    }

    /**
     * @param \DateTime $deliveryDate
     */
    public function setDeliveryDate(DateTime $deliveryDate): void
    {
        $this->deliveryDate = $deliveryDate;
    }

    /**
     * @param string $deliveryFirstName
     */
    public function setDeliveryFirstName(string $deliveryFirstName): void
    {
        $this->deliveryFirstName = $deliveryFirstName;
    }

    /**
     * @param string $deliveryLastName
     */
    public function setDeliveryLastName(string $deliveryLastName): void
    {
        $this->deliveryLastName = $deliveryLastName;
    }

    /**
     * @param null|string $deliveryPostcode
     */
    public function setDeliveryPostcode(?string $deliveryPostcode): void
    {
        $this->deliveryPostcode = $deliveryPostcode;
    }

    /**
     * @param string $deliveryStreet
     */
    public function setDeliveryStreet(string $deliveryStreet): void
    {
        $this->deliveryStreet = $deliveryStreet;
    }

    /**
     * @param string $deliveryTelephone
     */
    public function setDeliveryTelephone(string $deliveryTelephone): void
    {
        $this->deliveryTelephone = $deliveryTelephone;
    }

    /**
     * @param int $domainId
     */
    public function setDomainId(int $domainId): void
    {
        $this->domainId = $domainId;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @param \Shopsys\ShopBundle\Model\Order\Item\OrderItem $item
     */
    public function setItem(OrderItem $item): void
    {
        $this->item = $item;
    }

    /**
     * @param string $number
     */
    public function setNumber(string $number): void
    {
        $this->number = $number;
    }

    /**
     * @param \DateTime $orderDate
     */
    public function setOrderDate(DateTime $orderDate): void
    {
        $this->orderDate = $orderDate;
    }

    /**
     * @param null|string $physicalCondition
     */
    public function setPhysicalCondition(?string $physicalCondition): void
    {
        $this->physicalCondition = $physicalCondition;
    }

    /**
     * @param null|string $problemDescription
     */
    public function setProblemDescription(?string $problemDescription): void
    {
        $this->problemDescription = $problemDescription;
    }

    /**
     * @param string $serialNumber
     */
    public function setSerialNumber(string $serialNumber): void
    {
        $this->serialNumber = $serialNumber;
    }

    /**
     * @param \Shopsys\ShopBundle\Model\Transport\Transport $transport
     */
    public function setTransport(Transport $transport): void
    {
        $this->transport = $transport;
    }

    /**
     * @param string $unlockingCode
     */
    public function setUnlockingCode(string $unlockingCode): void
    {
        $this->unlockingCode = $unlockingCode;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getOrderNumber()
    {
        return $this->getItem()->getOrder()->getNumber();
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus(string $status) : void
    {
        $this->status = $status;
    }

    public function getResolveOption()
    {
        $resolve_options_path = "http://192.168.0.109:9000/api/rma_resolve_options/single/{$this->resolveOption}";
        $client = new Client();
        try{
            $request = $client->request('GET', $resolve_options_path);
            $result = json_decode($request->getBody()->__toString(), true);
        } catch (\Exception $ex) {
            $result = $this->resolveOption;
        }
        return $result;
    }

    public function getResolveOptionId()
    {
        return $this->resolveOption;
    }

    public function setResolveOption(int $resolveOption)
    {
        $this->resolveOption = $resolveOption;
    }

    public function updateFromWarrantyData(WarrantyData $warrantyData)
    {
        if(!empty($warrantyData->customer)){
            $this->customer = $warrantyData->customer;
        }

        if(!empty($warrantyData->transport)){
            $this->transport = $warrantyData->transport;
        }

        if(!empty($warrantyData->item)){
            $this->item = $warrantyData->item;
        }

        if(!empty($warrantyData->delivery_street)){
            $this->deliveryStreet = $warrantyData->delivery_street;
        }

        if(!empty($warrantyData->delivery_telephone)){
            $this->deliveryTelephone = $warrantyData->delivery_telephone;
        }

        if(!empty($warrantyData->delivery_company_name)){
            $this->deliveryCompanyName = $warrantyData->delivery_company_name;
        }

        if(!empty($warrantyData->order_date)){
            $this->orderDate = $warrantyData->order_date;
        }

        if(!empty($warrantyData->delivery_date)){
            $this->deliveryDate = $warrantyData->delivery_date;
        }
        if(!empty($warrantyData->problem_description)){
            $this->problemDescription = $warrantyData->problem_description;
        }
        if(!empty($warrantyData->delivery_last_name)){
            $this->deliveryLastName = $warrantyData->delivery_last_name;
        }

        if(!empty($warrantyData->delivery_first_name)){
            $this->deliveryFirstName = $warrantyData->delivery_first_name;
        }

        if(!empty($warrantyData->additional_comment)){
            $this->additionalComment = $warrantyData->additional_comment;
        }

        if(!empty($warrantyData->physical_condition)){
            $this->physicalCondition = $warrantyData->physical_condition;
        }

        if(!empty($warrantyData->serial_number)){
            $this->serialNumber = $warrantyData->serial_number;
        }

        if(!empty($warrantyData->unlocking_code)){
            $this->unlockingCode = $warrantyData->unlocking_code;
        }

        if(!empty($warrantyData->delivery_city)){
            $this->deliveryCity = $warrantyData->delivery_city;
        }

        if(!empty($warrantyData->delivery_postcode)){
            $this->deliveryPostcode = $warrantyData->delivery_postcode;
        }

        if(!empty($warrantyData->delivery_country)){
            $this->deliveryCountry = $warrantyData->delivery_country;
        }

        if(!empty($warrantyData->delivery_postcode)){
            $this->deliveryPostcode = $warrantyData->delivery_postcode;
        }

        if(!empty($warrantyData->status)){
            $this->status = $warrantyData->status;
        }

        if(!empty($warrantyData->resolve_option)){
            $this->resolveOption = $warrantyData->resolve_option;
        }
    }
}
