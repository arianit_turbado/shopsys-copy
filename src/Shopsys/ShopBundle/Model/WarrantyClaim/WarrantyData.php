<?php

namespace Shopsys\ShopBundle\Model\WarrantyClaim;

use Shopsys\ShopBundle\Model\Customer\User;
use Shopsys\ShopBundle\Model\Order\Item\OrderItem;
use Shopsys\ShopBundle\Model\Transport\Transport;
use Shopsys\ShopBundle\Model\Transport\TransportFacade;
use Shopsys\ShopBundle\Model\Order\Item\OrderItemFacade;

class WarrantyData
{
    public $customer;

    public $item;

    public $delivery_country;

    public $administrator_id;

    public $order_date;

    public $delivery_date;

    public $serial_number;

    public $unlocking_code;

    public $problem_description;

    public $physical_condition;

    public $additional_comment;

    public $delivery_first_name;

    public $delivery_last_name;

    public $delivery_company_name;

    public $delivery_telephone;

    public $delivery_city;

    public $delivery_street;

    public $delivery_postcode;

    public $transport;

    public $number;

    public $domain_id;

    public $status;

    public $resolve_option;

    public $shop_id;

    public function __construct($options = Array())
    {
        if (array_key_exists('customer_form', $options) && !is_null($options['customer_form'])) {
            $this->customer = $options['customer_form'];
        }

        if (array_key_exists('item', $options)) {
            $this->item = $options['item'];
        }

        if (array_key_exists('item_id', $options)) {
            $this->item = $options['item_id'];
        }

        if (array_key_exists('transport', $options)) {
            $this->transport = $options['transport'];
        }

        if (array_key_exists('transport_id', $options)) {
            $this->transport = $options['transport_id'];
        }

        if (array_key_exists('delivery_country', $options)) {
            $this->delivery_country = $options['delivery_country'];
        }

        if (array_key_exists('administrator_id', $options)) {
            $this->administrator_id = $options['administrator_id'];
        }

        if (array_key_exists('order_date', $options)) {
            $this->order_date = $options['order_date'];
        }

        if (array_key_exists('delivery_date', $options)) {
            $this->delivery_date = $options['delivery_date'];
        }

        if (array_key_exists('serial_number', $options)) {
            $this->serial_number = $options['serial_number'];
        }

        if (array_key_exists('unlocking_code', $options)) {
            $this->unlocking_code = $options['unlocking_code'];
        }

        if (array_key_exists('physical_condition', $options)) {
            $this->physical_condition = $options['physical_condition'];
        }

        if (array_key_exists('problem_description', $options)) {
            $this->problem_description = $options['problem_description'];
        }

        if (array_key_exists('additional_comment', $options)) {
            $this->additional_comment = $options['additional_comment'];
        }

        if (array_key_exists('delivery_first_name', $options)) {
            $this->delivery_first_name = $options['delivery_first_name'];
        }
        if (array_key_exists('delivery_last_name', $options)) {
            $this->delivery_last_name = $options['delivery_last_name'];
        }

        if (array_key_exists('delivery_company_name', $options)) {
            $this->delivery_company_name = $options['delivery_company_name'];
        }

        if (array_key_exists('delivery_telephone', $options)) {
            $this->delivery_telephone = $options['delivery_telephone'];
        }

        if (array_key_exists('delivery_city', $options)) {
            $this->delivery_city = $options['delivery_city'];
        }
        if (array_key_exists('delivery_postcode', $options)) {
            $this->delivery_postcode = $options['delivery_postcode'];
        }
        if (array_key_exists('delivery_street', $options)) {
            $this->delivery_street = $options['delivery_street'];
        }

        if (array_key_exists('status', $options)) {
            $this->status = $options['status'];
        }

        if (array_key_exists('resolve_option', $options)) {
            $this->resolve_option = $options['resolve_option'];
        }

        if (array_key_exists('shop_id', $options)) {
            $this->shop_id = $options['shop_id'];
        }
    }

    public function setCustomerByEntity(User $user = null)
    {
        $this->customer = $user;
    }

    public function setItemByEntity(OrderItem $item, $related_item_fields = true)
    {
        $this->item = $item;
        if($related_item_fields)
        {
            $item_order = $this->item->getOrder();
            $this->order_date = $item_order->getCreatedAt();
            $this->delivery_country = $item_order->getDeliveryCountry();
            $this->delivery_postcode = $item_order->getDeliveryPostcode();
            $this->delivery_city = $item_order->getDeliveryCity();
            $this->delivery_first_name = $item_order->getDeliveryFirstName();
            $this->delivery_last_name = $item_order->getDeliveryLastName();
            $this->delivery_company_name = $item_order->getDeliveryCompanyName();
            $this->delivery_street = $item_order->getDeliveryStreet();
            $this->delivery_telephone = $item_order->getDeliveryTelephone();
        }
    }

    public function setTransportByEntity(Transport $transport)
    {
        $this->transport = $transport;
    }

}
