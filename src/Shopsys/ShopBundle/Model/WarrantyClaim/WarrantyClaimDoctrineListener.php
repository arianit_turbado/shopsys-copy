<?php

namespace Shopsys\ShopBundle\Model\WarrantyClaim;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use GuzzleHttp\Client;
use Shopsys\ShopBundle\Model\Mail\MailerService;
use Shopsys\ShopBundle\Model\Order\StatusHistory;
use Shopsys\ShopBundle\Model\Mail\MessageData;
use Shopsys\ShopBundle\Model\WarrantyClaim\WarrantyClaim;

class WarrantyClaimDoctrineListener
{
    private $warrantyClaimRegisterUrl = "http://orders.turbado.eu/api/warranty/store/external";
    /**
     * @param \Doctrine\ORM\Event\LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        try {
            if ($entity instanceof WarrantyClaim) {

            }
        } catch (\Exception $ex) {

        }
        return;
    }
}