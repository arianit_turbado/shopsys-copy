<?php

namespace Shopsys\ShopBundle\Model\ArticleCategory;

use Shopsys\ShopBundle\Form\UrlListData;

class ArticleCategoryData
{
    /**
     * @var string[]
     */
    public $name;


    /**
     * @var string
     */
    public $placement;


    /**
     * @var string
     */
    public $iconName;

    public function __construct()
    {
        $this->name = [];
    }

    /**
     * @param \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategory $articleCategory
     */
    public function setFromEntity(ArticleCategory $articleCategory)
    {
        $translations = $articleCategory->getTranslations();
        $names = [];
        foreach ($translations as $translate) {
            $names[$translate->getLocale()] = $translate->getName();
        }
        $this->name = $names;
        $this->placement = $articleCategory->getPlacement();
        $this->iconName = $articleCategory->getIconName();
    }
}
