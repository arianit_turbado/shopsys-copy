<?php

namespace Shopsys\ShopBundle\Model\ArticleCategory;

class ArticleCategoryService
{
    /**
     * @param \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryData $articleCategoryData
     * @return \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategory
     */
    public function create(ArticleCategoryData $articleCategoryData)
    {
        $articleCategory = new ArticleCategory($articleCategoryData);
        return $articleCategory;
    }

    /**
     * @param \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategory $articleCategory
     * @param \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryData $articleCategoryData
     * @return \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategory
     */
    public function edit(ArticleCategory $articleCategory, ArticleCategoryData $articleCategoryData)
    {
        $articleCategory->edit($articleCategoryData);
        return $articleCategory;
    }
}
