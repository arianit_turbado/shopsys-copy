<?php

namespace Shopsys\ShopBundle\Model\ArticleCategory;

use Doctrine\ORM\EntityManager;
use Shopsys\ShopBundle\Component\Plugin\PluginCrudExtensionFacade;
use Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryRepository;
use Shopsys\ShopBundle\Model\Localization\Localization;
use Doctrine\ORM\Query\Expr\Join;

class ArticleCategoryFacade
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryRepository
     */
    private $articleCategoryRepository;

    /**
     * @var \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryService
     */
    private $articleCategoryService;

    /**
     * @var \Shopsys\ShopBundle\Component\Plugin\PluginCrudExtensionFacade
     */
    private $pluginCrudExtensionFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Localization\Localization
     */
    private $localization;

    public function __construct(
        EntityManager $em,
        ArticleCategoryRepository $articleCategoryRepository,
        ArticleCategoryService $articleCategoryService,
        Localization $localization,
        PluginCrudExtensionFacade $pluginCrudExtensionFacade
    )
    {
        $this->em = $em;
        $this->articleCategoryRepository = $articleCategoryRepository;
        $this->articleCategoryService = $articleCategoryService;
        $this->localization = $localization;
        $this->pluginCrudExtensionFacade = $pluginCrudExtensionFacade;
    }

    /**
     * @param \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryData $articleCategoryData
     * @return \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategory
     */
    public function create(ArticleCategoryData $articleCategoryData)
    {
        $articleCategory = $this->articleCategoryService->create($articleCategoryData);
        $this->em->persist($articleCategory);
        $this->em->flush($articleCategory);
        return $articleCategory;
    }

    /**
     * @param int $articleCategoryId
     * @param \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryData $articleCategoryData
     * @return \Shopsys\ShopBundle\Model\Category\Category
     */
    public function edit($articleCategoryId, ArticleCategoryData $articleCategoryData)
    {
        $articleCategory = $this->articleCategoryRepository->getById($articleCategoryId);
        $this->articleCategoryService->edit($articleCategory, $articleCategoryData);
        $this->em->flush();
        return $articleCategory;
    }

    /**
     * @param int $articleCategoryId
     * @return \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategory
     */
    public function getById($articleCategoryId)
    {
        return $this->articleCategoryRepository->getById($articleCategoryId);
    }

    /**
     * @param int $articleCategoryId
     */
    public function deleteById($articleCategoryId)
    {
        $articleCategory = $this->articleCategoryRepository->getById($articleCategoryId);
        // Normally, UnitOfWork performs UPDATEs on children after DELETE of main entity.
        // We need to update `parent` attribute of children first.
        $this->em->flush();

        $this->em->remove($articleCategory);
        $this->em->flush();
    }

    public function getArticleCategoryQueryBuilder()
    {
        $queryBuilder = $this->em->createQueryBuilder();
        $queryBuilder
            ->select('ac, act')
            ->from(ArticleCategory::class, 'ac')
            ->leftJoin('ac.translations', 'act', Join::WITH, 'act.locale = :locale')
            ->setParameters([
                'locale' => $this->localization->getAdminLocale(),
            ]);

        return $queryBuilder;
    }

    public function getAll()
    {
        return $this->articleCategoryRepository->getAll();
    }

}
