<?php

namespace Shopsys\ShopBundle\Model\ArticleCategory;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use Shopsys\ShopBundle\Component\Domain\Config\DomainConfig;
use Shopsys\ShopBundle\Component\Paginator\QueryPaginator;
use Shopsys\ShopBundle\Component\String\DatabaseSearching;

class ArticleCategoryRepository
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getArticleCategoryRepository()
    {
        return $this->em->getRepository(ArticleCategory::class);
    }

    /**
     * @param int $articleCategoryId
     * @return \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategory
     */
    public function getById($articleCategoryId)
    {
        $articleCategory = $this->findById($articleCategoryId);

        if ($articleCategory === null) {
            $message = 'Category with ID ' . $articleCategoryId . ' not found.';
            throw new \Exception($message);
        }

        return $articleCategory;
    }

    /**
     * @param int $articleCategoryId
     * @return \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategory|null
     */
    public function findById($articleCategoryId)
    {
        $articleCategory = $this->getArticleCategoryRepository()->find($articleCategoryId);
        /* @var $articleCategory \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategory */

        return $articleCategory;
    }

    private function getAllQueryBuilder()
    {
        return $this->getArticleCategoryRepository()
            ->createQueryBuilder('ac')
            ->orderBy('ac.id');
    }

    public function getAll()
    {
        return $this->getAllQueryBuilder()
            ->getQuery()
            ->getResult();
    }
}
