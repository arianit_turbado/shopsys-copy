<?php

namespace Shopsys\ShopBundle\Model\ArticleCategory;

use Shopsys\ShopBundle\Component\Plugin\PluginCrudExtensionFacade;
use Shopsys\ShopBundle\Component\Router\FriendlyUrl\FriendlyUrlFacade;
use Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryRepository;
use Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryData;

class ArticleCategoryDataFactory
{
    /**
     * @var \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryRepository
     */
    private $articleCategoryRepository;

    /**
     * @var \Shopsys\ShopBundle\Component\Router\FriendlyUrl\FriendlyUrlFacade
     */
    private $friendlyUrlFacade;

    /**
     * @var \Shopsys\ShopBundle\Component\Plugin\PluginCrudExtensionFacade
     */
    private $pluginCrudExtensionFacade;

    public function __construct(
        ArticleCategoryRepository $articleCategoryRepository,
        FriendlyUrlFacade $friendlyUrlFacade,
        PluginCrudExtensionFacade $pluginCrudExtensionFacade
    ) {
        $this->articleCategoryRepository = $articleCategoryRepository;
        $this->friendlyUrlFacade = $friendlyUrlFacade;
        $this->pluginCrudExtensionFacade = $pluginCrudExtensionFacade;
    }

    /**
     * @return \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryData
     */
    public function createDefault()
    {
        return new ArticleCategoryData();
    }

    /**
     * @param \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategory $articleCategory
     * @return \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryData
     */
    public function createFromArticleCategory(ArticleCategory $articleCategory)
    {

        $articleCategoryData = new ArticleCategoryData();
        $articleCategoryData->setFromEntity($articleCategory);
        return $articleCategoryData;
    }
}
