<?php

namespace Shopsys\ShopBundle\Model\ArticleCategory;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Prezent\Doctrine\Translatable\Annotation as Prezent;
use Shopsys\ShopBundle\Model\Localization\AbstractTranslatableEntity;

/**
 * @ORM\Table(name="article_categories")
 * @ORM\Entity
 */
class ArticleCategory extends AbstractTranslatableEntity
{
    const PLACEMENT_HEADER = 'header';
    const PLACEMENT_FOOTER = 'footer';
    const PLACEMENT_HEADER_FOOTER = 'headerFooter';


    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryTranslation[]
     *
     * @Prezent\Translations(targetEntity="Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryTranslation")
     */
    protected $translations;

    /**
     * @var string
     *
     * @ORM\Column(type="text", nullable = true)
     */
    private $placement;


    /**
     * @var string|null
     *
     * @ORM\Column(type="string", nullable=true)
     */
    private $iconName;


    /**
     * @param \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryData $articleCategoryData
     */
    public function __construct(ArticleCategoryData $articleCategoryData)
    {
        $this->translations = new ArrayCollection();
        $this->setTranslations($articleCategoryData);
        $this->placement = $articleCategoryData->placement;
        $this->iconName = $articleCategoryData->iconName;
    }

    /**
     * @param \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryData $articleCategoryData
     */
    public function edit(ArticleCategoryData $articleCategoryData)
    {
        $this->setTranslations($articleCategoryData);
        $this->placement = $articleCategoryData->placement;
        $this->iconName = $articleCategoryData->iconName;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string|null $locale
     * @return string
     */
    public function getName($locale = null)
    {
        return $this->translation($locale)->getName();
    }

    /**
     * @return string[]
     */
    public function getNames()
    {
        $namesByLocale = [];
        foreach ($this->translations as $translation) {
            $namesByLocale[$translation->getLocale()] = $translation->getName();
        }

        return $namesByLocale;
    }

    /**
     * @return string
     */
    public function getPlacement()
    {
        return $this->placement;
    }

    public function getIconName() {
        return $this->iconName;
    }


    /**
     * @param \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryData $articleCategoryData
     */
    private function setTranslations(ArticleCategoryData $articleCategoryData)
    {
        foreach ($articleCategoryData->name as $locale => $name) {
            $this->translation($locale)->setName($name);
        }
    }

    /**
     * @return \Shopsys\ShopBundle\Model\ArticleCategory\ArticleCategoryTranslation
     */
    protected function createTranslation()
    {
        return new ArticleCategoryTranslation();
    }


    public function setPlacement($placement)
    {
        $this->placement = $placement;
    }

    public function setIconName($iconName) {
        $this->iconName = $iconName;
    }
}
