<?php

namespace Shopsys\ShopBundle\Model\Transport\Detail;

use Shopsys\ShopBundle\Model\Transport\Transport;

class TransportDetail
{
    /**
     * @var \Shopsys\ShopBundle\Model\Transport\Transport
     */
    private $transport;

    /**
     * @var \Shopsys\ShopBundle\Model\Pricing\Price[]
     */
    private $basePricesByCurrencyId;

    /**
     * @param \Shopsys\ShopBundle\Model\Transport\Transport $transport
     * @param \Shopsys\ShopBundle\Model\Pricing\Price[] $basePricesByCurrencyId
     */
    public function __construct(
        Transport $transport,
        array $basePricesByCurrencyId
    ) {
        $this->transport = $transport;
        $this->basePricesByCurrencyId = $basePricesByCurrencyId;
    }

    /**
     * @return \Shopsys\ShopBundle\Model\Transport\Transport
     */
    public function getTransport()
    {
        return $this->transport;
    }

    /**
     * @return \Shopsys\ShopBundle\Model\Pricing\Price[]
     */
    public function getBasePricesByCurrencyId()
    {
        return $this->basePricesByCurrencyId;
    }
}
