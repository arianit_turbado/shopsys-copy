<?php

namespace Shopsys\ShopBundle\Model\Security;

class Roles
{
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_ADMIN_AS_CUSTOMER = 'ROLE_ADMIN_AS_CUSTOMER';
    const ROLE_CUSTOMER = 'ROLE_CUSTOMER';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
}
