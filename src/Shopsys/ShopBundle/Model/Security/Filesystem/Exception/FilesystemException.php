<?php

namespace Shopsys\ShopBundle\Model\Security\Filesystem\Exception;

use Shopsys\ShopBundle\Model\Security\Exception\SecurityException;

interface FilesystemException extends SecurityException
{
}
