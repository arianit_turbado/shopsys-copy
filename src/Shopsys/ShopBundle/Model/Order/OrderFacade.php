<?php

namespace Shopsys\ShopBundle\Model\Order;

use Doctrine\ORM\EntityManager;
use Shopsys\ShopBundle\Component\Domain\Domain;
use Shopsys\ShopBundle\Component\Setting\Setting;
use Shopsys\ShopBundle\Form\Admin\QuickSearch\QuickSearchFormData;
use Shopsys\ShopBundle\Model\Administrator\Security\AdministratorFrontSecurityFacade;
use Shopsys\ShopBundle\Model\Cart\CartFacade;
use Shopsys\ShopBundle\Model\Customer\CurrentCustomer;
use Shopsys\ShopBundle\Model\Customer\CustomerFacade;
use Shopsys\ShopBundle\Model\Customer\User;
use Shopsys\ShopBundle\Model\Heureka\HeurekaFacade;
use Shopsys\ShopBundle\Model\Localization\Localization;
use Shopsys\ShopBundle\Model\Order\Item\OrderProductFacade;
use Shopsys\ShopBundle\Model\Order\Mail\OrderMailFacade;
use Shopsys\ShopBundle\Model\Order\Preview\OrderPreview;
use Shopsys\ShopBundle\Model\Order\Preview\OrderPreviewFactory;
use Shopsys\ShopBundle\Model\Order\PromoCode\CurrentPromoCodeFacade;
use Shopsys\ShopBundle\Model\Order\Status\OrderStatus;
use Shopsys\ShopBundle\Model\Order\Status\OrderStatusRepository;
use Shopsys\ShopBundle\Model\Order\Item\OrderPayment;
use Shopsys\ShopBundle\Model\Order\Item\OrderProduct;
use Shopsys\ShopBundle\Model\Order\Item\OrderTransport;
use Shopsys\ShopBundle\Model\Product\Detail\ProductDetailFactory;
use Shopsys\ShopBundle\Component\Setting\SettingValue;

class OrderFacade
{
    const VARIABLE_NUMBER = '{number}';
    const VARIABLE_ORDER_DETAIL_URL = '{order_detail_url}';
    const VARIABLE_PAYMENT_INSTRUCTIONS = '{payment_instructions}';
    const VARIABLE_TRANSPORT_INSTRUCTIONS = '{transport_instructions}';

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\OrderNumberSequenceRepository
     */
    private $orderNumberSequenceRepository;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\OrderRepository
     */
    private $orderRepository;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\OrderService
     */
    private $orderService;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\OrderCreationService
     */
    private $orderCreationService;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\Status\OrderStatusRepository
     */
    private $orderStatusRepository;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\Mail\OrderMailFacade
     */
    private $orderMailFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\OrderHashGeneratorRepository
     */
    private $orderHashGeneratorRepository;

    /**
     * @var \Shopsys\ShopBundle\Component\Setting\Setting
     */
    private $setting;

    /**
     * @var \Shopsys\ShopBundle\Model\Localization\Localization
     */
    private $localization;

    /**
     * @var \Shopsys\ShopBundle\Model\Administrator\Security\AdministratorFrontSecurityFacade
     */
    private $administratorFrontSecurityFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\PromoCode\CurrentPromoCodeFacade
     */
    private $currentPromoCodeFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Cart\CartFacade
     */
    private $cartFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Customer\CustomerFacade
     */
    private $customerFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Customer\CurrentCustomer
     */
    private $currentCustomer;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\Preview\OrderPreviewFactory
     */
    private $orderPreviewFactory;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\Item\OrderProductFacade
     */
    private $orderProductFacade;

    /**
     * @var \Shopsys\ShopBundle\Model\Heureka\HeurekaFacade
     */
    private $heurekaFacade;

    /**
     * @var \Shopsys\ShopBundle\Component\Domain\Domain
     */
    private $domain;

    public function __construct(
        EntityManager $em,
        OrderNumberSequenceRepository $orderNumberSequenceRepository,
        OrderRepository $orderRepository,
        OrderService $orderService,
        OrderCreationService $orderCreationService,
        OrderStatusRepository $orderStatusRepository,
        OrderMailFacade $orderMailFacade,
        OrderHashGeneratorRepository $orderHashGeneratorRepository,
        Setting $setting,
        Localization $localization,
        AdministratorFrontSecurityFacade $administratorFrontSecurityFacade,
        CurrentPromoCodeFacade $currentPromoCodeFacade,
        CartFacade $cartFacade,
        CustomerFacade $customerFacade,
        CurrentCustomer $currentCustomer,
        OrderPreviewFactory $orderPreviewFactory,
        OrderProductFacade $orderProductFacade,
        HeurekaFacade $heurekaFacade,
        Domain $domain,
        ProductDetailFactory $productDetailFactory
    ) {
        $this->em = $em;
        $this->orderNumberSequenceRepository = $orderNumberSequenceRepository;
        $this->orderRepository = $orderRepository;
        $this->orderService = $orderService;
        $this->orderCreationService = $orderCreationService;
        $this->orderStatusRepository = $orderStatusRepository;
        $this->orderMailFacade = $orderMailFacade;
        $this->orderHashGeneratorRepository = $orderHashGeneratorRepository;
        $this->setting = $setting;
        $this->localization = $localization;
        $this->administratorFrontSecurityFacade = $administratorFrontSecurityFacade;
        $this->currentPromoCodeFacade = $currentPromoCodeFacade;
        $this->cartFacade = $cartFacade;
        $this->customerFacade = $customerFacade;
        $this->currentCustomer = $currentCustomer;
        $this->orderPreviewFactory = $orderPreviewFactory;
        $this->orderProductFacade = $orderProductFacade;
        $this->heurekaFacade = $heurekaFacade;
        $this->domain = $domain;
        $this->productDetailFactory = $productDetailFactory;

    }

    /**
     * @param \Shopsys\ShopBundle\Model\Order\OrderData $orderData
     * @param \Shopsys\ShopBundle\Model\Order\Preview\OrderPreview $orderPreview
     * @param \Shopsys\ShopBundle\Model\Customer\User|null $user
     * @return \Shopsys\ShopBundle\Model\Order\Order
     */
    public function createOrder(OrderData $orderData, OrderPreview $orderPreview, User $user = null)
    {
        $orderNumber = $this->orderNumberSequenceRepository->getNextNumber();
        $orderUrlHash = $this->orderHashGeneratorRepository->getUniqueHash();
        $toFlush = [];

        $this->setOrderDataAdministrator($orderData);

        $order = new Order(
            $orderData,
            $orderNumber,
            $orderUrlHash,
            $user
        );
        $toFlush[] = $order;

        $this->orderCreationService->fillOrderItems($order, $orderPreview);

        foreach ($order->getItems() as $orderItem) {
            $this->em->persist($orderItem);
            $toFlush[] = $orderItem;
        }

        $this->orderService->calculateTotalPrice($order);
        $this->em->persist($order);
        $this->em->flush($toFlush);

        return $order;
    }

    /**
     * @param \Shopsys\ShopBundle\Model\Order\OrderData $orderData
     * @return \Shopsys\ShopBundle\Model\Order\Order
     */
    public function createOrderFromFront(OrderData $orderData)
    {
        $domainConfig = $this->domain->getDomainConfigById($orderData->domainId);
        $locale = $domainConfig->getLocale();

        $orderData->status = $this->orderStatusRepository->getDefault();
        $orderPreview = $this->orderPreviewFactory->createForCurrentUser($orderData->transport, $orderData->payment);
        $user = $this->currentCustomer->findCurrentUser();


        if (isset($_SESSION['order']['pricing_group']))
            $orderData->pricingGroup = $_SESSION['order']['pricing_group'];
        else
            $orderData->pricingGroup = $this->setting->getForDomain(Setting::DEFAULT_PRICING_GROUP, $orderData->domainId);


        $order = $this->createOrder($orderData, $orderPreview, $user);
        $this->orderProductFacade->subtractOrderProductsFromStock($order->getProductItems());

        $this->cartFacade->cleanCart();
        $this->currentPromoCodeFacade->removeEnteredPromoCode();
        if ($user instanceof User) {
            $this->customerFacade->amendCustomerDataFromOrder($user, $order);
        }
        if ($this->heurekaFacade->isHeurekaShopCertificationActivated($orderData->domainId) &&
            $this->heurekaFacade->isDomainLocaleSupported($locale)
        ) {
            $this->heurekaFacade->sendOrderInfo($order);
        }

        return $order;
    }

    /**
     * @param int $orderId
     * @param \Shopsys\ShopBundle\Model\Order\OrderData $orderData
     * @return \Shopsys\ShopBundle\Model\Order\Order
     */
    public function edit($orderId, OrderData $orderData)
    {
        $order = $this->orderRepository->getById($orderId);
        $originalOrderStatus = $order->getStatus();
        $orderEditResult = $this->orderService->editOrder($order, $orderData);

        foreach ($orderEditResult->getOrderItemsToCreate() as $orderItem) {
            $this->em->persist($orderItem);
        }
        foreach ($orderEditResult->getOrderItemsToDelete() as $orderItem) {
            $this->em->remove($orderItem);
        }

        $this->em->flush();
        if ($orderEditResult->isStatusChanged()) {
            $mailTemplate = $this->orderMailFacade
                ->getMailTemplateByStatusAndDomainId($order->getStatus(), $order->getDomainId());
            if ($mailTemplate->isSendMail()) {
                $this->orderMailFacade->sendEmail($order);
            }
            if ($originalOrderStatus->getType() === OrderStatus::TYPE_CANCELED) {
                $this->orderProductFacade->subtractOrderProductsFromStock($order->getProductItems());
            }
            if ($orderData->status->getType() === OrderStatus::TYPE_CANCELED) {
                $this->orderProductFacade->addOrderProductsToStock($order->getProductItems());
            }
        }

        return $order;
    }

    /**
     * @param int $orderId
     * @return string
     */
    public function getOrderSentPageContent($orderId)
    {
        $order = $this->getById($orderId);
        $orderDetailUrl = $this->orderService->getOrderDetailUrl($order);
        $orderSentPageContent = $this->setting->getForDomain(Setting::ORDER_SENT_PAGE_CONTENT, $order->getDomainId());

        $variables = [
            self::VARIABLE_TRANSPORT_INSTRUCTIONS => $order->getTransport()->getInstructions(),
            self::VARIABLE_PAYMENT_INSTRUCTIONS => $order->getPayment()->getInstructions(),
            self::VARIABLE_ORDER_DETAIL_URL => $orderDetailUrl,
            self::VARIABLE_NUMBER => $order->getNumber(),
        ];

        return strtr($orderSentPageContent, $variables);
    }

    /**
     * @param \Shopsys\ShopBundle\Model\Order\FrontOrderData $orderData
     * @param \Shopsys\ShopBundle\Model\Customer\User $user
     */
    public function prefillFrontOrderData(FrontOrderData $orderData, User $user)
    {
        $order = $this->orderRepository->findLastByUserId($user->getId());
        $this->orderCreationService->prefillFrontFormData($orderData, $user, $order);
    }

    /**
     * @param int $orderId
     */
    public function deleteById($orderId)
    {
        $order = $this->orderRepository->getById($orderId);
        if ($order->getStatus()->getType() !== OrderStatus::TYPE_CANCELED) {
            $this->orderProductFacade->addOrderProductsToStock($order->getProductItems());
        }
        $order->markAsDeleted();
        $this->em->flush();
    }

    /**
     * @param \Shopsys\ShopBundle\Model\Customer\User $user
     * @return \Shopsys\ShopBundle\Model\Order\Order[]
     */
    public function getCustomerOrderList(User $user)
    {
        return $this->orderRepository->getCustomerOrderList($user);
    }

    /**
     * @param int $orderId
     * @return \Shopsys\ShopBundle\Model\Order\Order
     */
    public function getById($orderId)
    {
        return $this->orderRepository->getById($orderId);
    }

    /**
     * @param string $urlHash
     * @param int $domainId
     * @return \Shopsys\ShopBundle\Model\Order\Order
     */
    public function getByUrlHashAndDomain($urlHash, $domainId)
    {
        return $this->orderRepository->getByUrlHashAndDomain($urlHash, $domainId);
    }

    /**
     * @param string $orderNumber
     * @param \Shopsys\ShopBundle\Model\Customer\User $user
     * @return \Shopsys\ShopBundle\Model\Order\Order
     */
    public function getByOrderNumberAndUser($orderNumber, User $user)
    {
        return $this->orderRepository->getByOrderNumberAndUser($orderNumber, $user);
    }

    /**
     * @param string $orderNumber
     * @return \Shopsys\ShopBundle\Model\Order\Order
     */
    public function getByOrderNumber($orderNumber)
    {
        return $this->orderRepository->getByOrderNumber($orderNumber);
    }

    /**
     * @param \Shopsys\ShopBundle\Form\Admin\QuickSearch\QuickSearchFormData $quickSearchData
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function getOrderListQueryBuilderByQuickSearchData(QuickSearchFormData $quickSearchData)
    {
        return $this->orderRepository->getOrderListQueryBuilderByQuickSearchData(
            $this->localization->getAdminLocale(),
            $quickSearchData
        );
    }

    /**
     * @param \Shopsys\ShopBundle\Model\Order\OrderData $orderData
     */
    private function setOrderDataAdministrator(OrderData $orderData)
    {
        if ($this->administratorFrontSecurityFacade->isAdministratorLoggedAsCustomer()) {
            try {
                $currentAdmin = $this->administratorFrontSecurityFacade->getCurrentAdministrator();
                $orderData->createdAsAdministrator = $currentAdmin;
                $orderData->createdAsAdministratorName = $currentAdmin->getRealName();
            } catch (\Shopsys\ShopBundle\Model\Administrator\Security\Exception\AdministratorIsNotLoggedException $ex) {
            }
        }
    }
    /**
     * @param int $orderId
     * @param int $domainId
     */
    public function getOrderType($orderId,$domainId) {
        $order = $this->getById($orderId);

        $defaultPricingGroupIds = $this->em->getRepository(SettingValue::class)->findBy(['domainId' => $domainId, 'name' => 'defaultPricingGroupId']);
        $defaultPricingGroupIds[0]->getValue();

        if($order->getPricingGroup() == $defaultPricingGroupIds[0]->getValue() || !$order->getPricingGroup()) { // B2C
            if($order->getBenefits()){
                return 1; // B2C rent
            } else {
                return 2; // B2C sale
            }
        } else { // B2B or other
            return 3;
        }
    }

    public function getStatusHistoriesForOrder($order) {
        return $this->em->getRepository(StatusHistory::class)->findBy(['order' => $order], ['id' => 'asc']);

    }

    public function exportToJson($orderId) {
        $order = $this->getById($orderId);


        $shippingTotal = 0;
        $paymentTotal = 0;

        $response = [
            $orderId => [
                'info' => [
                    'order_number' => $order->getNumber(),
                    'order_url' => $this->orderService->getOrderDetailUrl($order),
                    'currency' => $order->getCurrency()->getCode(),
                    'currency_value' => $order->getCurrency()->getExchangeRate(),
                    'payment_method' => $order->getPaymentName(),
                    'payment_module_code' => $order->getPaymentName(),
                    'payment_id' => $order->getPayment()->getId(),
                    'shipping_method' => $order->getTransportName(),
                    'shipping_module_code' => $order->getTransportName(),
                    'shipping_id' => $order->getTransport()->getId(),
                    'date_purchased' => $order->getCreatedAt()->format('Y-m-d H:i:s'),
                    'orders_status' => $order->getStatus()->getName(),
                    'orders_status_id' => $order->getStatus()->getId(),
                    'last_modified' => $order->getUpdatedAt()->format('Y-m-d H:i:s'),
                    'total' => ($order->getTotalPriceWithVat() + 0),
                    'tax' => $order->getTotalVatAmount(),
                    'ip_address' => '',
                    'orders_id' => $orderId,
                    'ip_address' => '',
                    'shops_url' => 'http://159.100.242.231',
                    'is_cod' => false,
                    'cod_value' => null,
                    'cod_currency' => null,
                    'total_weight' => 0,
                    'delivery_price' => 'FREE',
                    'delivery_time' => '0',
                    'delivery_cost'=> '1 np',
                    'warranty' => '24 months',
                    'note' => $order->getNote(),
                    'pricingGroup' => $order->getPricingGroup()
                ],
                'products' => [],
                'status_history' => []

            ]
        ];

        $statusHistories = $this->getStatusHistoriesForOrder($order);
        foreach ($statusHistories as $statusHistory) {
            $response[$orderId]['status_history'][] = [
                'orders_status_history_id' => $statusHistory->getId(),
                'orders_id' => $orderId,
                'orders_status_id' => $statusHistory->getOrderStatus()->getId(),
                'orders_status_name' => $statusHistory->getOrderStatus()->getName(),
                'date_added' => $statusHistory->getDate()->format('Y-m-d H:i:s'),
                'customer_notified' => $statusHistory->getCustomerNotified(),
                'email_template' => $statusHistory->getEmailTemplate(),
                'comments' => '',
                'orders_status_text' => null,
                'language_id' => '',
            ];
        }


        //set cod values

        if($order->getPayment()->getId() == 2) {
            $response[$orderId]['info']['is_cod'] = true;
            $price = $order->getPayment()->getPrice($order->getCurrency())->getPrice() + 0;
//            $response[$orderId]['info']['cod_value'] = $price + ($order->getPayment()->getVat()->getPercent() * $price / 100);
            $response[$orderId]['info']['cod_value'] = $price;
            $response[$orderId]['info']['cod_currency'] = $order->getCurrency()->getCode();

        }



        $products = [];
        $discounts = [];
        $totalDiscounts = 0;
        foreach($order->getItems() as $orderItem) {
            if ($orderItem instanceof OrderProduct) {
                if(!$orderItem->hasProduct()) {
                    if((int)$orderItem->getPriceWithVat() < 0) {
                        $discounts[] = [
                            'title' => $orderItem->getName(),
                            "text" => $orderItem->getPriceWithVat() . $order->getCurrency()->getCode(),
                            'value' => $orderItem->getPriceWithVat(),
                            'class' => 'ot_discount'
                        ];
                        $totalDiscounts += $orderItem->getPriceWithVat();
                    }
                } else {
                    $weight = 0;

                    foreach ($this->productDetailFactory->getParameters($orderItem->getProduct()) as $parameter) {
                        if(strtolower($parameter->getParameter()->getName()) == 'weight' || strtolower($parameter->getParameter()->getName()) == 'typ soustavy') {
                            $weight = $parameter->getValue()->getText();
                        }
                    };


                    $pricesForPricingGroups = [];
                    $pricingGroupInfo = json_decode($orderItem->getItemHelper());

                    if($pricingGroupInfo) {
                        foreach($pricingGroupInfo->pricingGroups as $id => $pricingGroup) {
                            $pricesForPricingGroups[] = ['id' => $id, 'name' => $pricingGroup->name, 'coefficient' => $pricingGroup->coefficient, 'price' => $pricingGroupInfo->prices->priceWithVat * $pricingGroup->coefficient];
                        }

                    }

                    $products[] = [
                        'qty' => $orderItem->getQuantity(),
                        'id' => $orderItem->getProduct()->getId(),
                        'master_product_id' => $orderItem->getProduct()->getMasterProductId(),
                        'name' => $orderItem->getName(),
                        'model' => $orderItem->getName(),
                        'tax' => ($orderItem->getPriceWithVat() - $orderItem->getPriceWithoutVat()) + 0,
                        'price' => $orderItem->getPriceWithoutVat() + 0,
                        'final_price' => $orderItem->getPriceWithVat() + 0,
                        'products_pid' => $orderItem->getProduct()->getProductPid(),
                        'products_source' => $orderItem->getProduct()->getProductSource(),
                        'products_buy_price' => $orderItem->getProduct()->getProductBuyPrice() + 0,
                        'tracking_number' => '',
                        'weight' => $weight,
                        'helper' => $orderItem->getItemHelper(),
                        'pricesForPricingGroups' => $pricesForPricingGroups

                    ];
                }


            } else if($orderItem instanceof OrderTransport) {
                $shippingTotal += $orderItem->getPriceWithVat() + 0;
            } else if($orderItem instanceof OrderPayment) {
                $paymentTotal += $orderItem->getPriceWithVat() + 0;
            }
        }


        $response[$orderId]['products'] = $products;

        $response[$orderId]['customer'] = [
            'id' => $order->getCustomer() != null ? $order->getCustomer()->getId() : '',
            'name' => $order->getFirstName() . ' ' . $order->getLastName(),
            'company' => $order->getCompanyName(),
            'street_address' =>   $order->getCustomer() != null ? $order->getCustomer()->getBillingAddress()->getStreet() : '',
            'city' => $order->getCustomer() != null ? $order->getCustomer()->getBillingAddress()->getCity() : '',
            'postcode'=> $order->getCustomer() != null ? $order->getCustomer()->getBillingAddress()->getPostcode() : '',
            'country'=> $order->getCustomer() != null ? $order->getCustomer()->getBillingAddress()->getCountry() : '',
            'telephone' =>  $order->getTelephone(),
            'email_address' => $order->getEmail(),
            'state' => '',
            'suburb' => ''

        ];

        $response[$orderId]['delivery'] = [
            'name' => $order->getDeliveryFirstName() . ' ' . $order->getDeliveryFirstName(),
            'company' => $order->getDeliveryCompanyName(),
            'street_address' =>   $order->getDeliveryStreet(),
            'city' => $order->getDeliveryCity(),
            'postcode'=> $order->getDeliveryPostCode(),
            'country'=> $order->getDeliveryCountry()->getName(),
            'state' => '',
            'suburb' => ''
        ];

        if(count($discounts) > 0) {
            $response[$orderId]['discounts'] = $discounts;
        }

        $response[$orderId]['totals'] = [
            [
                "title" => "Shipping",
                "text" => $shippingTotal . $order->getCurrency()->getCode(),
                "value" => $shippingTotal,
                "class" => "ot_shipping" ],
            [
                "title" => "Payment",
                "text" => $paymentTotal . $order->getCurrency()->getCode(),
                "value" => $paymentTotal,
                "class" => "ot_payment" ],
            [
                "title" => "Total",
                "text" => ($order->getTotalPriceWithVat() + 0) . $order->getCurrency()->getCode(),
                "value" => $order->getTotalPriceWithVat() + 0,
                "class" => "ot_total" ],
            [
                "title" => "Total Discounts",
                "text" => $totalDiscounts . $order->getCurrency()->getCode(),
                "value" => $totalDiscounts,
                "class" => "ot_discounts_total" ],

        ];



        // var_dump($discounts);
        // die();


        $response[$orderId]['billing'] = [
            'name' => $order->getFirstName() . ' ' . $order->getLastName(),
            'company' => $order->getCompanyName(),
            'street_address' =>   $order->getStreet(),
            'city' => $order->getCity(),
            'postcode'=> $order->getPostcode(),
            'country'=> $order->getCountry()->getName(),
            'state' => '',
            'suburb' => ''
        ];

        $totalWeight = 0;

        foreach ($response[$orderId]['products'] as $product) {
            if($product['weight'] != 0 && $product['weight'] != '') {
                $weightArray = explode(' ', $product['weight']);
                $weightValue = $weightArray[0];
                if(count($weightArray) > 1 && strtolower($weightArray[1]) == 'g') {
                    $weightValue = $weightValue / 1000;
                }
                $totalWeight += $weightValue;
            }
        }

        $hash = md5(json_encode($response));

        $response[$orderId]['info']['total_weight'] = "$totalWeight KG";

        $response[$orderId]['info']['orders_hash'] = $hash;

        return $response;
    }
}
