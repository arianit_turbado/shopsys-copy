<?php

namespace Shopsys\ShopBundle\Model\Order\PromoCode\Exception;

use Shopsys\ShopBundle\Model\Order\Exception\OrderException;

interface PromoCodeException extends OrderException
{
}
