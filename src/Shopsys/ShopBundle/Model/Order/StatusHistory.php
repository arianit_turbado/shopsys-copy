<?php

namespace Shopsys\ShopBundle\Model\Order;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Prezent\Doctrine\Translatable\Annotation as Prezent;

/**
 * @ORM\Table(name="status_histories")
 * @ORM\Entity
 */
class StatusHistory
{

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var \Shopsys\ShopBundle\Model\Order\Order
     *
     * @ORM\ManyToOne(targetEntity="Shopsys\ShopBundle\Model\Order\Order")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private $order;



    /**
     * @var \Shopsys\ShopBundle\Model\Order\Status\OrderStatus
     *
     * @ORM\ManyToOne(targetEntity="Shopsys\ShopBundle\Model\Order\Status\OrderStatus")
     * @ORM\JoinColumn(name="order_status_id", referencedColumnName="id", nullable=false)
     */
    private $orderStatus;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $date;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default": 0})
     */
    private $customerNotified;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $emailTemplate;


    public function __construct()
    {
        $this->customerNotified = false;
        $this->emailTemplate = '';
    }

    public function getId()
    {
        return $this->id;
    }


    public function getOrder()
    {
        return $this->order;
    }

    public function setOrder($order)
    {
        $this->order = $order;
    }

    public function getOrderStatus()
    {
        return $this->orderStatus;
    }

    public function getCustomerNotified()
    {
        return $this->customerNotified;
    }

    public function getEmailTemplate()
    {
        return $this->emailTemplate;
    }

    public function setOrderStatus($orderStatus)
    {
        $this->orderStatus = $orderStatus;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function setCustomerNotified($customer_notified)
    {
        $this->customerNotified = $customer_notified;
    }

    public  function setEmailTemplate($email_template)
    {
        $this->emailTemplate = $email_template;
    }

}
