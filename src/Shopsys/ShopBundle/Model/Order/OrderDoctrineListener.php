<?php

namespace Shopsys\ShopBundle\Model\Order;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use GuzzleHttp\Client;
use PDepend\Util\Log;
use Shopsys\ShopBundle\Model\Order\StatusHistory;
use Shopsys\ShopBundle\Model\Order\OrderFacade;


class OrderDoctrineListener
{
    /**
     * @param \Doctrine\ORM\Event\LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof Order) {

            $changesArray = $args->getEntityManager()->getUnitOfWork()->getEntityChangeSet($args->getObject());

            if (array_key_exists('status', $changesArray)) {
                $em = $args->getEntityManager();
                $statusHistory = new StatusHistory();
                $statusHistory->setOrder($entity);
                $statusHistory->setDate(new \DateTime());
                $statusHistory->setOrderStatus($entity->getStatus());
                try {
                    $shopIds = [1 => '20', 2 => '9', 3 => '10', 4 => '7', 5 => '8'];
                    $domainId = $entity->getDomainId();
                    if (array_key_exists($domainId, $shopIds)) {
                        $client = new Client();
                        $response = $client->request('POST', 'http://orders.turbado.eu/api/statusEmailTemplate', [
                            'form_params' => [
                                'shop_id' => $shopIds[$domainId],
                                'order_id' => $entity->getId(),
                                'status_id' => $statusHistory->getOrderStatus()->getId()
                            ]
                        ]);
                        $result = json_decode($response->getBody());
                        if ($result->status == 200) {
                            $statusHistory->setCustomerNotified($result->message->customer_notified);
                            $statusHistory->setEmailTemplate($result->message->email_template);
                        }
                    }
                } catch (\Exception $ex) {
                    $statusHistory->setEmailTemplate($ex->getMessage());
                }
                $em->persist($statusHistory);
                $em->flush();
            }

            try {
                $client = new \GuzzleHttp\Client();

                $shopIds = [1 => '20', 2 => '9', 3 => '10', 4 => '7', 5 => '8'];
                $domainId = $entity->getDomainId();
                if (array_key_exists($domainId, $shopIds) && count($entity->getItemsWithoutTransportAndPaymentWithPositivePrice()) > 0) {

                    $response = $client->request('POST', 'http://orders.turbado.eu/api/updateOrder', [
                        'form_params' => [
                            'shop_id' => $shopIds[$domainId],
                            'order_id' => $entity->getId(),
                        ]
                    ]);

                }
            } catch (\Exception $e) {

            }
        }
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        if ($entity instanceof Order) {
            $em = $args->getEntityManager();
            $statusHistory = new StatusHistory();
            $statusHistory->setOrder($entity);
            $statusHistory->setDate(new \DateTime());
            $statusHistory->setOrderStatus($entity->getStatus());
            $em->persist($statusHistory);
            $em->flush();

            try {
                $client = new \GuzzleHttp\Client();

                $shopIds = [1 => '20', 2 => '9', 3 => '10', 4 => '7', 5 => '8'];
                $domainId = $entity->getDomainId();

                if (array_key_exists($domainId, $shopIds)) {

                    $response = $client->request('POST', 'http://orders.turbado.eu/api/registerOrder', [
                        'form_params' => [
                            'shop_id' => $shopIds[$domainId],
                            'order_id' => $entity->getId(),
                        ]
                    ]);
                }

            } catch (\Exception $e) {

            }


            return;
        }

    }
}
