<?php

namespace Shopsys\ShopBundle\Model\Order;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use GuzzleHttp\Client;
use Shopsys\ShopBundle\Model\Mail\MailerService;
use Shopsys\ShopBundle\Model\Order\StatusHistory;
use Shopsys\ShopBundle\Model\Mail\MessageData;
use Swift_Mailer;
use Swift_SmtpTransport;
use Swift_Transport_EsmtpTransport;
use PHPMailer\PHPMailer\PHPMailer;

class StatusHistoryDoctrineListener
{

    private $mailerService;


    public function _construct(MailerService $mailerService)
    {
        $this->mailerService = $mailerService;
    }

    /**
     * @param \Doctrine\ORM\Event\LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();

        try {
            if ($entity instanceof StatusHistory) {
                if ($entity->getCustomerNotified()) {
                    $email = $entity->getOrder()->getEmail();
                    $content = $this->replaceBodyByTemplate($entity->getEmailTemplate());
                    $this->sendMessage($email, $content);
                }
            }
        } catch (\Exception $ex) {

        }
        return;
    }

    /**
     * @return PHPMailer
     */
    private function getMailer() {
        $config = $this->getConfig();
        $mailer = new PHPMailer(true);
        // SMTP config
        $mailer->isSMTP();
        $mailer->Host = 'smtp.sendgrid.net';//'smtp.gmail.com';
        $mailer->SMTPAuth = true;
        $mailer->CharSet = "UTF-8";
        $mailer->Username = $config['username'];
        $mailer->Password = $config['password'];
        $mailer->Port = 25; //465;
        // $mailer->SMTPSecure = 'ssl';
        $mailer->SMTPOptions = [
            'ssl' => [
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            ]
        ];

        return $mailer;
    }

    private function sendMessage($to, $content) {
        $mailer = $this->getMailer();
        $config = $this->getConfig();

        $mailer->setFrom($config['email'], $config['name']);
        $mailer->addReplyTo($config['email'], $config['name']);

        $mailer->addAddress($to);

        $mailer->CharSet = "UTF-8";
        $mailer->isHTML(true);
        $mailer->Subject = $config['subject'];
        $mailer->Body = $content;
        $mailer->AltBody = strip_tags($content);

        $mailer->send();
    }

    private function getDomainsCofing() {
        return [
            '[172.22.0.6]' => ['username' => 'phptest130795@gmail.com' , 'password' => 'vigan_abd123', 'email' => 'phptest130795@gmail.com',  'name' => 'Eleka.de', 'subject' => 'Status Changed'],
            'eleka.de' => ['username' => 'Eleka' , 'password' => 'Gz5e%47A!azeR=hUr6', 'email' => 'hallo@eleka.de', 'name' => 'eleka.de', 'subject' => 'Status Changed'],
            'scpoznan.pl' => ['username' => 'SCpoznanPL1' , 'password' => 'bestcena1902', 'email' => 'szia@scpoznan.pl', 'name' => 'scpoznan.pl', 'subject' => 'Status Changed'],
            'scbudapest.hu' => ['username' => 'scbudapest' , 'password' => 'szia123pass', 'email' => 'szia@schu.hu', 'name' => 'scbudapest.hu', 'subject' => 'Status Changed'],
            'exafoto.sk' => ['username' => 'ockosicesk' , 'password' => '8686ac6f98b', 'email' => 'ahoj@ocsk.sk', 'name' => 'exafoto.sk', 'subject' => 'Status Changed'],
            'ocz.cz' => ['username' => 'ocostravacz' , 'password' => 'Creativ2', 'email' => 'ahoj@occz.cz', 'name' => 'exafoto.cz', 'subject' => 'Status Changed'],
        ];
    }

    private function domiansLayoustsPaths() {
        return [
            '[172.22.0.6]' => 'Mail/Layouts/eleka.html.twig',
            'eleka.de' => 'Mail/Layouts/eleka.html.twig',
            'scpoznan.pl' => 'Mail/Layouts/scpoznan.html.twig',
            'scbudapest.hu' => 'Mail/Layouts/scbudapest.html.twig',
            'exafoto.sk' => 'Mail/Layouts/exafoto.html.twig',
            'ocz.cz' => 'Mail/Layouts/ocz.html.twig',
        ];
    }

    private function replaceBodyByTemplate($body) {

        if (array_key_exists($this->getLocalDomain(), $this->domiansLayoustsPaths())) {
            $env = new \Twig_Environment(new \Twig_Loader_Filesystem(MailerService::$layoutsPath));
            $body = $env->render($this->domiansLayoustsPaths()[$this->getLocalDomain()], array('content' => $body));
        }
        return $body;
    }

    private $_localDomain;

    private function getLocalDomain() {
        if(empty($this->_localDomain)) {
            $this->_localDomain = (new Swift_SmtpTransport('smtp.sendgrid.net', 25))->getLocalDomain();
        }
        return $this->_localDomain;
    }

    private function getConfig() {
        $config = $this->getDomainsCofing();
        if(array_key_exists($this->getLocalDomain(), $this->domiansLayoustsPaths())) {
            $config = $config[$this->getLocalDomain()];
        } else {
            $config = $config['eleka.de'];
        }
        return $config;
    }
}