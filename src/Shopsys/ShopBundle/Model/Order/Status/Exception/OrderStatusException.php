<?php

namespace Shopsys\ShopBundle\Model\Order\Status\Exception;

use Shopsys\ShopBundle\Model\Order\Exception\OrderException;

interface OrderStatusException extends OrderException
{
}
