<?php

namespace Shopsys\ShopBundle\Model\Order\Item;

use Doctrine\ORM\EntityManager;
use Shopsys\ShopBundle\Model\Order\OrderRepository;
use Shopsys\ShopBundle\Model\Order\OrderService;
use Shopsys\ShopBundle\Model\Product\Pricing\ProductPriceCalculationForUser;
use Shopsys\ShopBundle\Model\Product\ProductRepository;
use Shopsys\ShopBundle\Model\Product\Pricing\ProductPrice;
use Shopsys\ShopBundle\Model\Pricing\Price;

class OrderItemFacade
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\OrderRepository
     */
    private $orderRepository;

    /**
     * @var \Shopsys\ShopBundle\Model\Product\ProductRepository
     */
    private $productRepository;

    /**
     * @var \Shopsys\ShopBundle\Model\Product\Pricing\ProductPriceCalculationForUser
     */
    private $productPriceCalculationForUser;

    /**
     * @var \Shopsys\ShopBundle\Model\Order\OrderService
     */
    private $orderService;

    public function __construct(
        EntityManager $em,
        OrderRepository $orderRepository,
        ProductRepository $productRepository,
        ProductPriceCalculationForUser $productPriceCalculationForUser,
        OrderService $orderService
    ) {
        $this->em = $em;
        $this->orderRepository = $orderRepository;
        $this->productRepository = $productRepository;
        $this->productPriceCalculationForUser = $productPriceCalculationForUser;
        $this->orderService = $orderService;
    }

    /**
     * @param int $orderId
     * @param int $productId
     * @return \Shopsys\ShopBundle\Model\Order\Item\OrderProduct
     */
    public function createOrderProductInOrder($orderId, $productId, $fixedPrice = null, $itemHelper = '')
    {
        $order = $this->orderRepository->getById($orderId);
        $product = $this->productRepository->getById($productId);

        if($fixedPrice == null) {
            $productPrice = $this->productPriceCalculationForUser->calculatePriceForUserAndDomainId(
                $product,
                $order->getDomainId(),
                $order->getCustomer()
            );
        } else {
            $fixedPrice = new Price($fixedPrice, $fixedPrice);

            $productPrice = new ProductPrice($fixedPrice, false);

        }


        $orderProduct = $this->orderService->createOrderProductInOrder($order, $product, $productPrice, $itemHelper);

        $this->em->persist($orderProduct);
        $this->em->flush([
            $order,
            $orderProduct,
        ]);

        return $orderProduct;
    }

    public function createOrderProductInOrderWithQuantity($orderId, $productId, $quantity, $fixedPrice = null, $itemHelper = '')
    {
        $order = $this->orderRepository->getById($orderId);
        $product = $this->productRepository->getById($productId);

        if($fixedPrice == null) {
            $productPrice = $this->productPriceCalculationForUser->calculatePriceForUserAndDomainId(
                $product,
                $order->getDomainId(),
                $order->getCustomer()
            );
        } else {
            $fixedPrice = new Price($fixedPrice, $fixedPrice);

            $productPrice = new ProductPrice($fixedPrice, false);

        }

        $orderProduct = $this->orderService->createOrderProductInOrderWithQuantity($order, $product, $productPrice, $quantity, $itemHelper);

        $this->em->persist($orderProduct);
        $this->em->flush([
            $order,
            $orderProduct,
        ]);

        return $orderProduct;
    }
}
