<?php

namespace Shopsys\ShopBundle\Model\Order\Item\Exception;

use Shopsys\ShopBundle\Model\Order\Exception\OrderException;

interface OrderItemException extends OrderException
{
}
