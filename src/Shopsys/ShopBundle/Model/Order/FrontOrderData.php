<?php

namespace Shopsys\ShopBundle\Model\Order;

class FrontOrderData extends OrderData
{
    /**
     * @var bool
     */
    public $companyCustomer;

    /**
     * @var bool
     */
    public $newsletterSubscription;
}
