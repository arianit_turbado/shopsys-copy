<?php

namespace Shopsys\ShopBundle\Model\Pricing\Vat\Exception;

use Shopsys\ShopBundle\Model\Pricing\Exception\PricingException;

interface VatException extends PricingException
{
}
