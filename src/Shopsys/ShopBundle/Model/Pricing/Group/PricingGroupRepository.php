<?php

namespace Shopsys\ShopBundle\Model\Pricing\Group;

use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Shopsys\ShopBundle\Component\Setting\SettingValue;
use Shopsys\ShopBundle\Model\Customer\User;

// added
use Doctrine\ORM\Query\ResultSetMapping;

class PricingGroupRepository
{
    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $em;

    /**
     * @param \Doctrine\ORM\EntityManager $em
     */

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    /**
     * @return \Doctrine\ORM\EntityRepository
     */
    private function getPricingGroupRepository()
    {
        return $this->em->getRepository(PricingGroup::class);
    }

    /**
     * @param int $pricingGroupId
     * @return \Shopsys\ShopBundle\Model\Pricing\Group\PricingGroup
     */
    public function getById($pricingGroupId)
    {
        $pricingGroup = $this->getPricingGroupRepository()->find($pricingGroupId);
        if ($pricingGroup === null) {
            $message = 'Pricing group with ID ' . $pricingGroupId . ' not found.';
            throw new \Shopsys\ShopBundle\Model\Pricing\Group\Exception\PricingGroupNotFoundException($message);
        }
        return $pricingGroup;
    }

    /**
     * @return \Shopsys\ShopBundle\Model\Pricing\Group\PricingGroup[]
     */
    public function getAll()
    {
        return $this->getPricingGroupRepository()->findAll();
    }


    public function getAllB2B($domainId){

        $pricingGroupArray = Array();
        foreach ($this->getPricingGroupRepository()->findBy(['domainId' => $domainId]) as $key => $value){

            if( strpos(strtolower($value->getName()),'b2b') !== false) {
                $pricingGroupArray[] = Array($value->getName() => $value->getId());
            }
        }

//        $queryBuilder = $this->em->createQueryBuilder()
//            ->select('sv.value')
//            ->from('setting', 'sv')
//            ->where("sv.name = 'defaultPricingGroupId'")
//            ->andWhere("sv.domain_id = ".$this->domain->getId());
//
//        $result = $queryBuilder->getQuery()->getResult();
//        echo '<pre>',print_r($result,2);
        //$tmp = $this->getPricingGroupRepository()->getDomainId(); // em->getRepository(PricingGroupSettingFacade::class);

        $rsm = new ResultSetMapping();
        $rsm->addScalarResult('value', 'value');
        $result = $this->em->createNativeQuery("SELECT value FROM setting_values WHERE name = 'defaultPricingGroupId' AND domain_id = ".$domainId, $rsm)->execute();

        // default regular pricing group

        $pricingGroupArray[] = Array($this->getById($result[0]['value'])->getName() =>  $result[0]['value']);

        return $pricingGroupArray;
    }

    public function getB2bPricesDiff($domainId) {

        $pricingGroupDiff = Array();
        foreach ($this->getPricingGroupRepository()->findAll() as $key => $value){
            if($value->getDomainId() == $domainId) {
                $pricingGroupDiff[$value->getId()] = round($_SESSION['order']['base_price']*$value->getCoefficient(),0);
            }
        }

        return $pricingGroupDiff;
    }

    /**
    * @return bool
    */
    public function checkIfB2b(){

        if(isset($_SESSION['order']['company_order']) && $_SESSION['order']['company_order'] == 'true'){
            return 'true';
        } else{
            return 'false';
        }
    }

    /**
     * @param int $domainId
     * @return \Shopsys\ShopBundle\Model\Pricing\Group\PricingGroup[]
     */
    public function getPricingGroupsByDomainId($domainId)
    {
        return $this->getPricingGroupRepository()->findBy(['domainId' => $domainId]);
    }

    /**
     * @param int $pricingGroupId
     * @return \Shopsys\ShopBundle\Model\Pricing\Group\PricingGroup|null
     */
    public function findById($pricingGroupId)
    {
        return $this->getPricingGroupRepository()->find($pricingGroupId);
    }

    /**
     * @param int $pricingGroupId
     * @return \Shopsys\ShopBundle\Model\Pricing\Group\PricingGroup|null
     */
    public function getDefault($domainId)
    {
        $defaultPricingGroupIds = $this->em->getRepository(SettingValue::class)->findBy(['domainId' => $domainId, 'name' => 'defaultPricingGroupId']);
        if(count($defaultPricingGroupIds) > 0) {
            $defaultPricingGroup = $defaultPricingGroupIds[0]->getValue();
        }
        return $this->getPricingGroupRepository()->find($defaultPricingGroup);
    }

    /**
     * @param int $pricingGroupId
     * @param int $domainId
     * @return \Shopsys\ShopBundle\Model\Pricing\Group\PricingGroup[]
     */
    public function getAllExceptIdByDomainId($pricingGroupId, $domainId)
    {
        $qb = $this->getPricingGroupRepository()->createQueryBuilder('pg')
            ->where('pg.domainId = :domainId')
            ->andWhere('pg.id != :id')
            ->setParameters(['domainId' => $domainId, 'id' => $pricingGroupId]);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param \Shopsys\ShopBundle\Model\Pricing\Group\PricingGroup $pricingGroup
     * @return bool
     */
    public function existsUserWithPricingGroup(PricingGroup $pricingGroup)
    {
        $query = $this->em->createQuery('
            SELECT COUNT(u)
            FROM ' . User::class . ' u
            WHERE u.pricingGroup = :pricingGroup')
            ->setParameter('pricingGroup', $pricingGroup);
        return $query->getOneOrNullResult(AbstractQuery::HYDRATE_SINGLE_SCALAR) > 0;
    }
}
