<?php

namespace Shopsys\ShopBundle\Model\Product\Availability\Exception;

use Shopsys\ShopBundle\Model\Product\Exception\ProductException;

interface AvailabilityException extends ProductException
{
}
