<?php

namespace Shopsys\ShopBundle\Model\Product;

use Doctrine\ORM\Mapping as ORM;
use Shopsys\ShopBundle\Component\Domain\Domain;

/**
 * @ORM\Table(name="product_domains")
 * @ORM\Entity
 */
class ProductDomain
{
    /**
     * @var \Shopsys\ShopBundle\Model\Product\Product
     *
     * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Shopsys\ShopBundle\Model\Product\Product")
     * @ORM\JoinColumn(nullable=false, name="product_id", referencedColumnName="id", onDelete="CASCADE")
     */
    private $product;

    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     */
    private $domainId;
    
    
    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $seoTitle;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $seoMetaDescription;

    /**
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     */
    private $shortDescription;

    /**
     * @var string
     *
     * @ORM\Column(type="tsvector", nullable=false)
     */
    private $descriptionTsvector;

    /**
     * @var string
     *
     * @ORM\Column(type="tsvector", nullable=false)
     */
    private $fulltextTsvector;

    /**
     * @var string|null
     *
     * @ORM\Column(type="text", nullable=true)
     */
    private $seoH1;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default" = false}, nullable=true)
     */
    private $productFeature;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default" = false}, nullable=true)
     */
    private $productSpecial;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", options={"default" = false}, nullable=true)
     */
    private $productOnSale;

    /**
     * @param \Shopsys\ShopBundle\Model\Product\Product $product
     * @param int $domainId
     */
    public function __construct(Product $product, $domainId)
    {
        $this->product = $product;
        $this->domainId = $domainId;
    }

    /**
     * @return int
     */
    public function getDomainId()
    {
        return $this->domainId;
    }
    
    
    /**
     * @return int
     */
    public function getProductId()
    {
        return $this->getProduct()->getId();
    }

    /**
     * @return string|null
     */
    public function getSeoTitle()
    {
        return $this->seoTitle;
    }

    /**
     * @return string|null
     */
    public function getSeoMetaDescription()
    {
        return $this->seoMetaDescription;
    }

    /**
     * @return string|null
     */
    public function getSeoH1()
    {
        return $this->seoH1;
    }

    public function getProductFeature() {
        return $this->productFeature;
    }

    public function setProductFeature($productFeature)
    {
        $this->productFeature = $productFeature;
    }

    public function getProductSpecial() {
        return $this->productSpecial;
    }

    public function setProductSpecial($productSpecial)
    {
        $this->productSpecial = $productSpecial;
    }

    public function getProductOnSale() {
        return $this->productOnSale;
    }

    public function setProductOnSale($productOnSale)
    {
        $this->productOnSale = $productOnSale;
    }

    /**
     * @param string|null $seoTitle
     */
    public function setSeoTitle($seoTitle)
    {
        $this->seoTitle = $seoTitle;
    }

    /**
     * @param string|null $seoMetaDescription
     */
    public function setSeoMetaDescription($seoMetaDescription)
    {
        $this->seoMetaDescription = $seoMetaDescription;
    }

    /**
     * @param string $seoH1
     */
    public function setSeoH1($seoH1)
    {
        $this->seoH1 = $seoH1;
    }


    /**
     * @param \Shopsys\ShopBundle\Component\Domain\Domain $domain
     * @return string
     */
    public function getSeoTitleForHtml(Domain $domain)
    {
        $seoTitle = $this->getSeoTitle();
        if ($seoTitle === null) {
            return $this->product->getName($domain->getLocale());
        } else {
            return $seoTitle;
        }
    }

    /**
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string|null
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * @param string|null $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @param string|null $shortDescription
     */
    public function setShortDescription($shortDescription)
    {
        $this->shortDescription = $shortDescription;
    }

    /**
     * @return \Shopsys\ShopBundle\Model\Product\Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
