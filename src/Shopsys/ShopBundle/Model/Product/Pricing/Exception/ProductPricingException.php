<?php

namespace Shopsys\ShopBundle\Model\Product\Pricing\Exception;

use Shopsys\ShopBundle\Model\Product\Exception\ProductException;

interface ProductPricingException extends ProductException
{
}
