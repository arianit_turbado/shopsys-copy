<?php

namespace Shopsys\ShopBundle\Model\Product\Exception;

interface VariantException extends ProductException
{
}
