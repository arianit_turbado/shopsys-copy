<?php

namespace Shopsys\ShopBundle\Model\Product\Flag\Exception;

use Shopsys\ShopBundle\Model\Product\Exception\ProductException;

interface FlagException extends ProductException
{
}
