<?php

namespace Shopsys\ShopBundle\Model\Product\Parameter\Exception;

use Shopsys\ShopBundle\Model\Product\Exception\ProductException;

interface ParameterException extends ProductException
{
}
