<?php

namespace Shopsys\ShopBundle\Model\Product\Parameter;

class ProductParameterValuesLocalizedData
{
    /**
     * @var \Shopsys\ShopBundle\Model\Product\Parameter\Parameter
     */
    public $parameter;

    /**
     * @var string[]
     */
    public $valueTextsByLocale;
}
