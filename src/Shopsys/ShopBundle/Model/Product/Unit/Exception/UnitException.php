<?php

namespace Shopsys\ShopBundle\Model\Product\Unit\Exception;

use Shopsys\ShopBundle\Model\Product\Exception\ProductException;

interface UnitException extends ProductException
{
}
