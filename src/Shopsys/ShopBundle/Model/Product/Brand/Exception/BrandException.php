<?php

namespace Shopsys\ShopBundle\Model\Product\Brand\Exception;

use Shopsys\ShopBundle\Model\Product\Exception\ProductException;

interface BrandException extends ProductException
{
}
