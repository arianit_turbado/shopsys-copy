<?php

namespace Shopsys\ShopBundle\Model\Payment\GoPay;

class GoPayForm
{
    public static function getPaymentForm($gatewayUrl, $embedJs)
    {
        $form = "<form id=\"gopay-payment-button\" action=\"{$gatewayUrl}\" method=\"POST\">";
        $form .= "<script type=\"text/javascript\" src=\"{$embedJs}\"></script>";
        $form .= "<script>";
        $form .= "document.forms[\"gopay-payment-button\"].submit();";
        $form .= "</script>";
        $form .= "</form>";
        return $form;
    }
}