<?php

namespace Shopsys\ShopBundle\Model\Payment\PayPal;

use PayPal\Api\RedirectUrls;

class PayPalRedirectUrlFactory
{
    /**
     * Return redirect urls for paypal payment
     *
     * @param string $successUrl
     * @param string $failureUrl
     * @return RedirectUrls
     */
    public static function create($successUrl, $failureUrl)
    {
        $redirectUrls = new RedirectUrls();
        $redirectUrls->setReturnUrl($successUrl)
            ->setCancelUrl($failureUrl);
        
        return $redirectUrls;
    }
}