<?php

namespace Shopsys\ShopBundle\Model\Payment\PayPal;
use \Shopsys\ShopBundle\Model\Order\OrderFacade;
use \PayPal\Api\Payment;
use \PayPal\Api\PaymentExecution;

class PayPalPaymentFacade
{
    const MODE_TEST = "test";
    const MODE_LIVE = "live";

    /**
     * @var \Shopsys\ShopBundle\Model\Order\OrderFacade
     */
    protected $orderFacade;

    public function __construct(OrderFacade $orderFacade) 
    {
        $this->orderFacade = $orderFacade;
    }

    /**
     * Sets the payment and returns the html form that redirects to PayPal for payment
     *
     * @param int $orderId
     * @param string $successUrl
     * @param string $failureUrl
     * @param string $mode test|live
     * @return string
     */
    public function redirectToPayPal($orderId, $successUrl, $failureUrl, $mode = "test", $domainConfig)
    {
        $order = $this->orderFacade->getById($orderId);

        $env = $mode == static::MODE_TEST ? PayPalContext::env_sandbox : PayPalContext::env_live;
        $context = PayPalContext::create($env, $order->getDomainId());
        $redirectUrls = PayPalRedirectUrlFactory::create($successUrl, $failureUrl);
        
        $transaction = PayPalPaymentFactory::createTransaction($order);
        $payemntForm = PayPalPaymentFactory::createPaymentForm(
            $transaction,
            $redirectUrls,
            $context,
            $domainConfig
        );

        return $payemntForm;
    }

    public function executePayment($domainId, $paymentId, $payerId, $mode = "test")
    {
        $env = $mode == static::MODE_TEST ? PayPalContext::env_sandbox : PayPalContext::env_live;
        $context = PayPalContext::create($env, $domainId);

        $payment = Payment::get($paymentId, $context);

        $execution = new PaymentExecution();
        $execution->setPayerId($payerId);
        
        $result = false;
        try {
            $result = $payment->execute($execution, $context);
        } catch (\Exception $ex) {
            throw new \Exception("Payment failed", 500, $ex);
        }

        return $result ? $result->getState() == "approved" : false;
    }
}