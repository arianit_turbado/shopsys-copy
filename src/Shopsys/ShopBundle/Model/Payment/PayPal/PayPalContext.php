<?php

namespace Shopsys\ShopBundle\Model\Payment\PayPal;

use \PayPal\Rest\ApiContext;
use \PayPal\Auth\OAuthTokenCredential;


class PayPalContext
{
    protected static $credentials = [
        1 => [//eleka.de
            "sandbox" => [
                "client-id" => 'AWh6BJeWSSQUlNX3XMK_KfFHg56EVLwGBa2Zo8rafJJ87tHr5ieqxS0Gnv9FcLJ1d6lrS_nnsftt-XHE',
                "secret" => 'EC1HC0fveL-F-m1xHF9T7oMFYrisxdwBs5i09jMFl9j77kO9SeyPvqlR4-tvIgIiPTZiPAuZ402drjiG',
                "account" => 'turbado-facilitator@turbado.de'
            ]
        ],
        2 => [//scbudapest.hu
            "sandbox" => [
                "client-id" => 'AcIUIHeXkrVNZpninZeWxOKACfghYDwhSmhQzX-po4qIalnnV9PIPrHC1gaZCoStRA6HwoEJs6g5tlCm',
                "secret" => 'EE-JN6G-EJo0ftz3SqQbTJtldYrcpgo1O7ExAp88IcPC0WH_HaQ4cXEyCysQpXTK4DWEsRY_SwGYOsW4',
                "account" => 'czesc-facilitator@bestcena.pl'
            ]
        ],
        3 => [//scpoznan.pl
            "sandbox" => [
                "client-id" => 'AcIUIHeXkrVNZpninZeWxOKACfghYDwhSmhQzX-po4qIalnnV9PIPrHC1gaZCoStRA6HwoEJs6g5tlCm',
                "secret" => 'EE-JN6G-EJo0ftz3SqQbTJtldYrcpgo1O7ExAp88IcPC0WH_HaQ4cXEyCysQpXTK4DWEsRY_SwGYOsW4',
                "account" => 'czesc-facilitator@bestcena.pl'
            ]
        ],
        4 => [//exafoto.sk
            "sandbox" => [
                "client-id" => 'AcIUIHeXkrVNZpninZeWxOKACfghYDwhSmhQzX-po4qIalnnV9PIPrHC1gaZCoStRA6HwoEJs6g5tlCm',
                "secret" => 'EE-JN6G-EJo0ftz3SqQbTJtldYrcpgo1O7ExAp88IcPC0WH_HaQ4cXEyCysQpXTK4DWEsRY_SwGYOsW4',
                "account" => 'czesc-facilitator@bestcena.pl'
            ]
        ],
        5 => [//ocz.sk
            "sandbox" => [
                "client-id" => 'AcIUIHeXkrVNZpninZeWxOKACfghYDwhSmhQzX-po4qIalnnV9PIPrHC1gaZCoStRA6HwoEJs6g5tlCm',
                "secret" => 'EE-JN6G-EJo0ftz3SqQbTJtldYrcpgo1O7ExAp88IcPC0WH_HaQ4cXEyCysQpXTK4DWEsRY_SwGYOsW4',
                "account" => 'czesc-facilitator@bestcena.pl'
            ]
        ],
        100 => [//turbado.ro
            "sandbox" => [
                "client-id" => 'AcIUIHeXkrVNZpninZeWxOKACfghYDwhSmhQzX-po4qIalnnV9PIPrHC1gaZCoStRA6HwoEJs6g5tlCm',
                "secret" => 'EE-JN6G-EJo0ftz3SqQbTJtldYrcpgo1O7ExAp88IcPC0WH_HaQ4cXEyCysQpXTK4DWEsRY_SwGYOsW4',
                "account" => 'czesc-facilitator@bestcena.pl'
            ]
        ],
    ];

    const env_live = "live";
    const env_sandbox = "sandbox";

    /**
     * Returns paypal api context for specific environment
     *
     * @param string $env sandbox|live
     * @return \PayPal\Rest\ApiContext
     */
    public static function create($env, $domain_id)
    {
        $context = null;
        switch ($env) {
            case static::env_live:
                $context = new ApiContext(
                    new OAuthTokenCredential(
                        static::$credentials[$domain_id][static::env_live]["client-id"],
                        static::$credentials[$domain_id][static::env_live]["secret"]
                    )
                );
                break;
            case static::env_sandbox:
            default:
                $context = new ApiContext(
                    new OAuthTokenCredential(
                        static::$credentials[$domain_id][static::env_sandbox]["client-id"],
                        static::$credentials[$domain_id][static::env_sandbox]["secret"]
                    )
                );
                break;
        }

        $context->setConfig(
            array(
                'mode' => PayPalContext::getPayPalEnv($domain_id),
                'log.LogEnabled' => true,
                'log.FileName' => __DIR__ . '/PayPal.log',
                'log.LogLevel' => static::env_live ? 'INFO' : 'DEBUG'
            ));

        return $context;
    }

    protected static function getPayPalEnv($domain_id)
    {
        switch ($domain_id) {
            case 1: //eleka.de
                return 'TEST';
            case 2: //scbudapest.hu
                return 'TEST';
            case 3: //scpoznan.pl
                return 'TEST';
            case 4: //exafoto.sk
                return 'TEST';
            case 5: //exafoto.cz
                return 'TEST';
            case 100: //english.en
                return 'TEST';
            default:
                return 'TEST';
        }
        return $data;
    }
}