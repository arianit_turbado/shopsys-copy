<?php

namespace Shopsys\ShopBundle\Model\Payment\PayPal;

use \Shopsys\ShopBundle\Model\Order\Order;

use \PayPal\Rest\ApiContext;
use \PayPal\Api\Payer;
use \PayPal\Api\Payment;
use \PayPal\Api\PaymentExecution;
use \PayPal\Api\Item;
use \PayPal\Api\ItemList;
use \PayPal\Api\Details;
use \PayPal\Api\Amount;
use \PayPal\Api\RedirectUrls;
use \PayPal\Api\Transaction;
use \PayPal\Api\Presentation;
use \PayPal\Api\WebProfile;

class PayPalPaymentFactory
{
    /**
     * Crafts paypal transaction from order
     *
     * @param Shopsys\ShopBundle\Model\Order\Order $order
     * @return \PayPal\Api\Transaction
     */
    public static function createTransaction(Order $order)
    {
        $currency_code = $order->getCurrency()->getCode();
        $products = $order->getProductItems();
        $items = [];
        $subTotal = $order->getTotalProductPriceWithVat();
        $total = $order->getTotalPriceWithVat();
        $shippingPrice = $total - $subTotal;

        foreach($products as $product)
        {
            $item = new Item();
            $item->setName($product->getName())
                ->setCurrency($currency_code)
                ->setQuantity($product->getQuantity())
                ->setSku($product->getId())
                ->setPrice($product->getPriceWithVat());
            $items[] = $item;
        }
        
        $itemList = new ItemList();
        $itemList->setItems($items);
        
        $details = new Details();

        $details->setShipping($shippingPrice)
            // ->setTax($vatPrice)
            ->setSubtotal($subTotal); // Normal price collected from products
        
        $amount = new Amount();
        $amount->setCurrency($currency_code)
            ->setTotal($total)
            ->setDetails($details);
        
        
        $transaction = new Transaction();
        $transaction->setAmount($amount)
            ->setItemList($itemList)
            ->setDescription("Payment description")
            ->setInvoiceNumber($order->getId());
        
        return $transaction;
    }

    /**
     * Returns payment form for paypal transaction
     *
     * @param Transaction $transaction
     * @param ApiContext $context
     * @param array $domainConfig
     * @return \PayPal\Api\WebProfile
     */
    protected static function resolveProfile($context, $domainConfig)
    {
        $list = WebProfile::get_list($context);
        foreach ($list as $profile) 
        {
            if($profile->getName() == $domainConfig['unique_id'])
                return $profile->getId();
        }
        
        $presentation = new Presentation();
        $presentation->setLocaleCode($domainConfig["locale_code"]);
        
        $profile = new WebProfile();
        $profile->setName($domainConfig['unique_id'])
            ->setPresentation($presentation);
        
        $createProfileResponse = $profile->create($context);
        
        return $createProfileResponse->getId();
    }

    /**
     * Returns payment form for paypal transaction
     *
     * @param Transaction $transaction
     * @param ApiContext $context
     * @param array $domainConfig
     * @return string
     */
    public static function createPaymentForm(Transaction $transaction, RedirectUrls $redirectUrls, $context, $domainConfig)
    {
        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $profileId = static::resolveProfile($context, $domainConfig);

        $payment = new Payment();
        $payment->setIntent("sale")
            ->setPayer($payer)
            ->setRedirectUrls($redirectUrls)
            ->setTransactions([$transaction])
            ->setExperienceProfileId($profileId);

        try{
            $payment->create($context);
        } catch (\PayPal\Exception\PayPalConnectionException $ex) {
            // echo '<pre>';print_r(json_decode($ex->getData()));exit;
            throw new \Exception("Failed to create PayPal payment", 500, $ex);
        } catch (\Exception $ex) {
            // var_dump($ex->getMessage());exit;
            throw new \Exception("Failed to create PayPal payment", 500, $ex);
        }

        $approvalUrl = $payment->getApprovalLink();

        return "<script type=\"text/javascript\">window.location.href = '{$approvalUrl}';</script>";
    }
}