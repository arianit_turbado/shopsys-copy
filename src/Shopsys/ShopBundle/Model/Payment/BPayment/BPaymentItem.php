<?php

namespace Shopsys\ShopBundle\Model\Payment\BPayment;

class BPaymentItem {
/*
* Item description, maximum length is 80 characters
*/

protected $description;
/*
* Number of items of this type
*/
protected $count = 1;
/*
* Unit price of the item
*/
protected $unitAmount;
/*
* Item subtotal.
* itemamount = itemcount * itemunitamount
*/
protected $amount;

public function __construct($description, $unitAmount, $count = 1) {
$this->setDescription($description);
$this->unitAmount = $unitAmount;
$this->count = $count;
$this->calculateAmount();
}

protected function setDescription($description) {
$this->description = substr($description, 0, 80);
}

protected function calculateAmount() {
$this->amount = $this->count * $this->unitAmount;
}

public function getAmount() {
return $this->amount;
}

public function get($i) {
$data = array();
$data["Itemdescription_{$i}"] = $this->description;
$data["Itemcount_{$i}"] = $this->count;
$data["Itemunitamount_{$i}"] = $this->unitAmount;
$data["Itemamount_{$i}"] = number_format($this->amount, 2, '.', '');
return $data;
}

}