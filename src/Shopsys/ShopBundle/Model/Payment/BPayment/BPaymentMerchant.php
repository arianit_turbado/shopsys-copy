<?php

namespace Shopsys\ShopBundle\Model\Payment\BPayment;

class BPaymentMerchant {
/*
* Optional parameter. If present, an e-mail is sent to the merchant upon
* successful payment. The message contains information about the merchant
* and buyer, along with the contents of the shopping cart.
*/

protected $email;
/*
* Optional parameter. If the merchantlogo parameter contains the URL of an
* image (100x100 pixels), it will be displayed on the left-hand side, over
* the Merchant’s name.
* Important: if the image is not available over the SSL protocol,
* IE displays a warning message.
*/
protected $logo;

public function __construct($configData = null) {
if (is_array($configData)) {
if (isset($configData["merchantemail"])) {
$this->email = $configData["merchantemail"];
}
if (isset($configData["merchantlogo"])) {
$this->logo = $configData["merchantlogo"];
}
}
}

public function setEmail($email) {
$this->email = $email;
}

public function setLogo($logo) {
$this->logo = $logo;
}

public function get() {
$data = array();
if (!is_null($this->email)) {
$data["merchantemail"] = $this->email;
}
if (!is_null($this->logo)) {
$data["merchantlogo"] = $this->logo;
}
return $data;
}

}