<?php

namespace Shopsys\ShopBundle\Model\Payment\BPayment;

class BPaymentResponse
{

    const StatusOK = "OK";
    /*
    * If the customer clicks on the “Cancel” button, we redirect them to the
    * address specified in the returnurlcancel parameter
    */
    const StatusCancel = "Cancel";
    /*
    * In case of unexpected error, the customer is redirected to the address
    * specified in the returnurlerror parameter.
    */
    const StatusError = "Error";
    /*
    * The customer has returned from the payment page to the webshop.
    */
    const StepConfirmation = "Confirmation";
    /*
    * The payment transaction has been completed. For further information, see
    * Section C
    */
    const StepPayment = "Payment";

    protected $merchantId;
    /*
    * Order number generated and sent to the payment page by the webshop
    * during payment initiation
    */
    protected $orderId;
    /*
    * This digital signature is created by joining the parameters below and
    * applying the MD5 signature function:
    * orderid + amount + secretkey
    */
    protected $orderHash;
    /*
    * Payment authorisation from Borgun
    */
    protected $authorizationCode;
    /*
    * Partially masked credit card number (1234-******-1234)
    */
    protected $creditCardNumber;
    /*
    * Buyer details
    * Available only if registration was requested (pagetype = 1)
    */
    protected $buyer;
    /*
    * Amount of the processed order
    */
    protected $amount;
    /*
    * Order's currency
    */
    protected $currency;
    /*
    * "OK"/"Cancel"/"Error"
    */
    protected $status;
    /*
    * Borgun sends a success message to the webshop about the transaction on
    * two occasions.
    * The first one is sent when the customer has successfully made the
    * payment and Borgun has displayed the transaction certificate for the
    * customer. Important: this URL request comes from the Borgun server, it
    * is not forwarded by the customer and it is not in the same active
    * session.
    * The second one is sent when the customer clicks on the “back to webshop”
    * button.
    * The steps are identified by the following values:
    * “Payment”: The payment transaction has been completed. For further
    * information, see Section C.
    * “Confirmation”: The customer has returned from the payment page to the
    * webshop
    */
    protected $step;
    /*
    * It is included if the payment page was accessed using a ticket (See
    * Section 4). This parameter will contain the identifier of the ticket
    * used.
    */
    protected $ticket;
    /*
    * Error description to be displayed to the customer
    */
    protected $errorDescription;
    /*
    * Error code
    */
    protected $errorCode;
    /*
    * The test version of the payment page returns further parameters in this
    * case: Details of the error
    */
    protected $errorDetailX;
    /*
    * The test version of the payment page returns further parameters in this
    * case: Data from the audit log
    */
    protected $auditLogX;

    public function __construct($post)
    {
        if (isset($post['orderid'])) {

            $this->orderId = $post['orderid'];
        }
        if (isset($post['orderhash'])) {
            $this->orderHash = $post['orderhash'];
        }
        if (isset($post['authorizationcode'])) {
            $this->authorizationCode = $post['authorizationcode'];
        }
        if (isset($post['creditcardnumber'])) {
            $this->creditCardNumber = $post['creditcardnumber'];
        }
        $this->buyer = new bpayment_buyer();
        if (isset($post['buyername'])) {
            $this->buyer->setName($post['buyername']);
        }
        if (isset($post['buyeraddress'])) {
            $this->buyer->setAddress($post['buyeraddress']);
        }
        if (isset($post['buyerzip'])) {
            $this->buyer->setZip($post['buyerzip']);
        }
        if (isset($post['buyercity'])) {
            $this->buyer->setCity($post['buyercity']);
        }
        if (isset($post['buyercountry'])) {
            $this->buyer->setCountry($post['buyercountry']);
        }
        if (isset($post['buyerphone'])) {
            $this->buyer->setPhone($post['buyerphone']);
        }
        if (isset($post['buyeremail'])) {
            $this->buyer->setEmail($post['buyeremail']);
        }
        if (isset($post['buyerreferral'])) {
            $this->buyer->setReferral($post['buyerreferral']);
        }
        if (isset($post['buyercomment'])) {
            $this->buyer->setComment($post['buyercomment']);
        }
        if (isset($post['merchantid'])) {
            $this->merchantId = $post['merchantid'];
        }
        if (isset($post['amount'])) {
            $this->amount = $post['amount'];
        }
        if (isset($post['currency'])) {
            $this->currency = $post['currency'];
        }
        if (isset($post['status'])) {
            $this->status = $post['status'];
        }
        if (isset($post['step'])) {
            $this->step = $post['step'];
        }
        if (isset($post['errordescription'])) {
            $this->errorDescription = $post['errordescription'];
        }
        if (isset($post['errorcode'])) {
            $this->errorCode = $post['errorcode'];
        }
        if (isset($post['errordetailx'])) {
            $this->errorDetailX = $post['errordetailx'];
        }
        if (isset($post['auditlogx'])) {
            $this->auditLogX = $post['auditlogx'];
        }
    }

    public function getErrorCode()
    {
        return $this->errorCode;
    }

    public function getErrorDescription()
    {
        return $this->errorDescription;
    }

    public function getStep()
    {
        return $this->step;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function getAmount()
    {
        return $this->amount;
    }

    public function setAmount($amount)
    {
        $this->amount = $amount;
    }

    public function getMerchantId()
    {
        return $this->merchantId;
    }

    public function getBuyer()
    {
        return $this->buyer;
    }

    public function getCreditCardNumber()
    {
        return $this->creditCardNumber;
    }

    public function getAuthorizationCode()
    {
        return $this->authorizationCode;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

//public function checkHash($secretKey) {
//    $checkHash = md5($this->orderId . $this->amount . $secretKey);
//    return ($checkHash == $this->orderHash);
//}

    public function checkHash($secretKey)
    {
//if ($this->environment == borgun_test_defaults::testEnvironmentSetting) {
//    $secretKey = borgun_test_defaults::secretKey;
//}
// $message = utf8_encode("{$this->orderId}|{$this->getAmount()}|{$this->currency}");
        $message = utf8_encode("{$this->orderId}|" . number_format($this->amount, 2, '.', '') . "|{$this->currency}");
//echo $message;
        $checkHash = hash_hmac('sha256', $message, $secretKey);
        return ($checkHash == $this->orderHash);
    }

}