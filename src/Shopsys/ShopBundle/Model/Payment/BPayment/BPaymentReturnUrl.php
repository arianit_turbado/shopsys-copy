<?php

namespace Shopsys\ShopBundle\Model\Payment\BPayment;

class BPaymentReturnUrl {
    /*
     * Customer is redirected to this page after successful payment
     */

    protected $success;
    /*
     * The server sends notification of the successful payment transaction to
     * this URL. If the parameter is omitted, the address provided for
     * returnurlsuccess is used.
     */
    protected $successServer;
    /*
     * Customer is redirected to this page if they clock ‘cancel’ instead of
     * finalising the payment.
     */
    protected $cancel;
    /*
     * Customers are redirected to this page if an unexpected error occurs.
     */
    protected $error;

    public function __construct($configData = null) {
        if (is_array($configData)) {
            if (isset($configData["returnurlsuccess"])) {
                $this->success = $configData["returnurlsuccess"];
            }
            if (isset($configData["returnurlsuccessserver"])) {
                $this->successServer = $configData["returnurlsuccessserver"];
            }
            if (isset($configData["returnurlcancel"])) {
                $this->cancel = $configData["returnurlcancel"];
            }
            if (isset($configData["returnurlerror"])) {
                $this->error = $configData["returnurlerror"];
            }
        }
    }

    public function setSuccess($url) {
        $this->success = $url;
    }

    public function getSuccess() {
        return $this->success;
    }

    public function setSuccessServer($url) {
        $this->successServer = $url;
    }

    public function setCancel($url) {
        $this->cancel = $url;
    }

    public function setError($url) {
        $this->error = $url;
    }

    public function get() {
        $data = array();
        $data["returnurlsuccess"] = $this->success;
        $data["returnurlsuccessserver"] = $this->successServer;
        $data["returnurlcancel"] = $this->cancel;
        $data["returnurlerror"] = $this->error;
        return $data;
    }

}