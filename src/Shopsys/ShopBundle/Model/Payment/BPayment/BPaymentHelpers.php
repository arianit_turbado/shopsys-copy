<?php

namespace Shopsys\ShopBundle\Model\Payment\BPayment;

class BPaymentHelpers
{

    public static $debug = false;

    public static function setDebug($debug = "False")
    {
        if ($debug == "True") {
            bpayment_helpers::$debug = true;
            error_reporting(E_ALL);
            ini_set('display_errors', 1);
        }
    }

    public static function debug($var, $title = null, $hidden = true)
    {
        if (self::$debug) {
            if ($hidden) {
                print "<!--";
            } else {
                print "<pre>";
            }
            if (!is_null($title)) {
                print ($hidden ? "<h1>" : "") . $title . ($hidden ? "</h1>" : "\n");
            }
            var_dump($var);
            if ($hidden) {
                print "-->";
            } else {
                print "</pre>";
            }
        }
    }

}
