<?php

namespace Shopsys\ShopBundle\Model\Payment\BPayment;

class BPaymentBuyer
{
    /*
    * The customer’s name. If left empty, the customer can enter it on the
    * payment page.
    */

    protected $name;
    /*
    * Optional parameter. If present, an e-mail is sent to the customer upon
    * successful payment. The message contains information about the merchant
    * and buyer, along with the contents of the shopping cart
    */
    protected $email;
    protected $address;
    protected $zip;
    protected $city;
    protected $country;
    protected $phone;
    protected $referral;
    protected $comment;

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }

    public function getAddress()
    {
        return $this->address;
    }

    public function setZip($zip)
    {
        $this->zip = $zip;
    }

    public function getZip()
    {
        return $this->zip;
    }

    public function setCity($city)
    {
        $this->city = $city;
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCountry($country)
    {
        $this->country = $country;
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    public function getPhone()
    {
        return $this->phone;
    }

    public function setReferral($referral)
    {
        $this->referral = $referral;
    }

    public function getReferral()
    {
        return $this->referral;
    }

    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    public function getComment()
    {
        return $this->comment;
    }

    public function get()
    {
        $data = array();
        if (!is_null($this->name)) {
            $data["buyername"] = $this->name;
        }
        if (!is_null($this->email)) {
            $data["buyeremail"] = $this->email;
        }
        if (!is_null($this->address)) {
            $data["buyeraddress"] = $this->address;
        }
        if (!is_null($this->zip)) {
            $data["buyerzip"] = $this->zip;
        }
        if (!is_null($this->city)) {
            $data["buyercity"] = $this->city;
        }
        if (!is_null($this->country)) {
            $data["buyercountry"] = $this->country;
        }
        if (!is_null($this->phone)) {
            $data["buyerphone"] = $this->phone;
        }
        if (!is_null($this->referral)) {
            $data["buyerreferral"] = $this->referral;
        }
        if (!is_null($this->comment)) {
            $data["buyercomment"] = $this->comment;
        }
        return $data;
    }

}