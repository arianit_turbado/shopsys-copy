<?php

namespace Shopsys\ShopBundle\Model\Payment\BPayment;

class BPaymentForm
{

    public static function lines($data)
    {
        $lines = "";
        foreach ($data as $key => $value) {
            $lines .= "<input type=\"hidden\" name=\"{$key}\" value=\"{$value}\"/>";
        }
        return $lines;
    }

    public static function form($action, $data, $id = null)
    {
        $form_id = !is_null($id) ? $id : "bpayment_form_" . md5(serialize($data));
        $form = "<form id=\"{$form_id}\" action=\"{$action}\" method=\"POST\">";
        $form .= self::lines($data);
        $form .= "</form>";
        return $form;
    }

    public static function auto_submit($action, $data, $id = null)
    {
        $form_id = !is_null($id) ? $id : "bpayment_form_" . md5(serialize($data));
        $form = self::form($action, $data, $form_id);
        $form .= "<script>";
        $form .= "document.forms[\"{$form_id}\"].submit();";
        $form .= "</script>";
        return $form;
    }

}