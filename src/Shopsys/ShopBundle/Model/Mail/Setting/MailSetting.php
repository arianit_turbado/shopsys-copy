<?php

namespace Shopsys\ShopBundle\Model\Mail\Setting;

class MailSetting
{
    const MAIN_ADMIN_MAIL = 'mainAdminMail';
    const MAIN_ADMIN_MAIL_NAME = 'mainAdminMailName';
}
