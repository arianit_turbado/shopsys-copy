<?php

namespace Shopsys\ShopBundle\Model\Mail;

use Swift_Attachment;
use Swift_Mailer;
use Swift_Message;

class MailerService
{
    /**
     * @var \Swift_Mailer
     */
    private $mailer;

    public static $layoutsPath  = '/var/www/html/new-shopsys-framework/shopsys-framework/src/Shopsys/ShopBundle/Resources/views';
    // public static $layoutsPath  = '/var/www/shopsys-framework/src/Shopsys/ShopBundle/Resources/views';


    /**
     * @param Swift_Mailer $mailer
     */
    public function __construct(Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param \Shopsys\ShopBundle\Model\Mail\MessageData $messageData
     */
    public function send(MessageData $messageData)
    {
        if (array_key_exists($this->mailer->getTransport()->getLocalDomain(), $this->getDomainsCofing())) {
            $configs = $this->getDomainsCofing()[$this->mailer->getTransport()->getLocalDomain()];
            $this->mailer->getTransport()->setUsername($configs['username']);
            $this->mailer->getTransport()->setPassword($configs['password']);
        }

        $message = $this->getMessageWithReplacedVariables($messageData);
        $failedRecipients = [];

        if ($messageData->body === null || $messageData->subject === null) {
            throw new \Shopsys\ShopBundle\Model\Mail\Exception\EmptyMailException();
        }

        $successSend = $this->mailer->send($message, $failedRecipients);
        if (!$successSend && count($failedRecipients) > 0) {
            throw new \Shopsys\ShopBundle\Model\Mail\Exception\SendMailFailedException($failedRecipients);
        }
    }

    /**
     * @param \Shopsys\ShopBundle\Model\Mail\MessageData $messageData
     * @return \Swift_Message
     */
    private function getMessageWithReplacedVariables(MessageData $messageData)
    {
        $toEmail = $messageData->toEmail;
        $body = $this->replaceVariables(
            $messageData->body,
            $messageData->variablesReplacementsForBody
        );

        $body = $this->replaceBodyByTemplate($body);

        $subject = $this->replaceVariables(
            $messageData->subject,
            $messageData->variablesReplacementsForSubject
        );
        $fromEmail = $messageData->fromEmail;
        $fromName = $messageData->fromName;

        $message = Swift_Message::newInstance();
        $message->setSubject($subject);
        $message->setFrom($fromEmail, $fromName);
        $message->setTo($toEmail);
        if ($messageData->bccEmail !== null) {
            $message->addBcc($messageData->bccEmail);
        }
        if ($messageData->replyTo !== null) {
            $message->addReplyTo($messageData->replyTo);
        }
        $message->setContentType('text/plain; charset=UTF-8');
        $message->setBody(strip_tags($body), 'text/plain');
        $message->addPart($body, 'text/html');
        foreach ($messageData->attachmentsFilepaths as $attachmentFilepath) {
            $message->attach(Swift_Attachment::fromPath($attachmentFilepath));
        }

        return $message;
    }

    /**
     * @param string $string
     * @param array $variablesKeysAndValues
     * @return string
     */
    private function replaceVariables($string, $variablesKeysAndValues)
    {
        return strtr($string, $variablesKeysAndValues);
    }

    private function getDomainsCofing() {
        return [
            '[172.18.0.6]' => ['username' => 'Eleka' , 'password' => 'Gz5e%47A!azeR=hUr6'],
            'eleka.de' => ['username' => 'Eleka' , 'password' => 'Gz5e%47A!azeR=hUr6'],
            'scpoznan.pl' => ['username' => 'SCpoznanPL1' , 'password' => 'bestcena1902'],
            'scbudapest.hu' => ['username' => 'scbudapest' , 'password' => 'szia123pass'],
            'exafoto.sk' => ['username' => 'ockosicesk' , 'password' => '8686ac6f98b'],
            'ocz.cz' => ['username' => 'ocostravacz' , 'password' => 'Creativ2'],
        ];
    }

    private function domiansLayoustsPaths() {
        return [
            '[172.18.0.6]' => 'Mail/Layouts/eleka.html.twig',
            'eleka.de' => 'Mail/Layouts/eleka.html.twig',
            'scpoznan.pl' => 'Mail/Layouts/scpoznan.html.twig',
            'scbudapest.hu' => 'Mail/Layouts/scbudapest.html.twig',
            'exafoto.sk' => 'Mail/Layouts/exafoto.html.twig',
            'ocz.cz' => 'Mail/Layouts/ocz.html.twig',
        ];
    }

    private function replaceBodyByTemplate($body) {

        if (array_key_exists($this->mailer->getTransport()->getLocalDomain(), $this->domiansLayoustsPaths())) {
            $env = new \Twig_Environment(new \Twig_Loader_Filesystem(MailerService::$layoutsPath));
            $body = $env->render($this->domiansLayoustsPaths()[$this->mailer->getTransport()->getLocalDomain()], array('content' => $body));
        }
        return $body;
    }

    public function getMailer()
    {
        return $this->mailer;
    }
}
