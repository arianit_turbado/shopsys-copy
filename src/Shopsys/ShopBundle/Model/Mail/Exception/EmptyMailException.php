<?php

namespace Shopsys\ShopBundle\Model\Mail\Exception;

use Exception;

class EmptyMailException extends Exception implements MailException
{
}
