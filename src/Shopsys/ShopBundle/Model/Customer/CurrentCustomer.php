<?php

namespace Shopsys\ShopBundle\Model\Customer;

use Shopsys\ShopBundle\Model\Pricing\Group\PricingGroupSettingFacade;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class CurrentCustomer
{
    /**
     * @var \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage
     */
    private $tokenStorage;

    /**
     * @var \Shopsys\ShopBundle\Model\Pricing\Group\PricingGroupSettingFacade
     */
    private $pricingGroupSettingFacade;

    public function __construct(
        TokenStorage $tokenStorage,
        PricingGroupSettingFacade $pricingGroupSettingFacade
    ) {
        $this->tokenStorage = $tokenStorage;
        $this->pricingGroupSettingFacade = $pricingGroupSettingFacade;
    }

    /**
     * @return \Shopsys\ShopBundle\Model\Pricing\Group\PricingGroup
     */
    public function getPricingGroup()
    {
        $user = $this->findCurrentUser();
        if ($user === null) {
            if (isset($_SESSION['order']['pricing_group']))
                return $this->pricingGroupSettingFacade->pricingGroupRepository->getById($_SESSION['order']['pricing_group']);
            else
                return $this->pricingGroupSettingFacade->getDefaultPricingGroupByCurrentDomain();


        } else {
            return $user->getPricingGroup();
        }
    }

    /**
     * @return \Shopsys\ShopBundle\Model\Pricing\Group\PricingGroup
     */
    public function setPricingGroup($pricingGroup)
    {
       // bad do not use
       // $this->pricingGroupSettingFacade->setDefaultPricingGroupForSelectedDomain($pricingGroup);
    }

    /**
     * @return \Shopsys\ShopBundle\Model\Customer\User|null
     */
    public function findCurrentUser()
    {
        $token = $this->tokenStorage->getToken();
        if ($token === null) {
            return null;
        }

        $user = $token->getUser();
        if (!$user instanceof User) {
            return null;
        }

        return $user;
    }
}
