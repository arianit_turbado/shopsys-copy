<?php

namespace Shopsys\ShopBundle\Model\ShopInfo;

use Shopsys\ShopBundle\Component\Setting\Setting;

class ShopInfoSettingFacade
{
    const SHOP_INFO_PHONE_NUMBER = 'shopInfoPhoneNumber';
    const SHOP_INFO_EMAIL = 'shopInfoEmail';
    const SHOP_INFO_PHONE_HOURS = 'shopInfoPhoneHours';
    const DEFAULT_PRICING_GROUP = 'defaultPricingGroupId';
    const FACEBOOK_LINK = 'facebookLink';
    const GOOGLE_PLUS_LINK = 'googlePlusLink';
    const TWITTER_LINK = 'twitterLink';
    const YOUTUBE_LINK = 'youtubeLink';
    const CUSTOMER_SERVICE_INFO = 'customerServiceInfo';

    /**
     * @var \Shopsys\ShopBundle\Component\Setting\Setting
     */
    private $setting;

    /**
     * @param \Shopsys\ShopBundle\Component\Setting\Setting $setting
     */
    public function __construct(Setting $setting)
    {
        $this->setting = $setting;
    }

    /**
     * @param int $domainId
     * @return string|null
     */
    public function getPhoneNumber($domainId)
    {
        return $this->setting->getForDomain(self::SHOP_INFO_PHONE_NUMBER, $domainId);
    }

    /**
     * @param int $domainId
     * @return string|null
     */
    public function getEmail($domainId)
    {
        return $this->setting->getForDomain(self::SHOP_INFO_EMAIL, $domainId);
    }

    /**
     * @param int $domainId
     * @return string|null
     */
    public function getPhoneHours($domainId)
    {
        return $this->setting->getForDomain(self::SHOP_INFO_PHONE_HOURS, $domainId);
    }

    /**
     * @param int $domainId
     * @return string|null
     */
    public function getFacebookLink($domainId)
    {
        return $this->setting->getForDomain(self::FACEBOOK_LINK, $domainId);
    }

    /**
     * @param int $domainId
     * @return string|null
     */
    public function getGooglePlusLink($domainId)
    {
        return $this->setting->getForDomain(self::GOOGLE_PLUS_LINK, $domainId);
    }

    /**
     * @param int $domainId
     * @return string|null
     */
    public function getTwitterLink($domainId)
    {
        return $this->setting->getForDomain(self::TWITTER_LINK, $domainId);
    }

    /**
     * @param int $domainId
     * @return string|null
     */
    public function getYoutubeLink($domainId)
    {
        return $this->setting->getForDomain(self::YOUTUBE_LINK, $domainId);
    }

    /**
     * @param int $domainId
     * @return string|null
     */
    public function getCustomerServiceInfo($domainId)
    {
        return $this->setting->getForDomain(self::CUSTOMER_SERVICE_INFO, $domainId);
    }


    /**
     * @param string|null $value
     * @param int $domainId
     */
    public function setPhoneNumber($value, $domainId)
    {
        $this->setting->setForDomain(self::SHOP_INFO_PHONE_NUMBER, $value, $domainId);
    }

    /**
     * @param string|null $value
     * @param int $domainId
     */
    public function setEmail($value, $domainId)
    {
        $this->setting->setForDomain(self::SHOP_INFO_EMAIL, $value, $domainId);
    }

    /**
     * @param string|null $value
     * @param int $domainId
     */
    public function setPhoneHours($value, $domainId)
    {
        $this->setting->setForDomain(self::SHOP_INFO_PHONE_HOURS, $value, $domainId);
    }

    /**
     * @param string|null $value
     * @param int $domainId
     */
    public function setFacebookLink($value, $domainId)
    {
        $this->setting->setForDomain(self::FACEBOOK_LINK, $value, $domainId);
    }

    /**
     * @param string|null $value
     * @param int $domainId
     */
    public function setGooglePlusLink($value, $domainId)
    {
        $this->setting->setForDomain(self::GOOGLE_PLUS_LINK, $value, $domainId);
    }

    /**
     * @param string|null $value
     * @param int $domainId
     */
    public function setTwitterLink($value, $domainId)
    {
        $this->setting->setForDomain(self::TWITTER_LINK, $value, $domainId);
    }

    /**
     * @param string|null $value
     * @param int $domainId
     */
    public function setYoutubeLink($value, $domainId)
    {
        $this->setting->setForDomain(self::YOUTUBE_LINK, $value, $domainId);
    }

    /**
     * @param string|null $value
     * @param int $domainId
     */
    public function setCustomerServiceInfo($value, $domainId)
    {
        $this->setting->setForDomain(self::CUSTOMER_SERVICE_INFO, $value, $domainId);
    }


    /**
     * @return \Shopsys\ShopBundle\Model\Pricing\Group\PricingGroup
     */
    public function getDefaultPricingGroupBySelectedDomain($domainId)
    {
        return $this->setting->getForDomain(self::DEFAULT_PRICING_GROUP, $domainId);
    }

    /**
     * @param \Shopsys\ShopBundle\Model\Pricing\Group\PricingGroup $pricingGroup
     */
    public function setDefaultPricingGroupForSelectedDomain(PricingGroup $pricingGroup)
    {
        $this->setting->setForDomain(Setting::DEFAULT_PRICING_GROUP, $pricingGroup->getId(), $this->adminDomainTabsFacade->getSelectedDomainId());
    }

    public function createNullValues() {
        $this->setting->createNullValues();
    }


}
