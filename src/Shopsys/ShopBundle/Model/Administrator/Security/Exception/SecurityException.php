<?php

namespace Shopsys\ShopBundle\Model\Administrator\Security\Exception;

use Shopsys\ShopBundle\Model\Administrator\Exception\AdministratorException;

interface SecurityException extends AdministratorException
{
}
