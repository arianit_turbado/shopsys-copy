<?php

namespace Shopsys\ShopBundle\Model\Administrator\Activity\Exception;

use Shopsys\ShopBundle\Model\Administrator\Exception\AdministratorException;

interface AdministratorActivityException extends AdministratorException
{
}
