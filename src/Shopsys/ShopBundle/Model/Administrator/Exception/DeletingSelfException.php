<?php

namespace Shopsys\ShopBundle\Model\Administrator\Exception;

use Exception;

class DeletingSelfException extends Exception implements AdministratorException
{
}
