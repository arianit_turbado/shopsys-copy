<?php

namespace Shopsys\ShopBundle\Model\Administrator\Exception;

use Exception;

class DeletingSuperadminException extends Exception implements AdministratorException
{
}
