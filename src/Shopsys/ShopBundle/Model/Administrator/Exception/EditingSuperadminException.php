<?php

namespace Shopsys\ShopBundle\Model\Administrator\Exception;

use Exception;

class EditingSuperadminException extends Exception implements AdministratorException
{
}
