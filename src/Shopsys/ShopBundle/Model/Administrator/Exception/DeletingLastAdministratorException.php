<?php

namespace Shopsys\ShopBundle\Model\Administrator\Exception;

use Exception;

class DeletingLastAdministratorException extends Exception implements AdministratorException
{
}
