<?php

namespace Shopsys\ShopBundle\Component\Router\FriendlyUrl\Exception;

use Shopsys\ShopBundle\Component\Router\Exception\RouterException;

interface FriendlyUrlException extends RouterException
{
}
