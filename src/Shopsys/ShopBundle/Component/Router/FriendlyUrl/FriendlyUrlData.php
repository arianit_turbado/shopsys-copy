<?php

namespace Shopsys\ShopBundle\Component\Router\FriendlyUrl;

class FriendlyUrlData
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;
}
