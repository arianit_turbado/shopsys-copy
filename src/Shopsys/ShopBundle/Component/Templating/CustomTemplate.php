<?php

namespace Shopsys\ShopBundle\Component\Templating;

use Shopsys\ShopBundle\Component\Domain\Domain;

class CustomTemplate
{
    /**
     * @var Domain
     */
    private $domain;

    /**
     * @param $domainId
     */
    private $domainId;

    /**
     * @var string
     */
    private $domainTemplateName;

    /**
     * @var string
     */
    private $rootDir;


    public function __construct($rootDir, Domain $domain)
    {
        $this->rootDir = $rootDir;
        $this->domain = $domain;
        $this->domainId = $this->domain->getCurrentDomainConfig()->getId();
        $this->domainTemplateName = '/' . $this->domain->getCurrentDomainConfig()->getDomainTemplate();
    }

    public function getTemplatePath($file)
    {
        $viewsPath = str_replace('app', '', $this->rootDir) . 'src/Shopsys/ShopBundle/Resources/views';

        if (file_exists($viewsPath . $this->domainTemplateName . $file)) {
            return $viewsPath . $this->domainTemplateName . $file;
        } else {
            return $viewsPath . $file;
        }
    }
}
