<?php

namespace Shopsys\ShopBundle\Component\Image\Processing\Exception;

use Shopsys\ShopBundle\Component\Image\Exception\ImageException;

interface ImageProcessingException extends ImageException
{
}
