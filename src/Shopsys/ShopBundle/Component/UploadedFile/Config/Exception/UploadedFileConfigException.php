<?php

namespace Shopsys\ShopBundle\Component\UploadedFile\Config\Exception;

use Shopsys\ShopBundle\Component\UploadedFile\Exception\FileException;

interface UploadedFileConfigException extends FileException
{
}
