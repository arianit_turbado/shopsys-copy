<?php

namespace Shopsys\GeneratorBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class GeneratorController extends Controller
{
    public function indexAction(Request $request)
    {
        $generatorsFormFactory = $this->get('shopsys.generator.generators_form_factory');
        /* @var $generatorsFormFactory \Shopsys\GeneratorBundle\Model\GeneratorsFormFactory */
        $generatorFacade = $this->get('shopsys.generator.generator_facade');
        /* @var $generatorFacade \Shopsys\GeneratorBundle\Model\GeneratorFacade */

        $form = $generatorsFormFactory->createForm();
        $form->handleRequest($request);

        $createdFiles = [];
        $errorMessage = null;

        if ($form->isValid()) {
            try {
                $createdFiles = $generatorFacade->generate($form->getData());
            } catch (\Shopsys\GeneratorBundle\Model\Exception\GeneratorTargetFileAlreadyExistsExpception $ex) {
                $errorMessage = $ex->getMessage();
            }
        }

        return $this->render('@ShopsysGenerator/index.html.twig', [
            'form' => $form->createView(),
            'generatorsNames' => $generatorFacade->getGeneratorsNames(),
            'createdFiles' => $createdFiles,
            'errorMessage' => $errorMessage,
        ]);
    }
}
