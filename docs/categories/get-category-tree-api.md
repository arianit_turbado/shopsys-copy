# Getting categories tree in json through route '/api/getCategoriesTree'

Following we will describe each step:

1. Go te the directory  \Shopsys\ShopBundle\Controller and create a new directory Api, there will be located the controllers related to the API

2. Under directory  \Shopsys\ShopBundle\Controller\Api create new controller called ApiController
    ```php
    namespace Shopsys\ShopBundle\Controller\Api;
    
    
    
    use Symfony\Component\HttpFoundation\JsonResponse;
    use Shopsys\ShopBundle\Model\Category\CategoryFacade;
    use Symfony\Component\HttpFoundation\Request;
    
    use Symfony\Bundle\FrameworkBundle\Controller\Controller;
    
    
    
    class ApiController extends Controller
    {
    
         public function __construct(
            CategoryFacade $categoryFacade
        ) {
            $this->categoryFacade = $categoryFacade;
    
        }
       
        public function getCategoriesTreeAction(Request $request)
        {
            $details = $this->categoryFacade->getCategoriesTreeJson($request->get('locale'));
    
            return new JsonResponse(array('categories' => $details));
        }
    
    }
    ```
    

3. Open \Shopsys\ShopBundle\Model\Category\CategoryFacade and add the following methods te get the categories tree
  ```php
  
    class CategoryFacade
    {
    
        public function getCategoriesTree($locale) {
               $categories = []; 
               $root_categories = $this->getAllCategoryDetails($locale);
               foreach ($root_categories as $root_category) {
                   $categories[] = $this->getCategoryWithTree($root_category);
               } 
               return $categories;
           }
       
           public function getCategoryWithTree($categoryDetails) {
               $category = ['id' => $categoryDetails->getCategory()->getId(), 'name' => $categoryDetails->getCategory()->getName(), 'children' => []];
               $children = $categoryDetails->getChildren();
               if(count($children) > 0) {
                   foreach ($children as $child) {
                       $category['children'][] = $this->getCategoryWithTree($child);
                   } 
               }
       
               return $category;
           }
    
    }
   ```

    
    
4. Under directory /Shopsys/ShopBundle/Resources/config, create file routing_api.yml, there will be located the routes for api, example of route te get categories tree
   ```
   api_category_tree: 
     path: '/getCategoriesTree/{locale}'
     defaults: { _controller: ShopsysShopBundle:Api\Api:getCategoriesTree, locale: 'en'}
    ```



5. Open the file /Shopsys/ShopBundle/Resources/config/routing.yml, and add:
   
   shopsys_shop_api:
     resource: "@ShopsysShopBundle/Resources/config/routing_api.yml"
     prefix: /api
     
   This is required in order to notify the framework for the new routers located in routing_api.yml


