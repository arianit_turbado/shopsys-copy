# Shopsys Framework Knowledge Base

## Introduction
* [Installation Guide](introduction/installation-guide.md) and [Installation Using Docker](introduction/installation-using-docker.md)
* [Phing Targets](introduction/phing-targets.md)
* [Automated Testing](introduction/automated-testing.md)
* [Basics About Model Architecture](introduction/basics-about-model-architecture.md)
* [Basics About Package Architecture](introduction/basics-about-package-architecture.md)

## Cookbook
* [Adding New Attribute to an Entity](cookbook/adding-new-attribute-to-an-entity.md)
* [Adding New Page With Recently Bought Products](cookbook/adding-new-page-with-recently-bought-products.md)
* [Basic Data Import](cookbook/basic-data-import.md)
* [Dumping and Importing the Database](cookbook/dumping-and-importing-the-database.md)
