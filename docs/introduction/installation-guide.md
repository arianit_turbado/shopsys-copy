# Installation Guide
This guide provides instructions how to install Shopsys Framework on your local machine as a server.
If you would like to use a prepared Docker container instead go to [Installation Using Docker](installation-using-docker.md).

## Requirements
First of all, you need to install the following software on your system:

* [GIT](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)
* [PostgreSQL 9.4](https://wiki.postgresql.org/wiki/Detailed_installation_guides) (On Windows OS, we recommend to use [EnterpriseDB PostgreSQL distribution](https://www.enterprisedb.com/downloads/postgres-postgresql-downloads#windows))
* [PHP 7.x](http://php.net/manual/en/install.php) (configure your `php.ini` by [Required PHP Configuration](required-php-configuration.md))
* [Composer](https://getcomposer.org/doc/00-intro.md#globally)
* [Node.js 4.x](https://nodejs.org/en/download/)
* [npm 2.x](https://nodejs.org/en/download/) (npm is automatically installed when you install Node.js)

*Note: The names link to the appropriate installation guide or download page.* 

After that, you can follow the steps below in order to install and configure Shopsys Framework.

## Steps
### 1. Install dependencies and configure parameters
```
composer install
```

Composer will prompt you to set main parameters (`app/config/parameters.yml`):

| Name                              | Description                                                                   |
| --------------------------------- | ----------------------------------------------------------------------------- |
| `database_host`                   | access data of your PostgreSQL database                                       |
| `database_port`                   | ...                                                                           |
| `database_name`                   | ...                                                                           |
| `database_user`                   | ...                                                                           |
| `database_password`               | ...                                                                           |
| `database_server_version`         | version of your PostgreSQL server                                             |
| `mailer_transport`                | access data of your mail server                                               |
| `mailer_host`                     | ...                                                                           |
| `mailer_user`                     | ...                                                                           |
| `mailer_password`                 | ...                                                                           |
| `mailer_disable_delivery`         | set to `true` if you don't want to send any e-mails                           |
| `mailer_master_email_address`     | set if you want to send all e-mails to one address (useful for development)   |
| `mailer_delivery_whitelist`       | set if you want to have master e-mail but allow sending to specific addresses |
| `error_reporting_email_to`        | e-mail address that will be used as recipient for error reports               |
| `error_reporting_email_from`      | e-mail address that will be used as sender for error reports                  |
| `secret`                          | randomly generated secret token                                               |

Composer will then prompt you to set parameters for testing environment (`app/config/parameters_test.yml`):

| Name                              | Description                                                                   |
| --------------------------------- | ----------------------------------------------------------------------------- |
| `test_database_host`              | access data of your PostgreSQL database for tests                             |
| `test_database_port`              | ...                                                                           |
| `test_database_name`              | ...                                                                           |
| `test_database_user`              | ...                                                                           |
| `overwrite_domain_url`            | overwrites URL of all domains for acceptance testing (set to `~` to disable)  |
| `selenium_server_host`            | with native installation the selenium server is on `localhost`                |
| `test_database_password`          | ...                                                                           |
| `test_mailer_transport`           | access data of your mail server for tests                                     |
| `test_mailer_host`                | ...                                                                           |
| `test_mailer_user`                | ...                                                                           |
| `test_mailer_password`            | ...                                                                           |

#### Choose environment type
For development choose `n` when asked `Build in production environment? (Y/n)`.

It will set the environment in your application to `dev` (this will, for example, show Symfony Web Debug Toolbar).
