**Description, reason for the PR:** ...

**New feature:** Yes/No <!-- Do not forget to update CHANGELOG.md and possibly docs/ -->

**BC breaks:** Yes/No <!-- Do not forget to update UPGRADE.md -->

**Fixes issues:** ...

**Standards and tests pass:** Yes/No

**Licence:** MIT

