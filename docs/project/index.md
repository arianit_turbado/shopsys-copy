# Project Documentation

*This is a place where to put any important information that other developers should know about the project. For advices on what should be documented see [Guidelines for Project Documentation](guidelines-for-project-documentation.md)*.

**{Put the content here}**
