module.exports = function(grunt) {

    require('es6-promise').polyfill();
    require('jit-grunt')(grunt, {
        sprite: 'pngsmith'
    });
    require('time-grunt')(grunt);

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        less: {
            helpersAdmin: {
                files: {
                    'src/Shopsys/ShopBundle/Resources/styles/admin/helpers/helpers-generated.less': 'src/Shopsys/ShopBundle/Resources/styles/admin/helpers.less'
                }
            },
            admin: {
                files: {
                    'web/assets/admin/styles/index_1526404914.css': 'src/Shopsys/ShopBundle/Resources/styles/admin/main.less'
                },
                options: {
                    compress: true,
                    sourceMap: true,
                    sourceMapFilename: 'web/assets/admin/styles/index_1526404914.css.map',
                    sourceMapBasepath: 'web',
                    sourceMapURL: 'index_1526404914.css.map',
                    sourceMapRootpath: '../../../'
                }
            },

            helpers1: {
                    files: {
                        'src/Shopsys/ShopBundle/Resources/styles/front/common/helpers/helpers-generated.less': 'src/Shopsys/ShopBundle/Resources/styles/front/common/helpers.less'
                    }
                },
                frontend1: {
                    files: {
                        'web/assets/frontend/styles/index1_1526404914.css': 'src/Shopsys/ShopBundle/Resources/styles/front/common/main.less'
                    },
                    options: {
                        compress: true,
                        sourceMap: true,
                        sourceMapFilename: 'web/assets/frontend/styles/index1_1526404914.css.map',
                        sourceMapBasepath: 'web',
                        sourceMapURL: 'index1_1526404914.css.map',
                        sourceMapRootpath: '../../../'
                    }
                },
            helpers2: {
                    files: {
                        'src/Shopsys/ShopBundle/Resources/styles/front/domain2/helpers/helpers-generated.less': 'src/Shopsys/ShopBundle/Resources/styles/front/domain2/helpers.less'
                    }
                },
                frontend2: {
                    files: {
                        'web/assets/frontend/styles/index2_1526404914.css': 'src/Shopsys/ShopBundle/Resources/styles/front/domain2/main.less'
                    },
                    options: {
                        compress: true,
                        sourceMap: true,
                        sourceMapFilename: 'web/assets/frontend/styles/index2_1526404914.css.map',
                        sourceMapBasepath: 'web',
                        sourceMapURL: 'index2_1526404914.css.map',
                        sourceMapRootpath: '../../../'
                    }
                },
            wysiwyg1: {
                    files: {
                        'web/assets/admin/styles/wysiwyg_1526404914.css': 'src/Shopsys/ShopBundle/Resources/styles/front/common/wysiwyg.less'
                    },
                    options: {
                        compress: true
                    }
                },
            wysiwyg2: {
                    files: {
                        'web/assets/admin/styles/wysiwyg_1526404914.css': 'src/Shopsys/ShopBundle/Resources/styles/front/domain2/wysiwyg.less'
                    },
                    options: {
                        compress: true
                    }
                },
            wysiwygLocalized1: {
                    files: {
                        'web/assets/admin/styles/wysiwyg-localized_1526404914.css': 'src/Shopsys/ShopBundle/Resources/styles/admin//wysiwyg-localized.less'
                    },
                    options: {
                        compress: true
                    }
                },            wysiwygLocalized2: {
                    files: {
                        'web/assets/admin/styles/wysiwyg-localized_1526404914.css': 'src/Shopsys/ShopBundle/Resources/styles/admin//wysiwyg-localized.less'
                    },
                    options: {
                        compress: true
                    }
                }            },

        postcss: {
            options: {
                processors: [
                    require('autoprefixer')({browsers: ['last 3 versions', 'ios 6', 'Safari 7', 'Safari 8', 'ie 7', 'ie 8', 'ie 9']})
                ]
            },
            dist: {
                src: ['web/assets/frontend/styles/*.css', 'web/assets/admin/styles/*.css']
            }
        },

        legacssy: {
            admin: {
                options: {
                    legacyWidth: 1200,
                    matchingOnly: false,
                    overridesOnly: false
                },
                files: {
                    'web/assets/admin/styles/index_1526404914-ie8.css': 'web/assets/admin/styles/index_1526404914.css'
                }
            },

            frontend1: {
                    options: {
                        legacyWidth: 1200,
                        matchingOnly: false,
                        overridesOnly: false
                    },
                    files: {
                        'web/assets/frontend/styles/index1_1526404914-ie8.css': 'web/assets/frontend/styles/index1_1526404914.css'
                    }
                },
            frontend2: {
                    options: {
                        legacyWidth: 1200,
                        matchingOnly: false,
                        overridesOnly: false
                    },
                    files: {
                        'web/assets/frontend/styles/index2_1526404914-ie8.css': 'web/assets/frontend/styles/index2_1526404914.css'
                    }
                }
            },

        sprite: {
            admin: {
                src: 'web/assets/admin/images/icons/*.png',
                dest: 'web/assets/admin/images/sprites/sprite.png',
                destCss: 'src/Shopsys/ShopBundle/Resources/styles/admin/libs/sprites.less',
                imgPath: '../images/sprites/sprite.png?v=' + (new Date().getTime()),
                algorithm: 'binary-tree',
                padding: 50,
                cssFormat: 'css',
                cssVarMap: function (sprite) {
                    sprite.name = 'sprite.sprite-' + sprite.name;
                },
                engineOpts: {
                    imagemagick: true
                },
                imgOpts: {
                    format: 'png',
                    quality: 90,
                    timeout: 10000
                },
                cssOpts: {
                    functions: false,
                    cssClass: function (item) {
                        return '.' + item.name;
                    },
                    cssSelector: function (sprite) {
                        return '.' + sprite.name;
                    }
                }
            },

            frontend: {
                src: 'web/assets/frontend/images/icons/*.png',
                dest: 'web/assets/frontend/images/sprites/sprite.png',
                destCss: 'src/Shopsys/ShopBundle/Resources/styles/front/common/libs/sprites.less',
                imgPath: '../images/sprites/sprite.png?v=' + (new Date().getTime()),
                algorithm: 'binary-tree',
                padding: 50,
                cssFormat: 'css',
                cssVarMap: function (sprite) {
                    sprite.name = 'sprite.sprite-' + sprite.name;
                },
                engineOpts: {
                    imagemagick: true
                },
                imgOpts: {
                    format: 'png',
                    quality: 90,
                    timeout: 10000
                },
                cssOpts: {
                    functions: false,
                    cssClass: function (item) {
                        return '.' + item.name;
                    },
                    cssSelector: function (sprite) {
                        return '.' + sprite.name;
                    }
                }
            }
        },

        webfont: {
            admin: {
                src: 'src/Shopsys/ShopBundle/Resources/svg/admin/*.svg',
                dest: 'web/assets/admin/fonts',
                destCss: 'src/Shopsys/ShopBundle/Resources/styles/admin/libs/',
                options: {
                    autoHint: false,
                    font: 'svg',
                    hashes: true,
                    types: 'eot,woff,ttf,svg',
                    engine: 'node',
                    stylesheet: 'less',
                    relativeFontPath: '../fonts',
                    fontHeight: '512',
                    descent: '0',
                    destHtml: 'docs/generated',
                    htmlDemoFilename: 'webfont-admin-svg',
                    templateOptions: {
                        baseClass: 'svg',
                        classPrefix: 'svg-',
                        mixinPrefix: 'svg-'
                    }
                }
            },
            frontend: {
                src: 'src/Shopsys/ShopBundle/Resources/svg/front/*.svg',
                dest: 'web/assets/frontend/fonts',
                destCss: 'src/Shopsys/ShopBundle/Resources/styles/front/common/libs/',
                options: {
                    autoHint: false,
                    font: 'svg',
                    hashes: true,
                    types: 'eot,woff,ttf,svg',
                    engine: 'node',
                    stylesheet: 'less',
                    relativeFontPath: '../fonts',
                    fontHeight: '512',
                    descent: '0',
                    destHtml: 'docs/generated',
                    htmlDemoFilename: 'webfont-frontend-svg',
                    templateOptions: {
                        baseClass: 'svg',
                        classPrefix: 'svg-',
                        mixinPrefix: 'svg-'
                    }
                }
            }
        },

        watch: {
            admin: {
                files: [
                    'src/Shopsys/ShopBundle/Resources/styles/admin/**/*.less',
                    '!src/Shopsys/ShopBundle/Resources/styles/admin/helpers.less',
                    '!src/Shopsys/ShopBundle/Resources/styles/admin/helpers/*.less',
                    'src/Shopsys/ShopBundle/Resources/styles/admin/helpers/helpers-generated.less'
                ],
                tasks: ['adminLess']
            },
            adminHelpers: {
                files: [
                    'src/Shopsys/ShopBundle/Resources/styles/admin/helpers.less',
                    'src/Shopsys/ShopBundle/Resources/styles/admin/helpers/*.less',
                    '!src/Shopsys/ShopBundle/Resources/styles/admin/helpers/helpers-generated.less'
                ],
                tasks: ['less:helpersAdmin']
            },
            adminSprite: {
                files: ['web/assets/admin/images/icons/**/*.png'],
                tasks: ['sprite:admin'],
                options: {
                    livereload: true
                }
            },
            adminWebfont: {
                files: ['src/Shopsys/ShopBundle/Resources/svg/admin/*.svg'],
                tasks: ['webfont:admin'],
                options: {
                    livereload: true
                }
            },
            frontendSprite: {
                files: ['web/assets/frontend/images/icons/**/*.png'],
                tasks: ['sprite:frontend'],
                options: {
                    livereload: true
                }
            },
            frontendWebfont: {
                files: ['src/Shopsys/ShopBundle/Resources/svg/front/*.svg'],
                tasks: ['webfont:frontend']
            },
            frontend1: {
                    files: [
                        'src/Shopsys/ShopBundle/Resources/styles/front/common/**/*.less',
                        '!src/Shopsys/ShopBundle/Resources/styles/front/common/helpers.less',
                        '!src/Shopsys/ShopBundle/Resources/styles/front/common/helpers/*.less',
                        'src/Shopsys/ShopBundle/Resources/styles/front/common/helpers/helpers-generated.less',
                        '!src/Shopsys/ShopBundle/Resources/styles/front/common/core/mixin/*.less',
                        'src/Shopsys/ShopBundle/Resources/styles/front/common/core/mixin/base.less'
                    ],
                    tasks:['frontendLess1','frontendLess2']
                    },
                helpers1: {
                    files: [
                        'src/Shopsys/ShopBundle/Resources/styles/front/common/helpers.less',
                        'src/Shopsys/ShopBundle/Resources/styles/front/common/helpers/*.less',
                        '!src/Shopsys/ShopBundle/Resources/styles/front/common/helpers/helpers-generated.less',
                        'src/Shopsys/ShopBundle/Resources/styles/front/common/core/mixin/*.less',
                        '!src/Shopsys/ShopBundle/Resources/styles/front/common/core/mixin/base.less'
                    ],
                    tasks:['less:helpers1','less:helpers2']
                    },
            frontend2: {
                    files: [
                        'src/Shopsys/ShopBundle/Resources/styles/front/domain2/**/*.less',
                        '!src/Shopsys/ShopBundle/Resources/styles/front/domain2/helpers.less',
                        '!src/Shopsys/ShopBundle/Resources/styles/front/domain2/helpers/*.less',
                        'src/Shopsys/ShopBundle/Resources/styles/front/domain2/helpers/helpers-generated.less',
                        '!src/Shopsys/ShopBundle/Resources/styles/front/domain2/core/mixin/*.less',
                        'src/Shopsys/ShopBundle/Resources/styles/front/domain2/core/mixin/base.less'
                    ],
                    tasks:['frontendLess2']
                    },
                helpers2: {
                    files: [
                        'src/Shopsys/ShopBundle/Resources/styles/front/domain2/helpers.less',
                        'src/Shopsys/ShopBundle/Resources/styles/front/domain2/helpers/*.less',
                        '!src/Shopsys/ShopBundle/Resources/styles/front/domain2/helpers/helpers-generated.less',
                        'src/Shopsys/ShopBundle/Resources/styles/front/domain2/core/mixin/*.less',
                        '!src/Shopsys/ShopBundle/Resources/styles/front/domain2/core/mixin/base.less'
                    ],
                    tasks:['frontendLess2']
                    },
            
            livereload: {
                options: {
                    livereload: true
                },
                files: ['web/assets/admin/styles/*.css', 'web/assets/frontend/styles/*.css']
            },
            twig: {
                files: ['src/Shopsys/ShopBundle/Resources/views/**/*.twig'],
                tasks: [],
                options: {
                    livereload: true,
                }
            }
        }
    });
    grunt.loadNpmTasks('grunt-spritesmith');

    grunt.registerTask('default', ["sprite:admin", "sprite:frontend", "webfont", "less", "postcss", "legacssy"]);

    grunt.registerTask('frontend1', ['webfont:frontend', 'sprite:frontend', 'less:frontend1', 'legacssy:frontend1', 'less:wysiwyg1'], 'postcss');
    grunt.registerTask('frontend2', ['webfont:frontend', 'sprite:frontend', 'less:frontend2', 'legacssy:frontend2', 'less:wysiwyg2'], 'postcss');
    grunt.registerTask('admin', ['sprite:admin', 'webfont:admin', 'less:admin', 'legacssy:admin' ]);

    grunt.registerTask('frontendLess1', ['less:frontend1', 'legacssy:frontend1', 'less:wysiwyg1']);
    grunt.registerTask('frontendLess2', ['less:frontend2', 'legacssy:frontend2', 'less:wysiwyg2']);
    grunt.registerTask('adminLess', ['less:admin', 'legacssy:admin' ]);
};
